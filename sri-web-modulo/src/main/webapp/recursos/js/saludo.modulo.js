(function (angular) {

    'use strict';

    angular.module('saludo', ['ui.router', 'ui.bootstrap','pascalprecht.translate','tmh.dynamicLocale']);


    angular.module('saludo').config(['$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {
            $stateProvider
                    .state('sri.saludo.saludar', {
                        url: '/saludar',
                        templateUrl: '/sri-web-modulo/paginas/saludo.html',
                        resolve: {
                            cargaRecursos: function ($ocLazyLoad) {
                                return $ocLazyLoad.load({
files: [
"/sri-web-modulo/recursos/js/servicios/SaludoServicio.js",
 "/sri-web-modulo/recursos/js/controladores/SaludoControlador.js"                                        
]
});
}
                        }
                    });

$urlRouterProvider.otherwise('sri.saludo');            
            }
    ]);



}(window.angular));