(function(angular){
     'use strict';
    angular.module('saludo').service('saludoServicio', [ "$http", "$q", function($http, $q){
            this.saludarServicio = function(){
                return " Saludo Servicio Angular";
            };
            
            this.saludarServicioRest = function(){
        	var dif = $q.defer();
        	var url = "/sri-base-interfaz/rest/Base/Saludar";
        	$http.get(url).success(
        		function(data, status){
        		    dif.resolve(data, status);
        		    console.log("Data:", data);
        		    console.log("Status: ",status);
        		   
        		}
        	).error(
        		function(data, status){
        		    dif.reject(data, status);
        		    console.log("Data: ",data);
			    console.log("Status: ",status);
			      
        		}
        	);
        	return dif.promise;
            };
    }]);
    
    
}(window.angular));


