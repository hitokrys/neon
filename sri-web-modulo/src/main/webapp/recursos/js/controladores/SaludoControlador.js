(function (angular) {
    'use strict';


    angular.module('saludo').controller('SaludoControlador', SaludoControlador);
    
    SaludoControlador.$inject = ['$scope', "saludoServicio",'$translate'  ];
    
 	
   function SaludoControlador (){
	   $scope.alerts = [];
       $scope.saludar = function () {
           $scope.alerts = [];
           var promise = saludoServicio.saludarServicioRest();
           promise.then(function(respuesta){
               $scope.saludo = "hola mundo " + saludoServicio.saludarServicio()+respuesta;
               $scope.alerts.push({type: 'success', msg: 'SAL.SALUDO.MENSAJE.EXITO'});   
           }, function(respuesta, estado){
               $scope.error = respuesta+' '+estado;
               $scope.alerts.push({type: 'danger', msg: 'SAL.SALUDO.MENSAJE.ERROR'}) ;   
           });
           
       };

       $scope.limpiar = function () {
           $scope.saludo = "";
           $scope.alerts = [];
       };
       $scope.closeAlert = function (index) {
           $scope.alerts.splice(index, 1);
       };
       
   	$scope.switchLanguage = function(key) {
   		console.log('cambia lenguaje a:'+key);
   		$scope.lenguajeActual = key;
   		$translate.use(key);
   	};	   
   }


}(window.angular));




