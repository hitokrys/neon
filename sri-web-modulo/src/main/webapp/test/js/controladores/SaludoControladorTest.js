describe("probando controlador SaludoControlador", function() {
    beforeEach(module("saludo")); // cargar el modulo app, si se encesitan mas
				// modulos se debe hacer con callback
    describe("SaludoControlador", function() {
	var scope, ctrl;
	// angular-mock permite el uso de la función inject para tener accesoa
	// los servicios y dependencias de angularjs
	// se obtine el $rootscope y el servicio $controller para simulat la
	// carga completa del SaludoControlador
	beforeEach(inject(function($rootScope, $controller) {
	    scope = $rootScope.$new();
	    ctrl = $controller("SaludoControlador", {
		$scope : scope
	    });
	}));
	// sección para cada crear las pruebas. Por cada it por cada cosa a
	// testear
	it("debe estar definida una variable hello", function() {
	    expect(scope.saludar).not.toThrow();
	})
    })
});