/**
 * @ngdoc module
 * @name sri.rutas.modulo.js 
 * @desc Módulo que proevee la configuración y carga dinámica de rutas
 * 27 de ene. de 2016
 */
(function(angular) {

	'use strict';

	angular.module('sri.rutas', [ 'ui.router' ])
	
	.run(function(Ruta) {
		Ruta.setUpRoutes();
	});

}(window.angular));