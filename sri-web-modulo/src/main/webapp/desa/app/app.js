(function(angular){
'use strict';
angular.module('sri',['pascalprecht.translate','ui.router', 'oc.lazyLoad','gettext','ngRoute','sri.rutas','ui.bootstrap']);


angular.module('sri').config(['$translateProvider','$stateProvider', '$urlRouterProvider','RutaProvider',
	function ($translateProvider,$stateProvider,$urlRouterProvider,RutaProvider) {

//<!--BEGIN:CONFIGURACION DE INTERNACIONALIZACION-->
                            $translateProvider.useLoader('$translatePartialLoader', {
                                urlTemplate: '{part}/{lang}.json'
                            });
                           // $translateProvider.useSanitizeValueStrategy('escaped');
                        //tmhDynamicLocaleProvider.localeLocationPattern("app/recursos/libs/angular-locale_{{ locale }}.js");                    

 
  $translateProvider.preferredLanguage('en');
  $translateProvider.useSanitizeValueStrategy('escaped');


//$translatePartialLoaderProvider.addPart('menu');
// //<!--END:CONFIGURACION DE INTERNACIONALIZACION-->

// CONFIGURACION DE RUTAS
$stateProvider.state('login', 
          {
             url: '/login',          
             templateUrl: 'desa/app/paginas/login.html',
             resolve:{
                cargaRecursos: function($ocLazyLoad){
                   return $ocLazyLoad.load({
                      files:[
                        'desa/app/controladores/AutenticacionControlador.js'
                      ]
                    })
                }
             }
          }).state('sri', 
          {
           url: '/sri',        
           templateUrl: 'desa/app/paginas/principal.html',
             resolve:{
                cargaRecursos: function($ocLazyLoad){
                   return $ocLazyLoad.load({
                      files:['desa/app/controladores/PrincipalControlador.js']})
                   }
                }
             }           
         );
          $urlRouterProvider.otherwise('login');
RutaProvider.setCollectionUrl('http://localhost:5000/rutas'); 

//FIN CONFIGURACION DE RUTAS
}]);


}(window.angular));