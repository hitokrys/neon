/**
 * @ngdoc provider
 * @name RutaProvider
 * @description Ruteo desde archivo json/WS. Las rutas son configuradas en dicho archivo mediante carga bajo demanda de
 *              recursos 27 de ene. de 2016
 */
(function (angular){
    'use strict';

    angular.module('sri.rutas').provider('Ruta', RutaProvider)
    /*.run(function (Ruta) {
                Ruta.setUpRoutes();
            })*/;

    RutaProvider.$inject = [ '$stateProvider' ];

    function RutaProvider ($stateProvider){

        var urlCollection;
                var estado;

                this.$get = function ($http, $state) {
                    return {
                        setUpRoutes: function () {
                            $http.get(urlCollection).success(function (collection) {
                                for (var routeName in collection) {
                                    if (collection[routeName].resolve != undefined) {
                                        estado = {
                                            name: collection[routeName].name,
                                            url: collection[routeName].url,
                                            templateUrl: collection[routeName].templateUrl,
                                            resolve:{
                                                 loadMyDirectives: function ($ocLazyLoad) {
                                                return $ocLazyLoad.load(
                                                    {
                                                      name: collection[routeName].resolve.name,
                                                      files: collection[routeName].resolve.files
                                                    })
                                                  }                                              
                                            }    
                                        }
                                    } else {
                                        estado = {
                                            name: collection[routeName].name,
                                            url: collection[routeName].url,
                                            templateUrl: collection[routeName].templateUrl,
                                        }
                                    }
                                    console.log("estado-leido:",estado);
                                    $stateProvider.state(estado, estado);
                                }
                            });
                        }
                    }
                };

                this.setCollectionUrl = function (url) {
                    urlCollection = url;
                    console.log("--urlCollection---", urlCollection);
                }

            }
            

        

    

}(window.angular));