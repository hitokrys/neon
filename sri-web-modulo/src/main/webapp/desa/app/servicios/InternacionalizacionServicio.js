(function (angular) {
	'use strict';
	/**
	 * @ngdoc Servicio
	 * @name InternacionalizacionServicio
	 * @description Este servicio actualiza el idioma actual donde el usuario final esta trabajando por las aplicaciones.
	 * @returns.
	 */
    
   	angular.module("sri").service("InternacionalizacionServicio",['$translatePartialLoader', '$translate',function($translatePartialLoader, $translate) {
    		
			this.usarIdiomaActual = function (lenguajeActual,ruta) {
				
				console.log("InternacionalizacionServicio  LNG:"+lenguajeActual+" RUTA:"+ruta);
				
				$translatePartialLoader.addPart(ruta);
				$translate.use(lenguajeActual);
				$translate.refresh();
			};
		
		}]);
    	

}(window.angular));