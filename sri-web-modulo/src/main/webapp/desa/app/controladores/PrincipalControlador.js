'use strict';
/**
 * @ngdoc controller
 * @name sbAdminApp.controller:MainController
 * @description Es el controlador inicial de la aplicación y permite inicializar valores.
 * @param $scope - ámbito
 * @param $position - 
 * @returns
 */
angular.module('sri')
  .controller('PrincipalControlador',
  	['$scope','$translate','$translatePartialLoader', '$state',
  	function($scope,$translate,$translatePartialLoader,$state)
 {

$scope.undato='Un datos';
console.log('PrincipalControlador...Hola');
//$translate.use('de');
//
//
		$translatePartialLoader.addPart('desa/recursos/lenguajes');
		$scope.lenguajeActual = 'es';

$scope.cambiaLenguaje = function (key) {
    $translate.use(key);
  };

$scope.cambiaLenguaje($scope.lenguajeActual);

$scope.login = function(){
    //alert('Alerta Login');
    console.log('Login funcion');
    $state.go('sri.dashboard');
  };   



 $scope.muestraItems = function(value){
    //alert('Alerta Login');
    //console.log('Items',$rootScope.items);
  console.log('$scope.refrescate',value);
    $scope.refrescate= !$scope.refrescate;
   
  };    

 $scope.toggleFun = function(value){
    console.log('Toggle');      
    var myEl = angular.element( document.querySelector( '#wrapper' ) );
    myEl.toggleClass("toggled");

    $scope.refrescate= !$scope.refrescate;
  };   

$scope.refrescate=false;


//$scope.toggleFun('valor');
 }]);