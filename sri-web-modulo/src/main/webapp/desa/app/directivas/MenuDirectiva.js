(function(angular) {
  angular.module('sri')
        .directive('menu', ['$http', '$q', function ($http, $q) {
                console.log("directive");
                //var urlData = "recursos/datos/prueba-menu.json";
                //var urlData = "http://localhost:8080/sri-base-web-servicios/rest/menuOpcion/json";
                var urlData = "http://localhost:5000/menu";
                console.log(" url de datos para menu: ", urlData);
                var directiveDef = {
                    restrict: 'A',
                    templateUrl: 'desa/app/paginas/parciales/menu-opciones.html',
                    link: function ($scope) {

                      console.log("  $scope.varParent:...."+$scope.varParent);

                        $http.get(urlData)
                                .success(function (data) {
                                    $scope.items = data;
                                    
                                })
                                .error(function (status, data) {
                                    $http.get(urlData).then(function (data) {
                                        $scope.items = data;
                                    })
                                });
                    },

                    controller: function ($scope, $q, $http,$rootScope) {
                        $scope.isCollapsed = false;
                        console.log(" es collapseVar:" + $scope.isCollapsed);


                        $scope.selectedMenu = 'sri-web';
                        $scope.collapseVar = 0;
                        $scope.multiCollapseVar = 0;

                        $scope.check = function (x) {
                          console.log("  item-id:"+ $scope.collapseVar+" collapseVar:" + x);
                          /*console.log("ITEMS MAIN:",$scope.itemsMain);
                           console.log("ITEMS",$scope.items);
                          $scope.items =[];

                          $scope.items = $scope.itemsMain;
                          $scope.unafuncionMain();
                          */

                         

                            if (x == $scope.collapseVar){
                                 console.log("TRUE");
                                $scope.collapseVar = 0;
                            }
                            else{
                                console.log("false");
                                $scope.collapseVar = x;
                            }
                        };

                        $scope.multiCheck = function (y) {

                            if (y == $scope.multiCollapseVar)
                                $scope.multiCollapseVar = 0;
                            else
                                $scope.multiCollapseVar = y;
                        };

                      
                    }                    
                }
                return directiveDef;
            }]);
})(window.angular);