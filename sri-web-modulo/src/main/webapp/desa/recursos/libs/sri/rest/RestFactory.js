(function(angular){
 'use strict';

var restModule=angular.module('app.rest.tools',[]);


restModule.factory('SriRestFactory', function($http, $q){

	var basePath = 'http://sri.ng.com';
	//  => http://domain.com/api/path/foo/bar

	function makeRequest(verb, uri, data){
		var defer = $q.defer();
		verb = verb.toLowerCase();

		//start with the uri
		var httpArgs = [basePath + uri];
		if (verb.match(/post|put/)){
			httpArgs.push( data );
		}

		$http[verb].apply(null, httpArgs)
		.success(function(data, status){
			defer.resolve(data);
			// update angular's scopes
			// $rootScope.$$phase || $rootScope.$apply();
		})
		.error(function(data, status){
			defer.reject('HTTP Error: ' + status);
		});

		return defer.promise;
	}

	return {
		get: function( uri ){
			return makeRequest( 'get', uri )
			.catch(function(data, status) {
 			   console.error('Gists error:', status, data);});
		}
		,post: function( uri, data ){
			return makeRequest( 'post', uri, data );
		}
		,put: function( uri, data ){
			return makeRequest( 'put', uri, data );
		}
		,delete: function( uri ){
			return makeRequest( 'delete', uri );
		}
	};

}); 


}(window.angular));