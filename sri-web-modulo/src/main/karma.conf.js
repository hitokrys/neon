// Karma configuration
// Generated on Thu Feb 11 2016 11:41:45 GMT-0500 (ECT)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: 'webapp',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      'recursos/libs/externos/angular/angular.js',      
      'recursos/libs/externos/angular-mocks/angular-mocks.js',
      'recursos/libs/externos/angular-ui-router/release/angular-ui-router.js',
      'recursos/libs/externos/angular-bootstrap/ui-bootstrap.js',
      'recursos/libs/externos/angular-translate/angular-translate.js',
      'recursos/libs/externos/angular-translate-loader-partial/angular-translate-loader-partial.js',
      'recursos/libs/externos/angular-translate-loader-partial/angular-translate-loader-partial.js',
      'recursos/libs/externos/angular-translate-loader-static-files/angular-translate-loader-static-files.js',
      'recursos/libs/externos/angular-dynamic-locale/src/tmhDynamicLocale.js',
      'recursos/js/saludo.modulo.js',
      'recursos/js/**/*.js',
      'test/js/**/*.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
	'recursos/js/**/*.js': ['jshint','coverage']
    },
    jshintPreprocessor: {
    	jshintrc: './.jshintrc',
    	stopOnError: false
    },

    coverageReporter: {//caracteristicas de archivo de cobertura
    	dir: 'test_reportes/cobertura/', 
    	reporters: [
		{
		    type: 'text-summary'
		},
    	{
    		type: 'lcov', subdir: '.'
    	},
    	{
    		type: 'cobertura', subdir: '.', 
    		file: 'cobertura.xml'
    	}
    	]
    },
    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress', 'jshint', 'junit', 'coverage'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,
    plugins: [
		'karma-chrome-launcher',
		'karma-firefox-launcher',
		'karma-jasmine',
		'karma-junit-reporter',
		'karma-coverage',
		'karma-jshint-preprocessor'
		//require('karma-jshint-sri.js')
		],
	junitReporter: { //archivo de salida junit
		outputFile: 'test_reportes/jshint/jshint.xml',
		outputFile: 'test_reportes/junit/junit.xml',
		suite: 'unit'
  },
  jshintReporter: { //archivo de salida jshint
		outputFile: 'test_reportes/jshint/jshint.xml',
		suite: 'unit'
  },
    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
