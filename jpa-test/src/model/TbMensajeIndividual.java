package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tb_mensaje_individual database table.
 * 
 */
@Entity
@Table(name="tb_mensaje_individual")
@NamedQuery(name="TbMensajeIndividual.findAll", query="SELECT t FROM TbMensajeIndividual t")
public class TbMensajeIndividual implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TbMensajeIndividualPK id;

	@Column(name="cod_simbox")
	private String codSimbox;

	private String estado;

	private String hora;

	private String mensaje;

	private String puerto;

	@Column(name="resp_envio")
	private String respEnvio;

	private String telefono;

	public TbMensajeIndividual() {
	}

	public TbMensajeIndividualPK getId() {
		return this.id;
	}

	public void setId(TbMensajeIndividualPK id) {
		this.id = id;
	}

	public String getCodSimbox() {
		return this.codSimbox;
	}

	public void setCodSimbox(String codSimbox) {
		this.codSimbox = codSimbox;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getHora() {
		return this.hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getMensaje() {
		return this.mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getPuerto() {
		return this.puerto;
	}

	public void setPuerto(String puerto) {
		this.puerto = puerto;
	}

	public String getRespEnvio() {
		return this.respEnvio;
	}

	public void setRespEnvio(String respEnvio) {
		this.respEnvio = respEnvio;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}