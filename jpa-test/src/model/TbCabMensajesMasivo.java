package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tb_cab_mensajes_masivos database table.
 * 
 */
@Entity
@Table(name="tb_cab_mensajes_masivos")
@NamedQuery(name="TbCabMensajesMasivo.findAll", query="SELECT t FROM TbCabMensajesMasivo t")
public class TbCabMensajesMasivo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codigo_cab")
	private int codigoCab;

	@Column(name="cant_proc")
	private int cantProc;

	@Column(name="cant_reg")
	private int cantReg;

	@Column(name="cod_simbox")
	private String codSimbox;

	private String estado;

	@Column(name="fecha_proc")
	private Object fechaProc;

	@Column(name="fecha_reg")
	private Object fechaReg;

	@Column(name="hora_proc")
	private String horaProc;

	@Column(name="hora_reg")
	private String horaReg;

	@Column(name="usuario_reg")
	private String usuarioReg;

	public TbCabMensajesMasivo() {
	}

	public int getCodigoCab() {
		return this.codigoCab;
	}

	public void setCodigoCab(int codigoCab) {
		this.codigoCab = codigoCab;
	}

	public int getCantProc() {
		return this.cantProc;
	}

	public void setCantProc(int cantProc) {
		this.cantProc = cantProc;
	}

	public int getCantReg() {
		return this.cantReg;
	}

	public void setCantReg(int cantReg) {
		this.cantReg = cantReg;
	}

	public String getCodSimbox() {
		return this.codSimbox;
	}

	public void setCodSimbox(String codSimbox) {
		this.codSimbox = codSimbox;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Object getFechaProc() {
		return this.fechaProc;
	}

	public void setFechaProc(Object fechaProc) {
		this.fechaProc = fechaProc;
	}

	public Object getFechaReg() {
		return this.fechaReg;
	}

	public void setFechaReg(Object fechaReg) {
		this.fechaReg = fechaReg;
	}

	public String getHoraProc() {
		return this.horaProc;
	}

	public void setHoraProc(String horaProc) {
		this.horaProc = horaProc;
	}

	public String getHoraReg() {
		return this.horaReg;
	}

	public void setHoraReg(String horaReg) {
		this.horaReg = horaReg;
	}

	public String getUsuarioReg() {
		return this.usuarioReg;
	}

	public void setUsuarioReg(String usuarioReg) {
		this.usuarioReg = usuarioReg;
	}

}