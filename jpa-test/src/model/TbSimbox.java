package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tb_simbox database table.
 * 
 */
@Entity
@Table(name="tb_simbox")
@NamedQuery(name="TbSimbox.findAll", query="SELECT t FROM TbSimbox t")
public class TbSimbox implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cod_simbox")
	private String codSimbox;

	@Column(name="cant_simcards")
	private int cantSimcards;

	@Column(name="clave_simbox")
	private String claveSimbox;

	private String estado;

	@Column(name="ip_address")
	private String ipAddress;

	private String marca;

	private String modelo;

	private String puerto;

	@Column(name="url_http_api")
	private String urlHttpApi;

	@Column(name="user_simbox")
	private String userSimbox;

	public TbSimbox() {
	}

	public String getCodSimbox() {
		return this.codSimbox;
	}

	public void setCodSimbox(String codSimbox) {
		this.codSimbox = codSimbox;
	}

	public int getCantSimcards() {
		return this.cantSimcards;
	}

	public void setCantSimcards(int cantSimcards) {
		this.cantSimcards = cantSimcards;
	}

	public String getClaveSimbox() {
		return this.claveSimbox;
	}

	public void setClaveSimbox(String claveSimbox) {
		this.claveSimbox = claveSimbox;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getMarca() {
		return this.marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return this.modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getPuerto() {
		return this.puerto;
	}

	public void setPuerto(String puerto) {
		this.puerto = puerto;
	}

	public String getUrlHttpApi() {
		return this.urlHttpApi;
	}

	public void setUrlHttpApi(String urlHttpApi) {
		this.urlHttpApi = urlHttpApi;
	}

	public String getUserSimbox() {
		return this.userSimbox;
	}

	public void setUserSimbox(String userSimbox) {
		this.userSimbox = userSimbox;
	}

}