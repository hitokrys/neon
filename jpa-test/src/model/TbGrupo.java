package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tb_grupo database table.
 * 
 */
@Entity
@Table(name="tb_grupo")
@NamedQuery(name="TbGrupo.findAll", query="SELECT t FROM TbGrupo t")
public class TbGrupo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cod_grupo")
	private String codGrupo;

	private String estado;

	@Column(name="nombre_grupo")
	private String nombreGrupo;

	public TbGrupo() {
	}

	public String getCodGrupo() {
		return this.codGrupo;
	}

	public void setCodGrupo(String codGrupo) {
		this.codGrupo = codGrupo;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getNombreGrupo() {
		return this.nombreGrupo;
	}

	public void setNombreGrupo(String nombreGrupo) {
		this.nombreGrupo = nombreGrupo;
	}

}