package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tb_det_mensajes_masivos database table.
 * 
 */
@Entity
@Table(name="tb_det_mensajes_masivos")
@NamedQuery(name="TbDetMensajesMasivo.findAll", query="SELECT t FROM TbDetMensajesMasivo t")
public class TbDetMensajesMasivo implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TbDetMensajesMasivoPK id;

	private String estado;

	private String mensaje;

	private String puerto;

	@Column(name="resp_envio")
	private String respEnvio;

	private String telefono;

	public TbDetMensajesMasivo() {
	}

	public TbDetMensajesMasivoPK getId() {
		return this.id;
	}

	public void setId(TbDetMensajesMasivoPK id) {
		this.id = id;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMensaje() {
		return this.mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getPuerto() {
		return this.puerto;
	}

	public void setPuerto(String puerto) {
		this.puerto = puerto;
	}

	public String getRespEnvio() {
		return this.respEnvio;
	}

	public void setRespEnvio(String respEnvio) {
		this.respEnvio = respEnvio;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}