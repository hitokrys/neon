package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the tb_det_mensajes_masivos database table.
 * 
 */
@Embeddable
public class TbDetMensajesMasivoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="codigo_cab")
	private int codigoCab;

	@Column(name="sec_mensaje")
	private int secMensaje;

	public TbDetMensajesMasivoPK() {
	}
	public int getCodigoCab() {
		return this.codigoCab;
	}
	public void setCodigoCab(int codigoCab) {
		this.codigoCab = codigoCab;
	}
	public int getSecMensaje() {
		return this.secMensaje;
	}
	public void setSecMensaje(int secMensaje) {
		this.secMensaje = secMensaje;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TbDetMensajesMasivoPK)) {
			return false;
		}
		TbDetMensajesMasivoPK castOther = (TbDetMensajesMasivoPK)other;
		return 
			(this.codigoCab == castOther.codigoCab)
			&& (this.secMensaje == castOther.secMensaje);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.codigoCab;
		hash = hash * prime + this.secMensaje;
		
		return hash;
	}
}