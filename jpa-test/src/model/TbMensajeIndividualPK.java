package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the tb_mensaje_individual database table.
 * 
 */
@Embeddable
public class TbMensajeIndividualPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String anio;

	private String mes;

	private String dia;

	@Column(name="sec_mensaje")
	private int secMensaje;

	@Column(name="usuario_reg")
	private String usuarioReg;

	public TbMensajeIndividualPK() {
	}
	public String getAnio() {
		return this.anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	public String getMes() {
		return this.mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getDia() {
		return this.dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public int getSecMensaje() {
		return this.secMensaje;
	}
	public void setSecMensaje(int secMensaje) {
		this.secMensaje = secMensaje;
	}
	public String getUsuarioReg() {
		return this.usuarioReg;
	}
	public void setUsuarioReg(String usuarioReg) {
		this.usuarioReg = usuarioReg;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TbMensajeIndividualPK)) {
			return false;
		}
		TbMensajeIndividualPK castOther = (TbMensajeIndividualPK)other;
		return 
			this.anio.equals(castOther.anio)
			&& this.mes.equals(castOther.mes)
			&& this.dia.equals(castOther.dia)
			&& (this.secMensaje == castOther.secMensaje)
			&& this.usuarioReg.equals(castOther.usuarioReg);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.anio.hashCode();
		hash = hash * prime + this.mes.hashCode();
		hash = hash * prime + this.dia.hashCode();
		hash = hash * prime + this.secMensaje;
		hash = hash * prime + this.usuarioReg.hashCode();
		
		return hash;
	}
}