/**
 * @ngdoc configuracion
 * @name GruntFile
 * @description Archivo de configuracion para la ejecucion de tareas de desarrollo
 * @param grunt
 */

// Funcion contenedora
module.exports = function (grunt){
	'use strict';

	// Configuracion del proyecto
	grunt.initConfig({
		wiredep : {
			target : {
				src : [ 'webapp/index.html' ]
			}
		},
		express : {
			// opciones para todos los ambientes
			options : {
				port : 3000
			},
			// opciones especificas por ambiente
			dev : {
				options : {
					script : 'server/app.js',
					debug : true
					}
			}
		},
		karma : {
			unit : {
				configFile : 'karma.conf.js'
			}
		},
	 uglify:{
			 my_target: {
			      files: [{
			          expand: true,
			          cwd: 'webapp/app/plantilla/js',			         
			          src: ['**/*.js', '!*.min.js'],
			          dest: 'webapp/app/plantilla/js',
			          ext: '.min.js'
			      }]
			    }
		 },
		jshint : {
			all : [ 'webapp/app/*.js','webapp/app/plantilla/js/**/*.js' ,  '!webapp/app/*.min.js','!webapp/app/plantilla/js/**/*.min.js'], 
			options : {
				jshintrc : './.jshintrc',
				reporter : require('jshint-html-reporter'),
				reporterOutput : 'webapp/test_reportes/jshint/jshint-reporte.html'
			}
		},

		open : {
			server : {
				// <%= express.options.port %> como scriptlet saco el puerto
				url : 'http://localhost:<%= express.options.port %>',
				// app: 'chromium-browser' // para linux
				app : 'Firefox'
			}
		},
		watch : {
			livereload : {
				files : [ 'webapp/*.html', 'webapp/app/**/*.html', 'webapp/app/plantilla/**/*.html',
						'webapp/recursos/**/*.json', 'webapp/app/*.js', 'webapp/app/plantilla/**/*.js' ],
				options : {
					livereload : true
				}
			}
		},
	
		clean : [ 'webapp/test_reportes/*','webapp/test_reportes/***/*' ,'webapp/**/test_reportes' ]

	});

	// carga de plugins
	grunt.loadNpmTasks('grunt-wiredep');
	grunt.loadNpmTasks('grunt-express-server');
	grunt.loadNpmTasks('grunt-open');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-ngdocs');
	grunt.loadNpmTasks('grunt-karma');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	// registro de tareas
	grunt.registerTask('default', [ 'uglify','wiredep', 'express', 'open', 'watch' ]);
	grunt.registerTask('limpiar', [ 'clean' ]);
	grunt.registerTask('validar', [ 'jshint' ]);
	grunt.registerTask('test', [ 'karma' ]);
	grunt.registerTask('docs', [ 'ngdocs' ]);
	grunt.registerTask('todo', [ 'wiredep', 'express', 'open', 'watch', 'clean', 'karma', 'jshint','uglify' ]);
	grunt.registerTask('construir', [ 'uglify']);

};