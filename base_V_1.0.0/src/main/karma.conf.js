/**
 * @ngdoc configuracion del karma
 * @name karma.conf.js
 * @description Describe la configuracion de karma (test runner) para la ejecucion de pruebas
 * @param config
 */
module.exports = function(config){
	config.set({
		//Path donde se resolveran los archivos 
		basePath: 'webapp',
		
		//Definicion el framework de pruebas
		frameworks: ['jasmine'],
		
		//Ubicacion de los archivo de la aplicación y al final donde almaceno las pruebas
		files: [
				'recursos/libs/externos/angular/angular.js',
				'app/*.js',
        		'app/**/*.js',
        		'app/**/**/*.js',
        		'app/**/**/**/*.js',
		        'test/**/**/**/*.js',
		        'test/**/**/*.js',
		        'test/**/*.js'],
		        
       //resultado de los test		        
		//reporters: ['progress'],
		reporters: ['junit', 'coverage'],

		//Configuración para asociar con coverage
	    preprocessors: {
		  'app/**/*.js': ['coverage'] 
		},
		//Puerto donde se levanta el servidor web
		port: 9876,
		//habilitar la salida de logs de reportes en colores
		colors: true,
		//nivel de log pueden ser: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
		logLevel: config.LOG_INFO,
		
		//Verifica si a ocurrido un cambio para volver a ejecutar las pruebas
		autoWatch: true,
		
		browsers: ['Firefox'],//Chrome
		
		//Si el navegador no captura nada en este tiempo en ms lo mata
		captureTimeout: 60000,
		
		  // Continuous Integration mode
		  //Indicamos que, después de correr los tests, si queremos que se termine el proceso entonces es true.
		singleRun: true,
		
		//Listado de plugins necesarios para Karma. Añadimos el lanzador de Chrome, Firefox y el adaptador de Jasmine.
	  plugins: [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
			'karma-junit-reporter',
            'karma-coverage',
            'karma-jshint-preprocessor'
        ],
        //Configuracion de los archivos de reporte
		junitReporter: {
            outputFile: 'test_reportes/junit/junit.xml',
            suite: 'unit'
        },
		coverageReporter: {
		 type: 'html',
            dir: 'test_reportes/coverage/',            
            reporters: [
                {type: 'lcov', subdir: '.'},
                {type: 'cobertura', subdir: '.', file: 'cobertura.xml'}
            ]
        }
		
	});
};