/**
 * @ngdoc controller
 * @name InicioControlador
 * @description Es el controlador inicial de la aplicación y permite inicializar valores.
 * @param $scope -
 *            ámbito
 * @param $translate -
 * @param $translatePartialLoader
 * @param $state
 * @returns.
 */
(function (angular){

	'use strict';

	angular.module('sri').controller('InicioControlador', InicioControlador);

	InicioControlador.$inject = [ '$scope', '$translate', '$translatePartialLoader', '$state', 'ConstanteValue' ];

	function InicioControlador ($scope,$translate,$translatePartialLoader,$state,Constante){

		$scope.messages = {
			datatable : {
				es : {
					"language" : {
						"sProcessing" : "Procesando...",
						"sLengthMenu" : "Mostrar _MENU_ registros",
						"sZeroRecords" : "No se encontraron resultados",
						"sEmptyTable" : "Ningún dato disponible en esta tabla",
						"sInfo" : "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
						"sInfoEmpty" : "Mostrando registros del 0 al 0 de un total de 0 registros",
						"sInfoFiltered" : "(filtrado de un total de _MAX_ registros)",
						"sInfoPostFix" : "",
						"sSearch" : "Buscar:",
						"sUrl" : "",
						"sInfoThousands" : ",",
						"sLoadingRecords" : "Cargando...",
						"oPaginate" : {
							"sFirst" : "<span class='glyphicon glyphicon-step-backward' aria-hidden='true'></span>",
							"sLast" : "<span class='glyphicon glyphicon-step-forward' aria-hidden='true'>",
							"sNext" : "<span class='glyphicon glyphicon-menu-right' aria-hidden='true'></span>",
							"sPrevious" : "<span class='glyphicon glyphicon-menu-left' aria-hidden='true'></span>"
						},
						"oAria" : {
							"sSortAscending" : ": Activar para ordenar la columna de manera ascendente",
							"sSortDescending" : ": Activar para ordenar la columna de manera descendente"
						}
					}
				},
				en : {
					"language" : {
						"sProcessing" : "Processing...",
						"sLengthMenu" : "Show _MENU_ entries",
						"sZeroRecords" : "No matching records found",
						"sEmptyTable" : "No data available in table",
						"sInfo" : "Showing _START_ to _END_ of _TOTAL_ entries",
						"sInfoEmpty" : "Showing 0 to 0 of 0 entries",
						"sInfoFiltered" : "(filtered from _MAX_ total entries)",
						"sInfoPostFix" : "",
						"sSearch" : "Search:",
						"sUrl" : "",
						"sInfoThousands" : ",",
						"sLoadingRecords" : "Loading...",
						"oPaginate" : {
							"sFirst" : "<span class='glyphicon glyphicon-step-backward' aria-hidden='true'></span>",
							"sLast" : "<span class='glyphicon glyphicon-step-forward' aria-hidden='true'>",
							"sNext" : "<span class='glyphicon glyphicon-menu-right' aria-hidden='true'></span>",
							"sPrevious" : "<span class='glyphicon glyphicon-menu-left' aria-hidden='true'></span>"
						},
						"oAria" : {
							"sSortAscending" : ": activate to sort column ascending",
							"sSortDescending" : ": activate to sort column descending"
						}
					}
				}
			},
			validator : {
				es : {
					required : "Este campo es obligatorio.",
					remote : "Por favor, rellena este campo.",
					email : "Por favor, escribe una dirección de correo válida.",
					url : "Por favor, escribe una URL válida.",
					date : "Por favor, escribe una fecha válida.",
					dateISO : "Por favor, escribe una fecha (ISO) válida.",
					number : "Por favor, escribe un número válido.",
					digits : "Por favor, escribe sólo dígitos.",
					creditcard : "Por favor, escribe un número de tarjeta válido.",
					equalTo : "Por favor, escribe el mismo valor de nuevo.",
					extension : "Por favor, escribe un valor con una extensión aceptada.",
					maxlength : $.validator.format("Por favor, no escribas más de {0} caracteres."),
					minlength : $.validator.format("Por favor, no escribas menos de {0} caracteres."),
					rangelength : $.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
					range : $.validator.format("Por favor, escribe un valor entre {0} y {1}."),
					max : $.validator.format("Por favor, escribe un valor menor o igual a {0}."),
					min : $.validator.format("Por favor, escribe un valor mayor o igual a {0}."),
					nifES : "Por favor, escribe un NIF válido.",
					nieES : "Por favor, escribe un NIE válido.",
					cifES : "Por favor, escribe un CIF válido."
				},
				en : {
					required : "This field is required.",
					remote : "Please fix this field.",
					email : "Please enter a valid email address.",
					url : "Please enter a valid URL.",
					date : "Please enter a valid date.",
					dateISO : "Please enter a valid date ( ISO ).",
					number : "Please enter a valid number.",
					digits : "Please enter only digits.",
					creditcard : "Please enter a valid credit card number.",
					equalTo : "Please enter the same value again.",
					maxlength : $.validator.format("Please enter no more than {0} characters."),
					minlength : $.validator.format("Please enter at least {0} characters."),
					rangelength : $.validator.format("Please enter a value between {0} and {1} characters long."),
					range : $.validator.format("Please enter a value between {0} and {1}."),
					max : $.validator.format("Please enter a value less than or equal to {0}."),
					min : $.validator.format("Please enter a value greater than or equal to {0}.")
				}
			}

		};
		// internacionalizacion
		$translatePartialLoader.addPart(Constante.DIRECTORIO_LENGUAJES);
		$scope.lenguajeActual = Constante.LENGUAJE_DEFECTO;

		$scope.switchLanguage = function (key){
			$scope.lenguajeActual = key;
			$translate.use(key);
			kendo.culture(key + '-EC'); // Para cambiar el leguaje de los widgets del kendo
			$translate.use(key); // Esto debe ser en funcion del lenguaje seleccionado
			$.extend($.validator.messages, $scope.messages.validator[key]); // Pone los textos para la validacion
			$.extend(true, $.fn.dataTable.defaults, $scope.messages.datatable[key]); // pone los textos en la tabla
			
			
			var tables = $.fn.dataTable.tables( { visible: true, api: true } );
			if (tables && tables.context.length > 0){				
				var i;
				for (i = 0; i < tables.context.length; i++) {
				    var sTableId = tables.context[i].sTableId;
				    if (sTableId){
			            var table = $('#'+sTableId).DataTable();
			            var initObject = table.init();
			            //$.extend( initObject, {"deferRender": true,"deferLoading": 0});			            
			            table.destroy(false);
			            $('#'+sTableId).empty();
			            
			            //Cambio el nombre de las columnas si es que hay las traducciones			            
			            $.each( initObject.columns, function( index, value ) {			            	
			            	if (value && value["title_"+key]){			            	
			            		$.extend( value, {"title": value["title_"+key]});
			            		$.extend( value, {"sTitle": value["title_"+key]});
			            	}
			            });
			            table = null;			            
			            $('#'+sTableId).DataTable(initObject);
				    }
			 	}
			}
			//i18n - File-Input (Jquery)
			fileInputsConi18n($scope.lenguajeActual);
			
			// de datos
		};
		// colapsar menu
		$scope.isCollapsed = false;
		$scope.getClass = function (isCollapsed){
			if (isCollapsed)
				return Constante.ESTILO_CLASE_OCULTAR_MENU;
			else
				return Constante.ESTILO_CLASE_MOSTRAR_MENU;
		};

		$scope.getClaseContenedor = function (isCollapsed){
			if (isCollapsed)
				return Constante.ESTILO_CLASE_CONTENEDOR_OPEN;
			else
				return Constante.ESTILO_CLASE_CONTENEDOR_COLAPSADO;
		};

		$scope.colapsarMenu = function (){
			$scope.isCollapsed = !$scope.isCollapsed;
		};
		$scope.login = function (){
			$state.go(Constante.RUTA_HOME);
		};
		$scope.verPerfilUsuario = true;
		$scope.mostrarPerfilUsuario = function (){

			$scope.verPerfilUsuario = !$scope.verPerfilUsuario;
			if ($scope.verPerfilUsuario) {
				$state.go(Constante.RUTA_HOME);
			} else {
				$state.go(Constante.RUTA_GENERICA);
			}
		};

		$scope.switchLanguage(Constante.LENGUAJE_DEFECTO);

	}
}(window.angular));