/**
 * @ngdoc constant
 * @name Constantes.js
 * @desc Define las constantes para los servicios y controladores genéricos para
 *       toda la aplicación
 * @restrict N/A 
 * 27 de ene. de 2016
 */
(function(angular) {
	'use strict';

	angular.module('sri').value("ConstanteValue", {
		"LENGUAJE_DEFECTO" : "es",
		"RUTA_GENERICA" : "sri-web",
		"RUTA_HOME" : "sri-web.home",
		"DIRECTORIO_LENGUAJES" : "recursos/lenguajes",
		"ESTILO_CLASE_MOSTRAR_MENU" : "sidebar-open",
		"ESTILO_CLASE_OCULTAR_MENU" : "sidebar-collapse",
		"ESTILO_CLASE_CONTENEDOR_OPEN" : "contenedor-controles-open",
		"ESTILO_CLASE_CONTENEDOR_COLAPSADO" : "contenedor-controles-collapsed",
		"WS_MENU_PRINCIPAL" : "recursos/datos/menu.json",
		"ERROR" : "danger",
		"EXITO" : "success",
		"INFO" : "info",
		"ADVERTENCIA" : "warning",
		"TIEMPO_VIGENCIA_ALERTA":17000

	});

}(window.angular));
