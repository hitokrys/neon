/**
 * @ngdoc servicio tipo factory
 * @name AlertaFactory
 * @description Permite generar una alerta generica para una pagina web
 */

(function(angular) {

	'use strict';

	angular.module('sri').factory('AlertaFactory', AlertaFactory);
	AlertaFactory.$inject = [ '$rootScope', 'ConstanteValue', '$timeout' ];

	function AlertaFactory($rootScope, Constante, $timeout) {
		var servicio = {
			agregarAlerta : function(type, message) {
				agregar(type, message, true);
			},
			agregarAlertaEstatica : function(type, message) {
				agregar(type, message, false);
			},
			cerrarAlerta : cerrarAlerta,
			cerrarAlertaIdx : cerrarAlertaIdx,
			limpiarAlerta : limpiarAlerta,
			get : get
		}, alerts = [];
		$rootScope.alerts = alerts;

		return servicio;		

		function agregar(type, msg, tiempo) {
			if(tiempo){
				$timeout(function() {
					cerrarAlertaIdx(alerts.length - 1);
				}, Constante.TIEMPO_VIGENCIA_ALERTA);
			}
			return alerts.push({
				msg : msg,
				type : type,
				cerrar : function() {
					return cerrarAlerta(this);
				}
			});
		}

		function cerrarAlerta(alert) {
			return cerrarAlertaIdx(alerts.indexOf(alert));
		}

		function cerrarAlertaIdx(index) {
			return alerts.splice(index, 1);
		}

		function limpiarAlerta() {			
			alerts.length = 0;
		}

		function get() {
			return alerts;
		}
	}
}(window.angular));
