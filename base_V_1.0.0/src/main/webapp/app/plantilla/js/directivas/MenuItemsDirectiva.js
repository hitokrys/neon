/**
 * @ngdoc directive
 * @name menuItems
 * @restrict A
 * @description Directiva por atributo que contiene la información que el menu
 *              desde un servicio REST 27 de ene. de 2016
 */
(function(angular) {

	'use strict';

	angular.module('sri').directive('menuItems', MenuItemsDirectiva);

	MenuItemsDirectiva.$inject = [ '$http', '$q', 'ConstanteValue' ];

	function MenuItemsDirectiva($http, $q, Constante) {
		var urlData = Constante.WS_MENU_PRINCIPAL;
		var directiveDef = {
			restrict : 'A',
			templateUrl : 'app/plantilla/paginas/menu-opciones.html',
			link : function($scope) {
				$http.get(urlData).success(function(data) {
					$scope.items = data;
				}).error(function() {
					console.log("Error en procesamiento de dato para carga de opciones de menu");
				});
			}
		};

		return directiveDef;
	}

}(window.angular));
