/**
 * @ngdoc directive
 * @name menu
 * @restrict E
 * @description Directiva por etiqueta que dibuja el menu del usuario logueado.
 *              27 de ene. de 2016
 */
(function(angular) {

	'use strict';

	angular.module('sri').directive('menuDirectiva', MenuDirectiva);

	MenuDirectiva.$inject = [ '$location', 'ConstanteValue','AlertaFactory' ];

	function MenuDirectiva($location, Constante, alertaFactory) {
		return {
			templateUrl : 'app/plantilla/paginas/menu.html',
			restrict : 'E',
			replace : true,
			scope : {},
			controller : function($scope) {
				$scope.isCollapsed = false;

				$scope.selectedMenu = Constante.RUTA_GENERICA;
				$scope.collapseVar = 0;
				$scope.multiCollapseVar = 0;

				$scope.check = function(x) {

					if (x === $scope.collapseVar)
						$scope.collapseVar = 0;
					else
						$scope.collapseVar = x;
				};
				$scope.limpiarAlertas = function() {
					alertaFactory.limpiarAlerta();
				};

				$scope.multiCheck = function(y) {

					if (y === $scope.multiCollapseVar)
						$scope.multiCollapseVar = 0;
					else
						$scope.multiCollapseVar = y;
				};
			}
		};
	}
}(window.angular));