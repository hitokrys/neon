(function(angular){
'use strict';
/**
 * @ngdoc sri.favoritos
 * @name Modulo Principal de Aplicaciones RI
 * @description Este módulo es el mpodulo principal de la arquitectura de las aplicaicones Web
 * # sri.favoritos
 *
 * Main module of the application.
 */

angular
  .module('sri.favoritos', [
    'oc.lazyLoad',
    'ui.router',
    'ui.bootstrap',
    'angular-loading-bar',
    'pascalprecht.translate',
    'tmh.dynamicLocale'
  ]);
}(window.angular));

