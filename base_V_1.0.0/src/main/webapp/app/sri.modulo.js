/**
 * @ngdoc module
 * @name sri
 * @description Este módulo es el modulo principal de la arquitectura de las Aplicaciones SRI permite unir todos los
 *              modulos y caracteristicas de la aplicacion para incentivar la modularizacion y patrones de separacion Se
 *              convierte en el manifiesto de que modulos definen la aplicación
 */

(function (angular){
	'use strict';

	angular.module('sri', [ 'sri.rutas', 'oc.lazyLoad', 'ui.router', 'ui.bootstrap', 'ngAnimate',
			'angular-loading-bar', 'pascalprecht.translate', 'tmh.dynamicLocale', 'gettext', 'ngFileUpload', 'ngAria',
			'angularUtils.directives.uiBreadcrumbs' ]);
	/* Configuración del módulo principal */
	angular.module('sri').config(
			[
					'$stateProvider',
					'$urlRouterProvider',
					'RutaProvider',
					'$ocLazyLoadProvider',
					'$translateProvider',
					'$translatePartialLoaderProvider',
					'tmhDynamicLocaleProvider',
					'$ariaProvider',
					function ($stateProvider,$urlRouterProvider,rutaProvider,$ocLazyLoadProvider,$translateProvider,
							$translatePartialLoaderProvider,tmhDynamicLocaleProvider,$ariaProvider){

						// Configuracion WAI-ARIA para accesibilidad
						$ariaProvider.config({
							ariaValue : true,
							tabindex : false
						});

						// <!--BEGIN:CONFIGURACION DE INTERNACIONALIZACION-->
						$translateProvider.useLoader('$translatePartialLoader', {
							urlTemplate : '{part}/{lang}.json'
						});
						$translateProvider.useSanitizeValueStrategy('escaped');
						tmhDynamicLocaleProvider
								.localeLocationPattern("app/recursos/libs/angular-locale_{{ locale }}.js");

						// <!--END:CONFIGURACION DE INTERNACIONALIZACION-->

						$ocLazyLoadProvider.config({
							debug : false,
							events : true,
						});

						$urlRouterProvider.otherwise('/login');
						rutaProvider.setCollectionUrl('recursos/datos/rutas.json');
					} ]);

	angular.module('sri').run([ '$rootScope', '$state', function ($rootScope,$state){
		$rootScope.$on("$stateNotFound", function (){
			$state.go('sri-web.no-encontrado');
		});
	} ]);
}(window.angular));
