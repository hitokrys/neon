/*
 * Clase EjemploJdbcProcessor.java	9 de Septiembre del 2015
 * 
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */

package ec.gob.sri.base.batch.test;

import static org.junit.Assert.assertEquals;

import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.BatchStatus;
import javax.batch.runtime.JobExecution;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import ec.gob.sri.base.batch.test.util.PruebasBatchUtil;

/**
 * @Version 1.0.0, 9 de Septiembre del 2015
 * 
 * @Author Edison Agurto
 */

@RunWith(Arquillian.class)
public class FuncionalidadJobTest {
	public static final Logger loguer = LoggerFactory.getLogger(FuncionalidadJobTest.class.getName());
	//private static final String DATA_CSV_TEST = "/META-INF/scripts/dataPersonaTest.csv";
	private static final String DATA_CSV_TEST = "/scripts/dataPersonaTest.csv";
	private static final String JOB_FUNCIONALIDAD1 = "jobFuncionalidad1";

	@Deployment
	public static WebArchive createDeployment() {
		WebArchive war = ShrinkWrap.create(WebArchive.class).addClass(PruebasBatchUtil.class)
				.addPackages(true, "ec.gob.sri.base.batch")
				.addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"))
				.addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("jboss-deployment-structure.xm"))
				.addAsResource("META-INF/batch-jobs/jobFuncionalidad1.xml")
				.addAsResource("META-INF/persistence.xml")
				.addAsResource("scripts/crearTablaPersona.sql")
				.addAsResource("scripts/dataPersonaTest.csv");
	
		loguer.info(war.toString(true));
		return war;
	}

	@Test
	public void pruebaFuncionalidad1Job() throws InterruptedException{
		final Properties parametrosJob = new Properties();
		parametrosJob.setProperty("resource", DATA_CSV_TEST);
		
		JobOperator operadorJob = BatchRuntime.getJobOperator();
		Long idEjecucion = operadorJob.start(JOB_FUNCIONALIDAD1, parametrosJob);
		
		JobExecution jobEjecutado = operadorJob.getJobExecution(idEjecucion);
		jobEjecutado = PruebasBatchUtil.mantenerEjecucion(jobEjecutado);

		assertEquals(jobEjecutado.getBatchStatus(), BatchStatus.COMPLETED);
	}	
}
