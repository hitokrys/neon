/*
 * Clase EjemploJdbcProcessor.java	9 de Septiembre del 2015
 * 
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */

package ec.gob.sri.base.batch.test.util;

import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.BatchStatus;
import javax.batch.runtime.JobExecution;

/**
 * @Version 1.0.0, 9 de Septiembre del 2015
 * 
 * @Author Edison Agurto
 */

public final class PruebasBatchUtil {
    private static final int INTENTOS_MAXIMOS = 10;
    private static final int TIEMPO_MILISEGUNDOS = 1000;

    private PruebasBatchUtil() {
        throw new UnsupportedOperationException();
    }
    
    public static JobExecution mantenerEjecucion(JobExecution jobEjecutado) throws InterruptedException {
        int itentos = 0;
		JobExecution ultimaEjecucion = BatchRuntime.getJobOperator()
				.getJobExecution(jobEjecutado.getExecutionId());
        while (!jobEjecutado.getBatchStatus().equals(BatchStatus.COMPLETED)) {
            if (itentos < INTENTOS_MAXIMOS) {
                itentos++;
                Thread.sleep(TIEMPO_MILISEGUNDOS);
                ultimaEjecucion = BatchRuntime.getJobOperator().getJobExecution(jobEjecutado.getExecutionId());
            } else {
                break;
            }
        }
        return ultimaEjecucion;
    }
}
