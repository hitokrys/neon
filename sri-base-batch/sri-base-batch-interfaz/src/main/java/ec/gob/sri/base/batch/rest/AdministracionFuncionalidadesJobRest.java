/*
 * Clase AdministracionFuncionalidad1JobRS.java	09/10/2015
 * 
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.base.batch.rest;

import java.util.ArrayList;
import java.util.List;

import javax.batch.operations.BatchRuntimeException;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.JobExecution;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ejb.EJB;

import ec.gob.sri.base.batch.operator.FuncionalidadesOperador;

/**
 * @Version 1.0.0, 9 de Septiembre del 2015
 * 
 * @Author Edison Agurto / mbcc010714
 */

@Path("baseBatchAdminJob")
public class AdministracionFuncionalidadesJobRest {

	@EJB
	FuncionalidadesOperador funcionalidadesOperador;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/iniciarJobFuncionalidad1")
    public List<String> iniciarJobFuncionalidad1() {
		
		JobExecution jobEjecutado = funcionalidadesOperador.iniciarJobFuncionalidad1();

		ArrayList<String> datosJobEjecutado = new ArrayList<String>();
        datosJobEjecutado.add(jobEjecutado.getBatchStatus().toString());
        datosJobEjecutado.add(jobEjecutado.getCreateTime().toString());
        datosJobEjecutado.add(new Long(jobEjecutado.getExecutionId()).toString());
        return datosJobEjecutado;
    }
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/iniciarJobFuncionalidad2")
    public List<String> iniciarJobFuncionalidad2() {		
		
		JobExecution jobEjecutado = funcionalidadesOperador.iniciarJobFuncionalidad2();
		
		ArrayList<String> datosJobEjecutado = new ArrayList<String>();
        datosJobEjecutado.add(jobEjecutado.getBatchStatus().toString());
        datosJobEjecutado.add(jobEjecutado.getCreateTime().toString());
        datosJobEjecutado.add(new Long(jobEjecutado.getExecutionId()).toString());
        return datosJobEjecutado;
    }
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/iniciarJobFuncionalidad3/{nombreArchivoDataParaProcesosBatch}")
	public List<String> iniciarJobFuncionalidad3(@PathParam("nombreArchivoDataParaProcesosBatch") String nombreArchivoDataParaProcesosBatch) {
		
		String rutaArchivosParaProcesosBatch_1 = System.getProperty("rutaArchivosDataParaProcesosBatch_1");
		if (rutaArchivosParaProcesosBatch_1 == null || rutaArchivosParaProcesosBatch_1.isEmpty()) {
	        throw new BatchRuntimeException("iniciarJobFuncionalidad3: Debe crear propiedad de ruta del archivo enviado >> nombreArchivoDataParaProcesosBatch:"+ nombreArchivoDataParaProcesosBatch + " rutaArchivosDataParaProcesosBatch_1:"+rutaArchivosParaProcesosBatch_1);	            
	    }
		String nombreArchivoDataParaProcesosBatchTmp = rutaArchivosParaProcesosBatch_1 +"\\"+ nombreArchivoDataParaProcesosBatch;
		
		JobExecution jobEjecutado = funcionalidadesOperador.iniciarJobFuncionalidad3(nombreArchivoDataParaProcesosBatchTmp);		
		ArrayList<String> datosJobEjecutado = new ArrayList<String>();
        datosJobEjecutado.add(jobEjecutado.getBatchStatus().toString());
        datosJobEjecutado.add(jobEjecutado.getCreateTime().toString());
        datosJobEjecutado.add(new Long(jobEjecutado.getExecutionId()).toString());
        return datosJobEjecutado;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/consultarEstadoJob/{idJobEjecutado}")
	public List<String> consultarEstadoFuncionalidadJob(@PathParam("idJobEjecutado") Long idJobEjecutado) {
		
		JobExecution jobEjecutado = BatchRuntime.getJobOperator().getJobExecution(idJobEjecutado);
		
		ArrayList<String> datosJobEjecutado = new ArrayList<String>();
		datosJobEjecutado.add(jobEjecutado.getBatchStatus().toString());
		datosJobEjecutado.add(jobEjecutado.getCreateTime().toString());
		datosJobEjecutado.add(new Long(jobEjecutado.getExecutionId()).toString());

		return datosJobEjecutado;
	}	
}
