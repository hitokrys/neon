/*
 * Clase Funcionalidad1Operator.java 09/09/2015
 * 
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */

package ec.gob.sri.base.batch.operator;

import java.util.Properties;

import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.JobExecution;
import javax.ejb.Singleton;
/**
 * @Version 1.0.0, 9 de Septiembre del 2015
 * 
 * @Author Edison Agurto / mbcc010714
 */
@Singleton
public class FuncionalidadesOperador {
	private static final String PARAMETRO_JOB_RECURSO_CSV = "resource";
	private static final String DATA_CSV = "/scripts/dataPersona.csv";
    private static final String JOB_FUNCIONALIDAD1 = "jobFuncionalidad1";
    private static final String JOB_FUNCIONALIDAD2 = "jobFuncionalidad2";
    
    /**
     * Este Job contiene un reader que lee un archivo .csv utilizando solo el api de File de java, 
     * el processor valida ciertos campos y el writer escribe en la base de datos mediante un adaptador para la
     * escritura o persistencia de archivos mediante jdbc. Cuya configuracion del job se encuentra en el archivo jobFuncionalidad1.xml.
     *  
     * */
    //@Schedule(minute = "*/1", hour = "*", persistent = false)
    public JobExecution iniciarJobFuncionalidad1() {
    	final Properties parametrosJob = new Properties();
    	JobOperator operadorJob = BatchRuntime.getJobOperator();
		parametrosJob.setProperty(PARAMETRO_JOB_RECURSO_CSV, DATA_CSV);    	
    	Long idEjecucion = operadorJob.start(JOB_FUNCIONALIDAD1, parametrosJob);
    	JobExecution jobEjecutado = operadorJob.getJobExecution(idEjecucion);
        
    	return jobEjecutado;
    } 
    
    /**
     * Este Job contiene un reader que lee un archivo .csv utilizando un adaptador para leer archivos .csv, 
     * el processor valida ciertos campos y el writer escribe en la base de datos mediante un adaptador para
     * archivos mediante jdbc. Adicionalmente este job tiene la configuracion para que el reader lo ejecute por partes(particiones)
     * y de esta manera el tiempo de procesamiento ira disminuyendo dependiendo del numero de particiones que se lo haga al
     * archivo. Cuya configuracion del job se encuentra en el archivo jobFuncionalidad2.xml.
     *  
     * */
    public JobExecution iniciarJobFuncionalidad2() {
    	final Properties parametrosJob = new Properties();
    	JobOperator operadorJob = BatchRuntime.getJobOperator();
		parametrosJob.setProperty(PARAMETRO_JOB_RECURSO_CSV, DATA_CSV);    	
    	Long idEjecucion = operadorJob.start(JOB_FUNCIONALIDAD2, parametrosJob);
    	JobExecution jobEjecutado = operadorJob.getJobExecution(idEjecucion);
        
    	return jobEjecutado;
    }  
    
    /**
     * Este Job es el mismo jobFuncionalidad2.xml con la diferencia de que el recurso de entrada o archivo es dinamico
     * ya que recibe como parametro el nombre del archivo a procesar enviado desde el recurso Rest.
     * Antes se debe crear la propiedad "rutaArchivosDataParaProcesosBatch_1" donde se especifica la ruta de los archivos .csv para ser procesados. 
     * Ejemplo de creación de propiedad en standalone*.xml:  
		<system-properties>
        <property name="rutaArchivosDataParaProcesosBatch_1" value="D:\ArchivosDataParaProcesosBatch_1"/>
		</system-properties>:
     *  
     * */
    public JobExecution iniciarJobFuncionalidad3(String nombreArchivoDataParaProcesosBatch) {
    	final Properties parametrosJob = new Properties();
    	JobOperator operadorJob = BatchRuntime.getJobOperator();
		parametrosJob.setProperty(PARAMETRO_JOB_RECURSO_CSV, nombreArchivoDataParaProcesosBatch);    	
    	Long idEjecucion = operadorJob.start(JOB_FUNCIONALIDAD2, parametrosJob);
    	JobExecution jobEjecutado = operadorJob.getJobExecution(idEjecucion);
        
    	return jobEjecutado;
    }     
}
