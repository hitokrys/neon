# Proyecto Base Batch según la especificación JSR-352 #

EL presente proyecto base se encuentra desarrollado para su despliegue sobre la implementación JBoss - Jberet  

Tareas que realiza el Job:  

1.- Realiza le lectura de un archivo csv llamado dataPersona.csv.  
2.- Lo procesa realizando una validación sobre la fecha y la transformación del nombre de la persona a mayúsculas.  
3.- Persiste sobre la base de datos por medio de JPA.  
  
## Funcionalidad del proyecto base:##

1.- Configuración básica de un Job con un step del tipo chunk:Uso de un Reader, Proccesor y un Writer personalizado.  
2.- Configuración de un test por medio de arquillian y ShrinkWrap.  
3.- Configuración de un servicio rest que permite ejecutar el Job desde el navegador.  
4.- Paso de parámetros desde el jobOperator hacia el reader del Job.  

## Instalación ##

#### Configuración del servidor:####
1.- Añadir el Driver ojdbc6.jar de Oracle en el servidor de aplicaciones.  
2.- Configurar el datasource respectivo a la aplicación batch a desarrollar en el archivo de configuración del servidor, con el nombre "java:/componenteDS"
```
...
<datasource jndi-name="java:/componenteDS" pool-name="componenteDS" enabled="true" statistics-enabled="true">
...
```
3. Configurar el datasource para el repositorio de jobs (job-repository) con el nombre de "RepositorioJobDS" asociado al esquema "RepositorioJob". 

4.- Adicionar la configuración del DataSource (anterior) en el subsistema de Batch.
```
subsystem xmlns="urn:jboss:domain:batch-jberet:1.0">
            <default-job-repository name="repositorioBatchModulo"/>
            <default-thread-pool name="batch"/>
            <job-repository name="in-memory">
                <in-memory/>
            </job-repository>
            <job-repository name="repositorioBatchModulo">
                <jdbc data-source="RepositorioJobDS"/>
            </job-repository>
            <thread-pool name="batch">
                <max-threads count="10"/>
                <keepalive-time time="30" unit="seconds"/>
            </thread-pool>
        </subsystem>
```  

#### Requisitos previos a la compilación.####  
1.- Crear la tabla "PERSONA" del script ubicado en el path: src/main/resources/META-INF/scripts/crearTablaPersona.sql  

#### Configuración del proyecto en Eclipse####  
1.- Instalar el plugin de maven sobre eclipse (más información documento Guia de Configuracion IDE Eclipse)  
2.- Importar el proyecto desde el repositorio SVN y luego convertirlo a Maven project (click derecho -> configure -> maven project).  
3.- Configurar el proyecto con la versión Jdk1.8.x  
4.- Levantar el servidor de aplicaciones y correr el goal desde el eclipse: maven test (Run As -> Maven Test, Maven: mvn test), el cual probará el job.  
5.- Revisar la base de datos para verificar si la tabla se llenó con datos del archivo csv ubicado en el   path:src/main/resources/scripts/dataPersonaTest.csv  

6.- Compilar y desplegar sobre el servidor de aplicaciones.   
7.- Para ejecutar el job desde el navegador se debe acceder a la siguiente url:   
   http://127.0.0.1:8080/sri-base-batch-interfaz/rest/baseBatchAdminJob/iniciarJobFuncionalidad1  
   resultado ejemplo: ["STARTING","Fri Aug 05 09:39:22 COT 2016","58"]  
  
8.- Para verificar el estado del job ejecutado ejecutar el servicio rest:  
    http://localhost:8080/5. sri-base-batch-interfaz/rest/baseBatchAdminJob/consultarEstadoJob/58 
    resultado ["STARTED","Fri Aug 05 09:39:22 COT 2016","58"]


## Librerías utilizadas##

- Test por medio de Arquillian usando JUnit, ShrinkWrap y Hamcrest   


## Consideraciones Importantes## 

- El paquete ec.gob.sri.base.batch.modelosoloejemplo solo se encuentra en el proyecto para efectos 
demostrativos del funcionamiento con JPA, las entidades de este paquete debe ser reemplazado con 
los proyectos (componentes jar del tipo model) que contienen los mapeos respectivos según su módulo de negocio.  
