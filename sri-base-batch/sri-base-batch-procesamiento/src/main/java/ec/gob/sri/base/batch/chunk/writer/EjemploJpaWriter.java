/*
 * Clase EjemploJpaWriter.java	30/9/2015
 * 
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */

package ec.gob.sri.base.batch.chunk.writer;

import java.util.List;

import javax.batch.api.chunk.AbstractItemWriter;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @Version 1.0.0, 9 de Septiembre del 2015
 * 
 * @Author Edison Agurto
 */
@Named
public class EjemploJpaWriter extends AbstractItemWriter {

	@PersistenceContext(unitName = "moduloBatchPU")
	EntityManager em;

	/**
	 * Se persite una entidad directamente sobre el componente batch
	 * solo como ejemplo, esto debe ser reemplazado por un componente 
	 * con el modelo del negocio correspondiente.
	 */
	@Override
	public void writeItems(final List<Object> items) {
		
		for (Object persona : items) {
			em.merge(persona);
		}
	}
}
