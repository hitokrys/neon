/*
 * Clase Persona.java	09/10/2015
 * 
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.base.batch.modelosoloejemplo;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 * Ejemplo: Entidad que mapea la tabla Dis_Persona.
 * 
 * @author Edison Agurto
 */
@Entity
@Table(name = "DIS_PERSONA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Persona.consultarTodos", query = "SELECT c FROM Persona c"),
    @NamedQuery(name = "Persona.consultarPorNombre", query = "SELECT c FROM Persona c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "Persona.consultarPorFechaNacimiento", query = "SELECT c FROM Persona c WHERE c.fechaNacimiento = :fechaNacimiento") })
public class Persona implements Serializable {

    @Id
    @Column(name = "CODIGO_PERSONA", unique = true, nullable = false)
    private int id;

    private static final long serialVersionUID = 1L;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 9, max = 10)
    @Column(name = "identificacion")
    private String identificacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombre")
    private String nombre;

    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_nacimiento")
    private Date fechaNacimiento;

    public Persona() {
    }

    public Persona(String nombre) {
        this.nombre = nombre;
    }

    public Persona(String nombre, Date fechaNacimiento) {
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
    }

    public Persona(int id, String identificacion, String nombre, Date fechaNacimiento) {
        this.id = id;
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (nombre == null ? 0 : nombre.hashCode());
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persona)) {
            return false;
        }
        Persona other = (Persona) object;
        return !((this.nombre == null && other.nombre != null) || (this.nombre != null && !this.nombre.equals(other.nombre)));
    }

    @Override
    public String toString() {
        return nombre + " " + identificacion;
    }

}
