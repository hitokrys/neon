/*
 * Clase EjemploCsvReader.java	30/9/2015
 * 
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */

package ec.gob.sri.base.batch.chunk.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.batch.api.BatchProperty;
import javax.batch.api.chunk.AbstractItemReader;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * @Version 1.0.0, 9 de Septiembre del 2015
 * 
 * @Author Edison Agurto
 */
@Named
public class EjemploCsvReader extends AbstractItemReader {

	private BufferedReader lector;

	@Inject
	@BatchProperty
	protected String resource;

	@Override
	public void open(Serializable checkpoint) {
		lector = new BufferedReader(new InputStreamReader(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(resource)));
	}

	@Override
	public String readItem() {
		try {
			return lector.readLine();
		} catch (IOException ex) {
			Logger.getLogger(EjemploCsvReader.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}
}
