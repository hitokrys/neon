/*
 * Clase EjemploProcessor.java	30/9/2015
 * 
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */

package ec.gob.sri.base.batch.chunk.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

import javax.batch.api.chunk.ItemProcessor;
import javax.inject.Named;

import ec.gob.sri.base.batch.modelosoloejemplo.Persona;

/**
 * @Version 1.0.0, 9 de Septiembre del 2015
 * 
 * @Author Edison Agurto
 */
@Named
public class EjemploProcessor implements ItemProcessor {
	public static final Logger loguer = LoggerFactory.getLogger(EjemploProcessor.class.getName());
	private static int id = 1;
	private final SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	public Persona processItem(Object item) {
		String cadenaLista= item.toString();
		loguer.info("item procesado >>> "+cadenaLista);//NOPMD

		StringTokenizer tokens = new StringTokenizer(cadenaLista, ",");
		Persona persona = new Persona();
		persona.setId(id++);
		persona.setIdentificacion(tokens.nextToken());
		persona.setNombre(tokens.nextToken());
		Date fechaNacimiento = null;

		try {
			String fechaNacimientoCadena  = tokens.nextToken();
			formatoFecha.setLenient(false);
			fechaNacimiento = formatoFecha.parse(fechaNacimientoCadena);
		} catch (ParseException parseException) {
			loguer.error(parseException.toString());
		}
		persona.setFechaNacimiento(fechaNacimiento);

		return persona;
	}
}
