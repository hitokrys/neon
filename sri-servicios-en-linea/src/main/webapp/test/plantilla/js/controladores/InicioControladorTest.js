/**
 * http://usejsdoc.org/
 */


describe("controladores.InicioControladorTest", function() {
	// cargar el modulo app, si se encesitan mas modulos se debe hacer con callback
	beforeEach(module("sri"));
	
	describe("InicioControlador", function() {
		var scope, ctrl;
		// angular-mock permite el uso de la función inject para tener acceso a
		// los servicios y dependencias de angularjs
		// se obtiene el $rootscope y el servicio $controller para simular la
		// carga completa del InicioControlador
		beforeEach(inject(function($rootScope, $controller) {
			scope = $rootScope.$new();
			ctrl = $controller("InicioControlador", {				
				$scope : scope
			});
		}));
		// sección para cada crear las pruebas. Por cada it por cada cosa a testear
		
		it ("debe devolver estilo responsive", function() {
			expect(scope.getClassResponsiveBar("/true")).toEqual('responsive');
		});

		it ("ejecucion de funcion buscarCELRecibidos() por tipo de comprobante", function() {
			scope.cel.tipoComprobante = "1";
			expect(scope.buscarCELRecibidos).not.toThrow();
		});
		/*it ("ejecucion de funcion buscarCELRecibidos() busca por RUC/Cedula/Pasaporte y por tipo de comprobante", function() {
			scope.radioToFind.nombre = "DOC";
			scope.radioToFind.valor = "1791925165001\nADMINELI CIA. LTDA";
			expect(scope.buscarCELRecibidos).not.toThrow();
		});
		it ("ejecucion de funcion buscarCELRecibidos() busca por Clave de acceso y por tipo de comprobante", function() {
			scope.radioToFind.nombre = "CLA";
			scope.radioToFind.valor = "CA:200620150117919251650012007";
			expect(scope.buscarCELRecibidos).not.toThrow();
		});
		it ("ejecucion de funcion buscarCELRecibidos() busca por Nro autorizacion y por tipo de comprobante", function() {
			scope.radioToFind.nombre = "AUT";
			scope.radioToFind.valor = "0010001360562012301910\nNA:3006201515593917919251650016761241326";
			expect(scope.buscarCELRecibidos).not.toThrow();
		});
		
		it ("ejecucion de funcion changeTipoFecha()", function() {
			expect(scope.changeTipoFecha).toThrow();
		});*/
	})
});
