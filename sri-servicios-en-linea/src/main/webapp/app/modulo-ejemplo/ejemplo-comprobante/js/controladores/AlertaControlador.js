/**
 * @ngdoc controller
 * @name AlertaControlador
 * @description Controlador de la pagina de formulario
 */
(function(angular) {

	'use strict';

	angular.module('sri').controller('AlertaControlador', AlertaControlador);
	
	

	AlertaControlador.$inject = [ '$scope', '$http', 'AlertaFactory', 'ConstanteValue' ];

	function AlertaControlador($scope, $http, alertaServicio, Constante, AgregarAlertaService) {
		alertaServicio.agregarAlerta(Constante.ADVERTENCIA, "Mensaje de advertencia");
		alertaServicio.agregarAlerta(Constante.INFO, "Mensaje de información");
		alertaServicio.agregarAlerta(Constante.ERROR, "Mensaje de error");
		alertaServicio.agregarAlerta(Constante.EXITO, "Exito total");
		
		

		
		$scope.generarAlerta = function() {
			//alertaServicio.eliminarAlerta();
			alertaServicio.agregarAlerta(Constante.EXITO, "Mensaje exito agregado");
		}
		
		
	}

}(window.angular));