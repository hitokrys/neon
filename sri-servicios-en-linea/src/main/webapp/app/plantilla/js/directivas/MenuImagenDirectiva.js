/**
 * @ngdoc directive
 * @name menu
 * @restrict E
 * @description Directiva por etiqueta que dibuja el menu del usuario logueado.
 *              27 de ene. de 2016
 */
(function(angular) {

	'use strict';
	
	angular.module('sri').directive('menuImagen', MenuImagenDirectiva);

	MenuImagenDirectiva.$inject = [ '$http', '$location', 'ConstanteValue','AlertaFactory', 'MenuPerfilFactory'];

	function MenuImagenDirectiva($http,$location, Constante, alertaFactory, menuPerfilFactory) {
		var urlData = Constante.WS_OPCIONES_AVATAR;
		return {
			templateUrl : 'app/plantilla/paginas/cuenta/modal-avatar.html',
			restrict : 'E',
			replace : true,
			controller: 'ConfiguracionCuentaControlador',
			controllerAs: 'configuracionCuentaCtrl',
			scope : {},
			link : function(scope, element, attr, ctrl) {
				scope.opcionesPerfil = menuPerfilFactory;
				
				$http.get(urlData).success(function(data) {
					scope.listaAvatar = data;						
				}).error(function() {
					console.log("Error en procesamiento de dato para carga de avatars");
				});
				
			
				scope.selectedImage = function(avatar){
					scope.opcionesPerfil.imagenPerfil = avatar;
				};
				
		        
			}
		};
	}
}(window.angular));