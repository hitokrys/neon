/**
 * @ngdoc directive
 * @name menu
 * @restrict E
 * @description Directiva por etiqueta que dibuja el menu del usuario logueado.
 *              27 de ene. de 2016
 */
(function(angular) {

	'use strict';

	angular.module('sri').directive('menuDirectiva', MenuDirectiva);

	MenuDirectiva.$inject = [ '$location', 'ConstanteValue','AlertaFactory', 'FiltroMenuFactory', '$window' ];

	function MenuDirectiva($location, Constante, alertaFactory, filtroMenuFactory,$window) {
		return {
			templateUrl : 'app/plantilla/paginas/menu.html',
			restrict : 'E',
			controller: 'InicioControlador',
			controllerAs: 'inicioCtrl',
			replace : true,
			link : function(scope) {
				scope.isCollapsed = false;

				scope.model = filtroMenuFactory;
				scope.selectedMenu = Constante.RUTA_GENERICA;
				scope.model.collapseVar = 0;
				scope.multiCollapseVar = 0;
				
				scope.check = function(x) {

					if (x === scope.model.collapseVar)
						scope.model.collapseVar = 0;
					else
						scope.model.collapseVar = x;
				};
				scope.limpiarAlertas = function() {
					alertaFactory.limpiarAlerta();
				};

				scope.multiCheck = function(y) {

					if (y === scope.multiCollapseVar)
						scope.multiCollapseVar = 0;
					else
						scope.multiCollapseVar = y;
				};
				
				//metodo que colapsa el menu cuando se da click sobre alguna opción del menu de botones y el tamaño de la pantalla es >768
				scope.colapsarMenu = function (){
					if(scope.model.noneStyle && $window.innerWidth>768){
						scope.model.isCollapsed = !scope.model.isCollapsed;
						scope.model.noneStyle = false;
					}
				};
			}
		};
	}
}(window.angular));