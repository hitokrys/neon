/** @ngdoc directive
 * @name menuNotificaciones
 * @restrict A
 * @description Directiva que controla la carga de notificaciones del menu derecho.
 *              02 de mayo de 2016
 */

(function(angular) {

	'use strict';

	angular.module('sri').directive('menuNotificaciones', MenuNotificacionesDirectiva);

	MenuNotificacionesDirectiva.$inject = [ '$http', '$q', 'ConstanteValue', 'NotificacionesFactory'];

	function MenuNotificacionesDirectiva($http, $q, Constante, notificacionesFactory) {
		var urlData = Constante.WS_MENU_NOTIFICACIONES;
		return{
			restrict : 'A',
			controller: 'InicioControlador',
			controllerAs: 'inicioCtrl',
			link : function(scope,elem,att) {
				$http.get(urlData).success(function(data) {
					scope.numeroNotificaciones = notificacionesFactory;
					scope.items = data;
					scope.numeroNotificaciones.numero = data.length;
				}).error(function() {
					console.log("Error en procesamiento de datos para la carga de notificaciones");
				});

			},
			templateUrl : 'app/plantilla/paginas/menu-notificaciones.html'
		}
	}

}(window.angular));