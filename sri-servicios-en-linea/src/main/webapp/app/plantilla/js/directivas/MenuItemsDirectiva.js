/**
 * @ngdoc directive
 * @name menuItems
 * @restrict A
 * @description Directiva por atributo que contiene la información que el menu
 *              desde un servicio REST 27 de ene. de 2016
 */
(function(angular) {

	'use strict';

	angular.module('sri').directive('menuItems', MenuItemsDirectiva);

	MenuItemsDirectiva.$inject = [ '$http', '$q', 'ConstanteValue', 'FiltroMenuFactory' ];

	function MenuItemsDirectiva($http, $q, Constante, filtroMenuFactory) {
		var urlData = Constante.WS_MENU_PRINCIPAL;
		return{
			restrict : 'A',
			controller: 'InicioControlador',
			controllerAs: 'inicioCtrl',
			link : function(scope,elem,att) {
				scope.model = filtroMenuFactory;
				$http.get(urlData).success(function(data) {
					scope.items = data;						
				}).error(function() {
					console.log("Error en procesamiento de dato para carga de opciones de menu");
				});
				
				scope.showDetail = function (item) {
				    if (scope.model.active != item) {
				      scope.model.active = item;
				    }
				    else {
				      scope.model.active = null;
				    }
				  };

			},
			templateUrl : 'app/plantilla/paginas/menu-opciones.html'
		}
	}

}(window.angular));