/**
 * @ngdoc directive
 * @name menuPerfil
 * @restrict A
 * @description Directiva por atributo que contiene la información que el menu perfil
 *              desde un servicio REST 23 de jun. de 2016
 */
(function(angular) {

	'use strict';

	angular.module('sri').directive('menuPerfil', MenuPerfilDirectiva);

	MenuPerfilDirectiva.$inject = [ '$http', '$q', 'ConstanteValue', 'MenuPerfilFactory' ];

	function MenuPerfilDirectiva($http, $q, Constante, menuPerfilFactory) {
		var urlDataPerfil = Constante.WS_MENU_PERFIL;
		var urlDataFavoritos= Constante.WS_MENU_FAVORITOS;
		var urlDataRuc = Constante.WS_MENU_RUC;
		return{
			restrict : 'A',
			controller: 'ConfiguracionCuentaControlador',
			controllerAs: 'configuracionCuentaCtrl',
			link : function(scope,elem,att) {
				scope.opcionesPerfil = menuPerfilFactory;
				scope.collapseVarFavorito = 0;
				
				$http.get(urlDataPerfil).success(function(data) {
					scope.items = data;						
				}).error(function() {
					console.log("Error en procesamiento de dato para carga de opciones del menu perfil");
				});
				
				$http.get(urlDataFavoritos).success(function(dataFavoritos) {
					scope.itemsFavoritos = dataFavoritos;						
				}).error(function() {
					console.log("Error en procesamiento de dato para carga de opciones del menu favoritos");
				});
				
				$http.get(urlDataRuc).success(function(dataRuc) {
					scope.itemsRuc = dataRuc;						
				}).error(function() {
					console.log("Error en procesamiento de dato para carga de opciones del menu Ruc");
				});
				
				
				scope.checkFavorito = function(x) {

					if (x === scope.collapseVarFavorito)
						scope.collapseVarFavorito = 0;
					else
						scope.collapseVarFavorito = x;
				};
				
				scope.active = null;
				
				scope.showDetailFavorito = function (item) {
				    if (scope.active != item) {
				      scope.active = item;
				    }
				    else {
				      scope.active = null;
				    }
				  };
	                    
	                         
	             $("#itemsMenuPerfil").find("a").click(false);

	            
				  
			},
			templateUrl : 'app/plantilla/paginas/menu-perfil.html'
		}
	}

}(window.angular));