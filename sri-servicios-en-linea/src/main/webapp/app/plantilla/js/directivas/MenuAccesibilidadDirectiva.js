/** @ngdoc directive
 * @name menuAccesibilidad
 * @restrict E
 * @description Directiva que controla el menu de accesibilidad.
 *              26 de mayo de 2016
 */

(function(angular) {

	'use strict';

	angular.module('sri').directive('menuAccesibilidad', MenuAccesibilidadDirectiva);

	MenuAccesibilidadDirectiva.$inject = [ '$location', 'ConstanteValue', 'FiltroMenuFactory'];

	function MenuAccesibilidadDirectiva($location, Constante, filtroMenuFactory) {
		return {
			templateUrl : 'app/plantilla/paginas/menu-accesibilidad.html',
			restrict : 'E',
			replace : true,
			scope : {},
			controller: 'InicioControlador',
			controllerAs: 'inicioCtrl',
			link : function(scope) {
				scope.model = filtroMenuFactory;
				
				
				// Funcionalidad de accesibilidad aumenta y disminuye el tamaniode la letra de la  pagina
				scope.total = 1;
				scope.sizeFont = 0.1;
				scope.model.total = 1;
				scope.model.totalEtiquetas = 1;
				
				//aumenta el tamanio de los botones
				
				scope.model.height = 32;
				scope.sizeHeigth = 5;
				scope.width = 105;
				scope.sizeWidth = 20;
				
				//aumenta el tamanio cajas de texto
				scope.model.widthText = 100;
				scope.sizeWidthText = 10;
				
				//mover a la derecha
				
				scope.model.left = 0;
				scope.ziseLeft = 23;
				
				//mover abajo
				
				scope.top = 0;
				scope.model.top=0;
				scope.ziseTop= 5;

				scope.increase = function (){

					if (scope.total <= 1.9) {
						scope.total += parseFloat(scope.sizeFont);
						scope.model.total += parseFloat(scope.sizeFont);
						scope.model.totalNumero = scope.total;
					}
					if (scope.total <= 1.3 && scope.total >=1.2)
						{
						scope.model.top += parseFloat(6);
						}
					if (scope.total <= 1.4 && scope.total >=1.3)
					{
					scope.model.top += parseFloat(7);
					}
					if (scope.total <= 1.6 && scope.total >1.4)
					{
					scope.model.top += parseFloat(10);
					}
					
					if (scope.total <= 1.8 && scope.total >1.6)
					{
					scope.model.top += parseFloat(11);
					}
					if (scope.total >1.9 && scope.total < 1.9 )
					{
						
					scope.model.top += parseFloat(20);
					}
					if(scope.model.totalEtiquetas<=1.5)
						{
						scope.model.totalEtiquetas += parseFloat(scope.sizeFont);
						scope.model.height += parseFloat(scope.sizeHeigth);
						scope.width += parseFloat(scope.sizeWidth);
						scope.model.widthText += parseFloat(scope.sizeWidthText);
						scope.model.left += parseFloat(scope.ziseLeft);
						scope.top += parseFloat(scope.ziseTop);
						}
				};

				scope.decrease = function (){
					if (scope.total >= 1.1) {
						scope.total -= parseFloat(scope.sizeFont);
						scope.model.total -= parseFloat(scope.sizeFont);
						scope.model.top -= parseFloat(scope.ziseTop);
					}
					if(scope.model.totalEtiquetas >=1.1)
					{
					scope.model.totalEtiquetas -= parseFloat(scope.sizeFont);
					scope.model.height -= parseFloat(scope.sizeHeigth);
					scope.width -= parseFloat(scope.sizeWidth);
					scope.model.widthText -= parseFloat(scope.sizeWidthText);
					scope.model.left -= parseFloat(scope.ziseLeft);
					scope.top -= parseFloat(scope.ziseTop);
					}
				};
				
				// cambio de estilo, colores de accesibilidad

				scope.model.estilo = {
					color : "",
					backgroundColor : ''

				};

				scope.estiloMenu = function (){
					scope.model.colores = scope.model.estilo.color;
					scope.model.background = scope.model.estilo.backgroundColor;

				};
				
				scope.getClassAccesibilidadLogin = function (isAccesibilidadLogin){
					if(isAccesibilidadLogin)
						return Constante.ESTILO_CLASE_MENU_ACCESIBILIDAD_LOGIN;
				};
				
				scope.getClassAccesibilidadSubMenuLogin = function (isAccesibilidadLogin){
					if(isAccesibilidadLogin)
						return Constante.ESTILO_CLASE_SUBMENU_ACCESIBILIDAD_LOGIN;
				};
		
			}
		};
		
	}

}(window.angular));