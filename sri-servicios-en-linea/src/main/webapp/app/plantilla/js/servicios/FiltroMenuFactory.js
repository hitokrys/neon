/**
 * @ngdoc factory
 * @name FiltroMenuFactory.js 
 * @desc Permite administrar los parametros del menu
 * 8 de jun. de 2016
 */

(function(angular) {

	'use strict';
	
	angular.module('sri').factory('FiltroMenuFactory', function (){
		var modeloBusqueda = {
				buscaMenu : '',
				collapseVar : '0',
		        active:'',
		        disabled:'false',
		        isCollapsed: 'false',
		        noneStyle: 'false',
		        isVisible:'false',
		        isCollapsedBar: 'false'
			};
	
		return modeloBusqueda;
	});
}(window.angular));