/**
 * @ngdoc factory
 * @name MenuPerfilFactory.js 
 * @desc Mostrar informacion de relacionada con la configuracion del menu de perfil
 * 21 de jun. de 2016
 */

(function(angular) {

	'use strict';
	
	angular.module('sri').factory('MenuPerfilFactory', function (){
		var opcionesPerfil = {
				isCollapsedPerfil: 'false',
		        isCollapsedRuc: 'false',
		        isCollapsedFavorito: 'false',
		        imagenPerfil: ''
			};
	
		return opcionesPerfil;
	});
}(window.angular));