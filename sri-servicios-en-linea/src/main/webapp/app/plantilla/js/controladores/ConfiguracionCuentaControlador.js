/**
 * http://usejsdoc.org/
 */


(function (angular){

	'use strict';

	 angular.module('sri').controller('ConfiguracionCuentaControlador', ConfiguracionCuentaControlador);
	
	 ConfiguracionCuentaControlador.$inject = [ 'ConstanteValue', 'MenuPerfilFactory'];

	 function ConfiguracionCuentaControlador (Constante, menuPerfilFactory){
		 
		 var vm = this;
		 vm.opcionesPerfil = menuPerfilFactory;
		 vm.opcionesPerfil.isCollapsedPerfil = false;
		 vm.opcionesPerfil.isCollapsedRuc = false;
		 vm.opcionesPerfil.isCollapsedFavorito = false;
		
		 

		 $("#datePicker").kendoDatePicker({
     	    close: function() {
     	       console.log("closed datePicker");   
     	    }
     	});
     	    
     	var two = $("#datePicker2");

     	two.kendoDatePicker({});

     	 
		    vm.colapsarMenuPerfil = function (){
				vm.opcionesPerfil.isCollapsedPerfil = !vm.opcionesPerfil.isCollapsedPerfil;
			    vm.opcionesPerfil.isCollapsedRuc = false;
			    vm.opcionesPerfil.isCollapsedFavorito = false;
			};   
			
			vm.getClassResponsiveMenuConf = function (isCollapsedMenu){
				if (isCollapsedMenu)
					return Constante.ESTILO_CLASE_MOSTRAR_ESTILO_RESPONSIVE;
			};
			
			vm.colapsarMenuRuc = function (){
				vm.opcionesPerfil.isCollapsedRuc = !vm.opcionesPerfil.isCollapsedRuc;	
				vm.opcionesPerfil.isCollapsedPerfil = false;
			    vm.opcionesPerfil.isCollapsedFavorito = false;
			};  
			
			
			vm.colapsarMenuFavorito = function (){
				vm.opcionesPerfil.isCollapsedFavorito = !vm.opcionesPerfil.isCollapsedFavorito;
				vm.opcionesPerfil.isCollapsedPerfil = false;
			    vm.opcionesPerfil.isCollapsedRuc = false;
			};   
		    
			
			vm.opcionesPerfil.imagenPerfil = {
					src : Constante.RUTA_IMAGEN_PERFIL_DEFAULT
			};
     	
     	
}
}(window.angular));