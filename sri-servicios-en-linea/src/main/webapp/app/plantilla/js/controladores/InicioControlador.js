/**
 * @ngdoc controller
 * @name InicioControlador
 * @description Es el controlador inicial de la aplicación y permite inicializar valores.
 * @param $scope - 
 *            ámbito
 * @param $translate -
 * @param $translatePartialLoader
 * @param $state
 * @returns.
 */
(function(angular) {

	'use strict';

	angular.module('sri').controller('InicioControlador', InicioControlador);

	InicioControlador.$inject = [ '$translate', '$translatePartialLoader', '$state', 'ConstanteValue', 'FiltroMenuFactory' ];

	function InicioControlador ($translate,$translatePartialLoader,$state,Constante,filtroMenuFactory){
		var vm = this;

		vm.messages = {
			datatable : {
				es : {
					"language" : {
						"sProcessing" : "Procesando...",
						"sLengthMenu" : "Mostrar _MENU_ registros",
						"sZeroRecords" : "No se encontraron resultados",
						"sEmptyTable" : "Ningún dato disponible en esta tabla",
						"sInfo" : "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
						"sInfoEmpty" : "Mostrando registros del 0 al 0 de un total de 0 registros",
						"sInfoFiltered" : "(filtrado de un total de _MAX_ registros)",
						"sInfoPostFix" : "",
						"sSearch" : "Buscar:",
						"sUrl" : "",
						"sInfoThousands" : ",",
						"sLoadingRecords" : "Cargando...",
						"oPaginate" : {
							"sFirst" : "<span class='glyphicon glyphicon-step-backward' aria-hidden='true'></span>",
							"sLast" : "<span class='glyphicon glyphicon-step-forward' aria-hidden='true'>",
							"sNext" : "<span class='glyphicon glyphicon-menu-right' aria-hidden='true'></span>",
							"sPrevious" : "<span class='glyphicon glyphicon-menu-left' aria-hidden='true'></span>"
						},
						"oAria" : {
							"sSortAscending" : ": Activar para ordenar la columna de manera ascendente",
							"sSortDescending" : ": Activar para ordenar la columna de manera descendente"
						}
					}
				},
				en : {
					"language" : {
						"sProcessing" : "Processing...",
						"sLengthMenu" : "Show _MENU_ entries",
						"sZeroRecords" : "No matching records found",
						"sEmptyTable" : "No data available in table",
						"sInfo" : "Showing _START_ to _END_ of _TOTAL_ entries",
						"sInfoEmpty" : "Showing 0 to 0 of 0 entries",
						"sInfoFiltered" : "(filtered from _MAX_ total entries)",
						"sInfoPostFix" : "",
						"sSearch" : "Search:",
						"sUrl" : "",
						"sInfoThousands" : ",",
						"sLoadingRecords" : "Loading...",
						"oPaginate" : {
							"sFirst" : "<span class='glyphicon glyphicon-step-backward' aria-hidden='true'></span>",
							"sLast" : "<span class='glyphicon glyphicon-step-forward' aria-hidden='true'>",
							"sNext" : "<span class='glyphicon glyphicon-menu-right' aria-hidden='true'></span>",
							"sPrevious" : "<span class='glyphicon glyphicon-menu-left' aria-hidden='true'></span>"
						},
						"oAria" : {
							"sSortAscending" : ": activate to sort column ascending",
							"sSortDescending" : ": activate to sort column descending"
						}
					}
				}
			},
			validator : {
				es : {
					required : "Este campo es obligatorio.",
					remote : "Por favor, rellena este campo.",
					email : "Por favor, escribe una dirección de correo válida.",
					url : "Por favor, escribe una URL válida.",
					date : "Por favor, escribe una fecha válida.",
					dateISO : "Por favor, escribe una fecha (ISO) válida.",
					number : "Por favor, escribe un número válido.",
					digits : "Por favor, escribe sólo dígitos.",
					creditcard : "Por favor, escribe un número de tarjeta válido.",
					equalTo : "Por favor, escribe el mismo valor de nuevo.",
					extension : "Por favor, escribe un valor con una extensión aceptada.",
					maxlength : $.validator.format("Por favor, no escribas más de {0} caracteres."),
					minlength : $.validator.format("Por favor, no escribas menos de {0} caracteres."),
					rangelength : $.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
					range : $.validator.format("Por favor, escribe un valor entre {0} y {1}."),
					max : $.validator.format("Por favor, escribe un valor menor o igual a {0}."),
					min : $.validator.format("Por favor, escribe un valor mayor o igual a {0}."),
					nifES : "Por favor, escribe un NIF válido.",
					nieES : "Por favor, escribe un NIE válido.",
					cifES : "Por favor, escribe un CIF válido."
				},
				en : {
					required : "This field is required.",
					remote : "Please fix this field.",
					email : "Please enter a valid email address.",
					url : "Please enter a valid URL.",
					date : "Please enter a valid date.",
					dateISO : "Please enter a valid date ( ISO ).",
					number : "Please enter a valid number.",
					digits : "Please enter only digits.",
					creditcard : "Please enter a valid credit card number.",
					equalTo : "Please enter the same value again.",
					maxlength : $.validator.format("Please enter no more than {0} characters."),
					minlength : $.validator.format("Please enter at least {0} characters."),
					rangelength : $.validator.format("Please enter a value between {0} and {1} characters long."),
					range : $.validator.format("Please enter a value between {0} and {1}."),
					max : $.validator.format("Please enter a value less than or equal to {0}."),
					min : $.validator.format("Please enter a value greater than or equal to {0}.")
				}
			}
		};
		// internacionalizacion
		$translatePartialLoader.addPart(Constante.DIRECTORIO_LENGUAJES);
		vm.lenguajeActual = Constante.LENGUAJE_DEFECTO;

		vm.switchLanguage = function(key) {
			vm.lenguajeActual = key;
			$translate.use(key);
			kendo.culture(key + '-EC'); // Para cambiar el leguaje de los widgets del kendo
			$translate.use(key); // Esto debe ser en funcion del lenguaje seleccionado
			$.extend($.validator.messages, vm.messages.validator[key]); // Pone los textos para la validacion
			$.extend(true, $.fn.dataTable.defaults, vm.messages.datatable[key]); // pone los textos en la tabla
		// de datos
		};
		// colapsar menu
		vm.model = filtroMenuFactory; //pasa parametros a la directiva del menu
		vm.model.isCollapsed = false;
		vm.model.isCollapsedBar = false; //colapsa barra derecha de botones
		vm.model.noneStyle = false; //menu de botones
		vm.botonMenuCollapsed = true; //boton derecho menu
		vm.chevronLeft = false; //flecha de menu hamburguesa
		vm.model.disabled = false; //deshabilita click en botones menu colapsado


		vm.verPerfilUsuario = true;

		vm.model.isVisible = false;
		vm.model.isAccesibilidadLogin = false;

		vm.getClass = function(isCollapsed) {
			if (isCollapsed)
				return Constante.ESTILO_CLASE_OCULTAR_MENU;
			else
				return Constante.ESTILO_CLASE_MOSTRAR_MENU;
		};

		//Metodo que devuelve el estilo responsive para ajustar los estilos de algunos componentes en base al menu principal
		vm.getClassResponsive = function(isCollapsed) {
			if (isCollapsed)
				return Constante.ESTILO_CLASE_MOSTRAR_ESTILO_RESPONSIVE;
		};

		vm.getClaseContenedor = function(isCollapsed) {
			if (isCollapsed)
				return Constante.ESTILO_CLASE_CONTENEDOR_OPEN;
			else
				return Constante.ESTILO_CLASE_CONTENEDOR_COLAPSADO;
		};

		vm.colapsarMenu = function() {
			vm.model.isCollapsed = !vm.model.isCollapsed;
			vm.model.buscaMenu = "";
			vm.model.collapseVar = 0;
			vm.model.active = null;
			vm.model.noneStyle = !vm.model.noneStyle;
			vm.botonMenuCollapsed = !vm.botonMenuCollapsed;
			vm.chevronLeft = !vm.chevronLeft;
			vm.model.disabled = !vm.model.disabled;
		};

		vm.login = function(ruc,clave) {
			if (ruc && ruc == '1234567890001' && clave == '12345') {
				$state.go(Constante.RUTA_HOME);
			}
		};
		
		vm.mostrarPerfilUsuario = function (){

			vm.verPerfilUsuario = !vm.verPerfilUsuario;
			if (vm.verPerfilUsuario) {
				$state.go(Constante.RUTA_HOME);
			} else {
				$state.go(Constante.RUTA_GENERICA);
			}
		};

		vm.switchLanguage(Constante.LENGUAJE_DEFECTO);

		//Colapsar Barra derecha
		vm.colapsarBarra = function() {
			//busca el estilo de la lista para cambiar la visualizacion
			// document.getElementsByClassName("navbar-top-links")[0].classList.toggle("responsive");
			vm.model.isCollapsedBar = !vm.model.isCollapsedBar;
			var fadeContent = $('.fade-content');
			var bodyContent =  $('body');
			fadeContent.hide();
			if (vm.model.isCollapsedBar){
				 bodyContent.animate({left: "-250px"}, 400).css({"overflow":"hidden"});
				fadeContent.fadeIn();
			}
			else{
				  bodyContent.animate({left: "0"}, 400).css({"overflow":"scroll"});
				  fadeContent.fadeOut();
			 }
		};


		//Metodo que devuelve el estilo responsive para ajustar los estilos de algunos componentes en base al menu derecho
		vm.getClassResponsiveBar = function(isCollapsedBar) {
			if (isCollapsedBar)
				return Constante.ESTILO_CLASE_MOSTRAR_ESTILO_RESPONSIVE;
		};

		// show and hide div
		vm.ShowHide = function(isAccesibilidadLogin) {
			vm.model.isVisible = vm.model.isVisible ? false : true;
			vm.model.isAccesibilidadLogin = isAccesibilidadLogin;
		};

		vm.showHideResponsive = function() {
			vm.model.isVisible = false;
		};

		//deshabilita click sobre menu colapsado
		 vm.model.isDisabled = false;
		    vm.disableClick = function(isCollapsed) {
		        alert("Clicked!");
		        if(isCollapsed)
		        vm.model.isDisabled = true;
		        vm.model.disable='';
		        return false;
		    };
		
		vm.tiposComprobantes = [ {
			codigoTipoComprob : "1",
			tipoComprobante : "Factura"
		},
			{
				codigoTipoComprob : "2",
				tipoComprobante : "Notas de Crédito"
			},
			{
				codigoTipoComprob : "3",
				tipoComprobante : "Notas de Débito"
			},
			{
				codigoTipoComprob : "4",
				tipoComprobante : "Comprobante de Retención"
			},
			{
				codigoTipoComprob : "5",
				tipoComprobante : "Todos"
			} ];

		vm.productsDataSource = {
			type : "odata",
			serverFiltering : true,
			transport : {
				read : {
					url : "http://demos.telerik.com/kendo-ui/service/Northwind.svc/Products",
				}
			}
		};
		
		vm.datasourceParent = [
		{ parentName: "Parent1", parentId: 1 },
		{ parentName: "Parent2", parentId: 2 }];

		vm.datasourceChild = [
		 { childName: "Child1", childId: 1, parentId: 1 },
		 { childName: "Child2", childId: 2, parentId: 2 },
		 { childName: "Child3", childId: 3, parentId: 1 },
		 { childName: "Child4", childId: 4, parentId: 2 }
		 ]; 
			
	}
}(window.angular));