/**
 * @ngdoc provider
 * @name RutaProvider
 * @description Ruteo desde archivo json/WS. Las rutas son configuradas en dicho archivo mediante carga bajo demanda de
 *              recursos 27 de ene. de 2016
 * Copyright 2016 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
(function (angular){
	'use strict';

	angular.module('sri.rutas', [ 'ui.router' ])
	
	.run(function(Ruta) {
		Ruta.setUpRoutes();
	});
	
	angular.module('sri.rutas').provider('Ruta', RutaProvider);

	RutaProvider.$inject = [ '$stateProvider' ];

	function RutaProvider ($stateProvider){

		var urlCollection;
		var estado;
		/* Inicializacion de pagina de inicio */
		$stateProvider.state('login', {
			url : '/login',
			templateUrl : 'app/plantilla/paginas/login.html',
			data : {
				displayName : 'Acceso'
			},
			resolve : {
				cargaRecursos : function ($ocLazyLoad){
					return $ocLazyLoad.load({
						files : [ 'app/plantilla/js/controladores/AutenticacionControlador.js'

						]
					});
				}
			}
		}).state(
				'sri-web',
				{
					url : '/sri-web',
					templateUrl : 'app/plantilla/paginas/inicio.html',
					data : {
						displayName : 'Inicio'
					},
					resolve : {
						loadMyDirectives : function ($ocLazyLoad){
							return $ocLazyLoad.load({
								name : 'sri',
								files : [ 'app/plantilla/js/directivas/MenuDirectiva.js',
										'app/plantilla/js/directivas/MenuItemsDirectiva.js',
										'app/plantilla/js/directivas/MenuNotificacionesDirectiva.js',
										'app/plantilla/js/directivas/MenuAccesibilidadDirectiva.js',
										'app/plantilla/js/directivas/MenuPerfilDirectiva.js',
										'app/plantilla/js/directivas/MenuImagenDirectiva.js'

								]
							});
						}
					}
				}).state('sri-web.home', {
			url : '/home',
			controller : 'InicioControlador',
			templateUrl : 'app/plantilla/paginas/cuenta/configuracion-cuenta.html',
			data : {
				displayName : 'Perfil Usuario'
			}
		}).state('sri-web.no-encontrado', {
			templateUrl : 'app/plantilla/paginas/404.html',
			url : '/no-encontrado',
			data : {
				displayName : 'No Encontrado'
			}
		});
		this.$get = function ($http){
			return {
				setUpRoutes : function (){
					$http
							.get(urlCollection)
							.success(
									function (collection){
										for ( var routeName in collection) {
											if (collection[routeName].resolve !== undefined) {
												var files = "";
												if (collection[routeName].resolve.files.length > 0) {
													files = '[';
													for ( var file in collection[routeName].resolve.files) {
														files = files + '"' +
																collection[routeName].resolve.files[file] + '",';
													}
													files = files.substr(0, files.length - 1);
													files = files + ']';
												} else {
													files = '"' + collection[routeName].resolve.files + '"';
												}
												/* jslint evil: true */
												eval('estado = {' +
														'name: collection[routeName].name,' +
														'url: collection[routeName].url,' +
														'templateUrl: collection[routeName].templateUrl,' +

														'resolve: {' +
														'loadMyDirectives: function($ocLazyLoad) {'+
														' return $ocLazyLoad.load({name:"' +
														collection[routeName].resolve.name + '", files:' + files +
														'});}' + '}' + ', data : {' +
														'     displayName: collection[routeName].displayName' + '  }' +
														'}');
											} else {
												estado = {
													name : collection[routeName].name,
													url : collection[routeName].url,
													templateUrl : collection[routeName].templateUrl,
													data : {
														displayName : collection[routeName].displayName
													}
												};
											}
											$stateProvider.state(estado, estado);
										}
									});
				}
			};
		};

		this.setCollectionUrl = function (url){
			urlCollection = url;
		};

	}

}(window.angular));