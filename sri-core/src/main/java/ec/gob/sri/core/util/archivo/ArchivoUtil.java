/**
 * Clase ArchivoUtil.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.util.archivo;

import java.io.File;
import java.io.IOException;

import ec.gob.sri.core.exception.base.BaseExcepcion;
import ec.gob.sri.core.util.StringUtil;



/**
 * Funciones utiles
 * @author mrobayo
 */
public final class ArchivoUtil {
	
	private ArchivoUtil(){
		
	}

	/**
	 * Valida que existe un directorio
	 *
	 * @param directoryPath
	 * @throws BaseExcepcion
	 */
	public static void verificaDirecotrio(File directoryPath)  {
		if (!directoryPath.exists() || !directoryPath.isDirectory()) {
			// Create a directory; all non-existent ancestor directories are automatically created
			boolean success = directoryPath.mkdirs();
			if (!success){
				throw new IllegalArgumentException("No es posible crear Ruta: "+directoryPath.getAbsolutePath());
			}
				
		}
	}

	/**
	 * Nombre del archivo (fix it for InternetExplorer)
	 */
	public static String arreglaNombre(String nombre) {
		String value = StringUtil.trimToName(StringUtil.trimToEmpty(nombre));
		if (value.indexOf('\\') >= 0) {
			value = value.substring(value.lastIndexOf('\\')+1);
		}
		return value;
	}

	/**
	 * Quita espacios en blanco
	 */
	public static String quitaEspaciosBlanco(String nombre) {
		String filename = arreglaNombre(nombre);
		filename = filename.replaceAll("['\"\\s]", "_");
		return filename;
	}

	/**
	 * Extension del archivo
	 */
	public static String obtenerExtension(String fileName) {
		String name = StringUtil.trimToEmpty(fileName);
		int index = name.lastIndexOf('.');
		String ext = "";
		if (index >= 0) {
			ext = name.substring(index);
		}
		return ext.toLowerCase();
	}

	/**
	 * Temporal file name
	 * @param dir
	 */
	public static String getTempFileName(File dir) {
		String tempFileName = null;
		File tempFile;

		try {
			tempFile = File.createTempFile("session", "", dir);
			tempFileName = tempFile.getName();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return tempFileName;
	}





	/**
	 * File name without the extension
	 */
	public static String quitaExtension (String str) {
        // Handle null case specially.
        if (str == null){
        	return null;
        }

        // Get position of last '.'.
        int pos = str.lastIndexOf('.');

        // If there wasn't any '.' just return the string as is.
        if (pos == -1){
        	return str;
        }

        // Otherwise return the string, up to the dot.
        return str.substring(0, pos);
    }

}
