/**
 * Clase ConstantesJPA.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.model.base;

/**
 * Para facilitar algunas constantes
 * @author hrobayo@gmail.com Mar 16, 2010
 */
public class ConstantesJPA {

	public static final int CODE_SZ = 20;
	public static final int SHORT_SZ = 60;
	public static final int NAME_SZ = 100; //60
	public static final int FULL_SZ = 600;
	public static final int HALF_SZ = 300;

	public static final int DESC_SZ = 4000;

	public static final String SEQ = "_seq", ID = "id_";



	public static final String FK = "fk_";
	public static final String UNIQUE = "uk_";

	
	

}
