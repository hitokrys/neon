/**
 * Clase LectorBlobDB.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.dao.util.blob;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;

import javax.inject.Inject;

import org.slf4j.Logger;

/**
 * Read a Blob file :)
 * 
 * @author mrobayo
 */
public class LectorBlobDB implements LectorBlob {

	@Inject
	Logger log;

	final ByteArrayOutputStream out;

	public LectorBlobDB(ByteArrayOutputStream out) {
		this.out = out;
	}

	public ByteArrayOutputStream getOutput() {
		return out;
	}

	/**
	 * Reads a Blob Field
	 */
	public boolean leerBlob(Object val) {
		boolean exito = true;
		if (val instanceof Blob) {
			final int blockSize = 4096;

			byte[] data = new byte[blockSize];

			InputStream in = null;
			try {
				Blob blob = (Blob) val;

				in = blob.getBinaryStream();
				int c;

				while ((c = in.read(data, 0, blockSize)) != -1) {
					out.write(data, 0, c);
				}
				out.flush();
				out.close();
			} catch (SQLException | IOException e) {
				log.error("error", e);
			} finally {
				try {
					if (in != null) {
						in.close();
					}
				} catch (IOException e) {
					log.error("error", e);
				}
			}
		} else if (val instanceof byte[]) {
			try {
				out.write((byte[]) val);

				out.flush();
				out.close();

			} catch (IOException e) {
				exito = false;
				e.printStackTrace();
			}
		}
		return exito;
	}
}
