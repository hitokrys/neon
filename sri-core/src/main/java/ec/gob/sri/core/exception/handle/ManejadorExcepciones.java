/**
 * Clase ManejadorExcepciones.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.exception.handle;

/**
 * @author Hugo Camilo Robayo Ayala
 * Mantiene el contexto de el error:
 * 		- Ubicación del error
 *      - Contexto del Error
 */
public interface ManejadorExcepciones {
	
	
	Object handleFilure(ContextoFalla fc);

}
