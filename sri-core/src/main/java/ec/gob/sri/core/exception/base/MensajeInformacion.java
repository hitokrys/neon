/**
 * Clase InformacionError.java 04/03/2016
 * Copyright 2016 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.exception.base;

/**
 * @author mbcc010714
 *
 */
public class MensajeInformacion extends BaseExcepcion {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MensajeInformacion() {
	}
	
	public MensajeInformacion(String codigoMensajeInformacion) {
		this.setCodigo(codigoMensajeInformacion);
	}
	
	public String agregarMensajeInformacion() {
		return error;
	}
}
