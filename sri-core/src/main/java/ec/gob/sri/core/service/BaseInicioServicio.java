/**
 * Clase BaseInicioServicio.java 21 de dic. de 2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.metamodel.ManagedType;
import javax.persistence.metamodel.Metamodel;

import ec.gob.sri.core.dao.util.jql.JQLUtil;

/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
@Startup
@Singleton
public class BaseInicioServicio {
	
	/*
	 * Aqui tengo en memoria toda la info de las clases
	 */
	private final static Map<Class, Set> ALL_JPA_MAPPED_CLASESS = new HashMap<Class, Set>();
	
	@Inject EntityManager em;
	
	public BaseInicioServicio() {}
	
	
	/**
	 * Aqui se inicializa la utilidad de generación JQL
	 */
	@PostConstruct 
	void init(){		
		
		if (em != null){
			Metamodel metamodel = em.getMetamodel();
			for (ManagedType<?> managedType : metamodel.getManagedTypes()) {			
				ALL_JPA_MAPPED_CLASESS.put(managedType.getJavaType(), managedType.getAttributes());			
			}
			
			JQLUtil.setJPAMetaData(ALL_JPA_MAPPED_CLASESS);			
		}
	}
	


}
