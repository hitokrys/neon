/**
 * Clase BaseFachadaServicioImplAbstracta.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.service;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.slf4j.Logger;

import ec.gob.sri.core.dao.BaseDAO;
import ec.gob.sri.core.dao.util.blob.LectorBlob;
import ec.gob.sri.core.dao.util.paging.ManejadorPagina;
import ec.gob.sri.core.dao.util.paging.PaginaDatos;

/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
public abstract class BaseFachadaServicioImplAbstracta implements BaseFachadaServicio {

	@Inject
	EntityManager em;
	@Inject
	BaseDAO dao;
	@Inject
	Logger log;

	@Override
	public <D> D guardarEntidad(D objetoDeEntidad) {
		return dao.guardarEntidad(objetoDeEntidad);
	}

	public ByteArrayOutputStream obtenerContenidoDeArchivo(Long idDeArchivoAObtener) {
		// ByteArrayOutputStream out = new ByteArrayOutputStream();

		// Archivo archivo = dao.findById(Archivo.class, archivoId);
		// dao.readBlobFile(Archivo.class, "bodyFile", archivo.getId(), new
		// LectorBlob(out));
		// return out;
		log.debug("corregir esto!!");
		return null;
	}

	@Override
	public <T> boolean leerArchivoBlob(Class<T> claseDeEntidad, String campoBlob, Serializable idDeEntidad, LectorBlob lectorDeBlob) {
		return dao.leerArchivoBlob(claseDeEntidad, campoBlob, idDeEntidad, lectorDeBlob);
	}

	@Override
	public <D> D buscarPorIdDeEntidad(Class<D> claseDeEntidad, Serializable idDeEntidad) {
		return dao.buscarPorIdDeEntidad(claseDeEntidad, idDeEntidad);
	}
	
	@Override
	public <D> List<D> buscarPorObjetoDeEntidad(Object objetoEntidad) {
		return dao.buscarPorObjetoDeEntidad(objetoEntidad);
	}
	
	@Override
	public <T> List<T> buscarPorParametrosSimple(Class<T> claseDeEntidad, String consultaAEjecutar, Object... valoresDeParametros) {
		return dao.buscarPorParametrosSimple(claseDeEntidad, consultaAEjecutar, valoresDeParametros);
	}

	@Override
	public <D> List<D> buscarTodos(Class<D> claseDeEntidad) {
		return dao.buscarTodos(claseDeEntidad);
	}

	@Override
	public <T> List<T> buscarPorParametros(String consultaAEjecutar, Object... valoresDeParametros) {
		return dao.buscarPorParametros(consultaAEjecutar, valoresDeParametros);
	}

	@Override
	public <T> PaginaDatos<T> buscarConsultasPaginadas(Class<T> claseDeEntidad, String consultaAEjecutar, ManejadorPagina manejadorPaginacion, Object... valoresDeParametros) {
		return dao.buscarConsultasPaginadas(claseDeEntidad, consultaAEjecutar, manejadorPaginacion, valoresDeParametros);
	}

	public void setEntityManager(EntityManager entityManager) {
		this.dao.setEntityManager(entityManager);
	}



}
