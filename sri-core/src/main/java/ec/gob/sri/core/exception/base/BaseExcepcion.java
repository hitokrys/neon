/**
 * Clase BaseExcepcion.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.exception.base;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.ejb.ApplicationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ec.gob.sri.core.exception.base.types.CodigoError;
import ec.gob.sri.core.exception.base.types.SeveridadError;
import ec.gob.sri.core.exception.base.types.TipoError;

/**
 */

/**
 * @author Hugo Camilo Robayo Ayala
 *
 * BaseExcepcion es una subclase de RuntimeExcepticon y puede ser usada con excepcion base
 * para lanzar desde un contenedor, mantiene el registro si la excepcion fue logeada o no,
 * tambien mantiene su ID que es unico y puede ser llevada a la capa del cliente y mostrada
 * al usuario. El usuario final puede hacer uso de ese ID en caso de requerir soporte.
 */
@ApplicationException(rollback = true, inherited = true)
@JsonIgnoreProperties ({"stackTrace","cause"})
public class BaseExcepcion extends RuntimeException {
	
	private static final long serialVersionUID = 7805393878607788241L;
	private static final Logger logger =  LoggerFactory.getLogger(BaseExcepcion.class);
	
	protected String id;
	protected String error;
	
	// Verifica si fue logeada
	protected boolean logged;
	
	//TODO deberoia marcarse el nombre de usuario aqui o debería ser un parametro
	protected String username;
	
	/*
	 * El codigo de error es un string, asi se permite que otras apliaciones tengan sus
	 * propios codigos, deberian implementar su propias enumeraciones.
	 */
	protected String codigo = CodigoError.SRI_APP_CODE_BASE.getValue();
	
	/*
	 * Si multiples son lanzadas simultaneamente en algun momento
	 * la severidad permite al analizador escoger una estrategia adecuada 
	 */
	protected SeveridadError severidad = SeveridadError.ADVERTENCIA;
	
	/*
	 * Identifica el tipo de error 
	 */
	protected TipoError tipo = TipoError.INTERNO;
	
	protected List<InformacionError> errorInfoList = new ArrayList<InformacionError>();
		
	/**
	 * Genera un ID unico
	 * @return
	 */
	public static String getExceptionID() {
		StringBuffer exceptionId = new StringBuffer();
		try{
			exceptionId.append(InetAddress.getLocalHost().getHostName());  	
		} catch(UnknownHostException ue) {
			exceptionId.append(".....");
		}
		
		return exceptionId.append(System.currentTimeMillis()).toString();		 
	}	
		
	/**
	 * Prepara la instancia
	 */
	private void prepare(){
		id = getExceptionID();
		username = "nombreUsuario"; //TODO obtener el usuario de alguna manera, si es posible ponerlo en el thread local.
		
		if (errorInfoList.isEmpty()){
			agregarInformacionError(new InformacionError(this.getMessage(), this.getCause())); 			
		}
		
		if (this.error == null){
			this.error = this.getMessage();
		}
	}

	/*
	 * Varios Consturctores 
	 */

	public BaseExcepcion(){
		super();
		prepare();
	}
		
	public BaseExcepcion(String msg, Object... params){		
		super((msg == null) ? null:String.format(msg, params) );		
		prepare();
	}

	public BaseExcepcion(Exception e, String msg, Object... params) {
		super((msg == null) ? null:String.format(msg, params), e);
		prepare();
	}
	
	public BaseExcepcion(Exception e, String code){		
		super(e.getMessage(), e);		
		prepare();
		this.codigo = code;
		agregarMensajeError();
	}
	public BaseExcepcion(String msg){
		super(msg);
		prepare();		
		agregarMensajeError();
	}

	public BaseExcepcion(Exception e){		
		super(e.getMessage(), e);
		prepare();
	}

	public BaseExcepcion(String msg, String argument){
		super(String.format(msg, argument));
		prepare();
	}

	/**
	 * @param errorInfo
	 * @param e
	 * @return
	 */
	public BaseExcepcion agregarInformacionError(InformacionError errorInfo, BaseExcepcion e) {
		agregarInformacionError(errorInfo);
		return this;
	}

	/**
	 * Permite hacer la reclasificacion
	 * @param info
	 * @return
	 */
	public BaseExcepcion agregarInformacionError(InformacionError info){		
		//TODO si es el primer errorInfo se deberia repaldar la primera informacion de 
		//la excepcion como un errorInfo?
		
		if (info.getTipo() != this.tipo){
			this.tipo = info.getTipo();
		}
		
		if (info.getSeveridad() != this.severidad){
			this.severidad = info.getSeveridad();
		}
		
		if (info.getCodigo() != null){
			setCodigo(info.getCodigo());
		}
		
		//TODO Verificar si se debe hacer la validacion de cambio de mensaje, codigo, etc.		
		this.errorInfoList.add(info);
		
		return this;		
	}
	
	/**
	 * Agrega un mensaje de error
	 */
	private final void agregarMensajeError(){
		String mensajeError = null;
		try {
			ResourceBundle resourceBundle = ResourceBundle.getBundle("/messages");
			if (resourceBundle != null){
				mensajeError = resourceBundle.getString(codigo);

				if (codigo.equals(CodigoError.SRI_APP_CODE_BASE.getValue())){
					StringBuffer stringBuffer = new StringBuffer();
					stringBuffer.append(mensajeError).append(':').append(this.getMessage());
					mensajeError = stringBuffer.toString();
				}
			}			
		} catch (MissingResourceException e) {		
			logger.warn("No se puede obtener archivo bundle de mensajes parametrizados:",e.getMessage());
			mensajeError = this.getMessage();
		}	
		if (mensajeError != null){
			error = mensajeError;
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public void log(){
		if (!isLogged()){
			logger.error("Error:", this);
			this.setLogged(true);
		}
	}
	
	/*
	 * Getters & Setters
	 */
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String code){
		this.codigo = code;
		agregarMensajeError();
	}
	
	public SeveridadError getSeveridad() {
		return severidad;
	}

	public void setSeveridad(SeveridadError severity) {
		this.severidad = severity;
	}
	
	public boolean isLogged() {
		return logged;
	}

	public void setLogged(boolean logged) {
		this.logged = logged;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}
	
}