/**
 * Clase Eliminable.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.model.types;

import java.util.Date;

/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
public interface Eliminable {
	
	/**
	 * Estado del Objeto
	 */
	boolean isActivo();

	/**
	 * Fecha en q se puso inactivo
	 */
	Date getFechaInactivo();

	/**
	 * Fecha en q se puso inactivo
	 */
	Eliminable inactivar();



}
