/**
 * Clase TablaDatos.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.rest.datatable;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.enterprise.util.Nonbinding;
import javax.interceptor.InterceptorBinding;

@InterceptorBinding
@Target({ TYPE, METHOD })
@Retention(RUNTIME)
@Documented
/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
public @interface TablaDatos {
	
	// Propiedad utilizadas por el Interceptor y el Filtro de propiedades de Jackson
	public static final String FILTER_NAME = "dataTableFilter";
	public static final String FILTER_DATA_PROPERTIES = "mDataProp_";
	
	// Empty String
	public static final String EMPTY_FILTER = "";
	
	

	//Propiedades utilizadas por el TablaDatos JS
	public static final String DATA_PROPERTY = "data";	
	public static final String DATA_ECHO = "draw";
	public static final String DATA_TOTAL_RECORDS = "recordsTotal";
	public static final String DATA_TOTAL_DISPLAY_RECORDS = "recordsFiltered";
	public static final String TOTAL = "total";
	public static final String PAGESIZE = "tamanioPagina";
	
	
	
	//Propiedad para registrar la clave en el mapa de salida que contiene el objeto DATAPAGE
	public static final String DATAPAGE_PROPERTY = "_DATAPAGE_PROPERTY_";	
	
	
	@Nonbinding public TablaDatosFiltro tipoFiltro() default TablaDatosFiltro.FILTER_DEFAULT;
	@Nonbinding public String propiedades() default EMPTY_FILTER;

}
