/**
 * Clase InformacionError.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.exception.base;

import java.util.HashMap;
import java.util.Map;

import ec.gob.sri.core.exception.base.types.CodigoError;
import ec.gob.sri.core.exception.base.types.SeveridadError;
import ec.gob.sri.core.exception.base.types.TipoError;

/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
public class InformacionError {

	protected Throwable cause;
	protected String codigo;
	protected TipoError tipo = TipoError.INTERNO;
	protected SeveridadError severidad = SeveridadError.GRAVE;
	protected String userErrorDescription;
	protected String errorDescription;
	protected String errorCorrection;

	/*
	 * Parametros sobre - Causas de la excepcion - Detalles acerca del
	 * componente - Frecuencia que se lanza - Otras...
	 */
	protected Map<String, Object> parameters = new HashMap<>();

	/**
	 * Instancia un Error info basico
	 */
	public InformacionError(String errorDescription, Throwable cause) {
		this.cause = cause;
		this.codigo = CodigoError.SRI_APP_CODE_BASE.getValue();
		this.errorDescription = errorDescription;

	}

	public InformacionError(String errorCode) {
		this.codigo = errorCode;
	}

	/**
	 * Instancia un Error info basico
	 */
	public InformacionError(String code, String errorDescription, Throwable cause) {
		this.cause = cause;
		this.codigo = code;
		this.errorCorrection = errorDescription;

	}

	public Object getParameter(String key) {
		return parameters.get(key);
	}

	public void addParameter(String key, Object value) {
		parameters.put(key, value);
	}

	public String getErrorCorrection() {
		return errorCorrection;
	}

	public void setErrorCorrection(String errorCorrection) {
		this.errorCorrection = errorCorrection;
	}

	public Throwable getCause() {
		return cause;
	}

	public String getCodigo() {
		return codigo;
	}

	public TipoError getTipo() {
		return tipo;
	}

	public SeveridadError getSeveridad() {
		return severidad;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setTipo(TipoError type) {
		this.tipo = type;
	}

	public void setSeveridad(SeveridadError severity) {
		this.severidad = severity;
	}

}
