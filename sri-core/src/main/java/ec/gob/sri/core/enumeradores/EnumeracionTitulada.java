/**
 * Clase EnumeracionTitulada.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.enumeradores;

/**
 * Interface for making the access to the string value uniform
 * http://www.hibernate.org/273.html
 *
 * @author usuario
 */
public interface EnumeracionTitulada {

	/**
	 * Current string value stored in the enum
	 * @return
	 */
	String getValue();
	
	/**
	 * Current string value of the enum
	 * @return
	 */
	String getName();
	

}
