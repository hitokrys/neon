/**
 * Clase Archivo.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.util.archivo;


/**
 * UploadItem
 * @author mrobayo
 */
public class Archivo {

	private String nombre;
	private byte[] contenido;
	private String tipoContenido;
	
	public Archivo() {
		
	}
	
	public Archivo(String originalFileName, byte[] contents, String contentType) {
		this.nombre = originalFileName;
		this.contenido = contents;
		this.tipoContenido = contentType; 
	}
	
	public String getFileName() {
		return ArchivoUtil.arreglaNombre(nombre);
	}
	
	/**
	 * Nombre del archivo sin extension
	 */
	public String getFileName2() {
		return ArchivoUtil.quitaExtension(getFileName());
	}
	
	public long getSize() {
		if (contenido != null) {
			return contenido.length;
		}
		return 0;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String originalFileName) {
		this.nombre = originalFileName;
	}
	public byte[] getContenido() {
		return contenido;
	}
	public void setContenido(byte[] contents) {
		this.contenido = contents;
	}
	public String getTipoContenido() {
		return tipoContenido;
	}
	public void setTipoContenido(String contentType) {
		this.tipoContenido = contentType;
	}

	@Override
	public String toString() {
		return nombre;
	}
	
}
