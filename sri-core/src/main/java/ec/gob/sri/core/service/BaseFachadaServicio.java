/**
 * Clase BaseFachadaServicio.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.service;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.List;

import ec.gob.sri.core.dao.util.blob.LectorBlob;
import ec.gob.sri.core.dao.util.paging.ManejadorPagina;
import ec.gob.sri.core.dao.util.paging.PaginaDatos;

/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
public interface BaseFachadaServicio  {

	<D> D guardarEntidad(final D objetoDeEntidad);		
	
	ByteArrayOutputStream obtenerContenidoDeArchivo(Long idDeArchivoAObtener);

	<T> boolean leerArchivoBlob(final Class<T> claseDeEntidad, final String campoBlob, final Serializable idDeEntidad, final LectorBlob lectorDeBlob);

	<D> D buscarPorIdDeEntidad(final Class<D> claseDeEntidad, Serializable idDeEntidad);

	<D> List<D> buscarPorObjetoDeEntidad(Object objetoEntidad);	

	/**
	 * Realiza una consulta por los valores de los correspondientes parametros dados en la consulta a ejecutar.
	 * 
	 * @param claseDeEntidad
	 * @param consultaAEjecutar
	 * @param valoresDeParametros
	 * @return Retorna la lista de objetos de entidades que devulelve la consulta
	 */
	<T> List<T> buscarPorParametrosSimple(Class<T> claseDeEntidad, String consultaAEjecutar, Object... valoresDeParametros);
	
	<D> List<D> buscarTodos(final Class<D> claseDeEntidad);
	
	/**
	 * Realiza una consulta por los valores de los correspondientes parametros dados en 
	 * la consulta a ejecutar, los valores de parametros son pasados con un Map.
	 *
	 * @param consultaAEjecutar
	 * @param valoresDeParametros
	 * @return Retorna la lista de objetos de entidades que devulelve la consulta
	 */	
	<T> List<T> buscarPorParametros(String consultaAEjecutar, Object... valoresDeParametros);	
	
	<T> PaginaDatos<T> buscarConsultasPaginadas(final Class<T> claseDeEntidad,
			final String consultaAEjecutar, final ManejadorPagina manejadorPaginacion, final Object... valoresDeParametros);		
}
