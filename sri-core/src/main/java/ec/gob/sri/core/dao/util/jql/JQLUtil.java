/**
 * Clase JQLUtil.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.dao.util.jql;

import java.beans.IntrospectionException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.Attribute.PersistentAttributeType;
import javax.persistence.metamodel.Type;

import org.apache.commons.beanutils.PropertyUtils;

import ec.gob.sri.core.exception.base.BaseExcepcion;
import ec.gob.sri.core.model.types.Identificable;
import ec.gob.sri.core.util.ListUtil;
import ec.gob.sri.core.util.StringUtil;


/**
 * Permite generar sentencias HQL desde un objeto ejemplo
 * @author Davide Alberto Molin (davide.molin@gmail.com)
 * @modified Hugo Camilo Robayo Ayala
 */
@SuppressWarnings("rawtypes")
public final class JQLUtil extends Predicado {

	/*
	 * Aqui tengo en memoria todas info de las clases
	 */
	private final static Map<Class, Set> ALL_JPA_MAPPED_CLASESS = new HashMap<Class, Set>();
	

    String incipit;
    
    //following the incipit, usually contains joins
    String header = "";
    
    //closing the query: order by and other things
    String footer = "";
    
    //distinct predicate
    String distinctSentence;
    private String alias;
    boolean swCount;
    
    boolean ignoreBooleanFields;

    List<String> ignoreProperties = new ArrayList<String>();

    private Set<Attribute<?,?>> attributes; 
	
	Map<String,String> joinAliases = new HashMap<String, String>();

	public static final String DEFAULT_ALIAS = "o";

	private static final String AS = " AS ";

	public enum SortOrder { ASC, DESC };

    public JQLUtil( Class  entityClass )
    {
        this(entityClass, DEFAULT_ALIAS);
    }

    public JQLUtil( Class entityClass, String alias ) {
    	super();
        this.incipit = "FROM "+ entityClass.getName()+ AS + alias +" ";
        this.alias = alias;
    }

    public  JQLUtil( Class entityClass, String alias, Set<Attribute<?,?>> attributes )
    {
        this(entityClass, alias);
        this.attributes = attributes;
    }

    public  JQLUtil( Class entityClass, String alias, Set<Attribute<?,?>> attributes , boolean ignoreBoolean)
    {
        this(entityClass, alias, attributes);
        this.ignoreBooleanFields = ignoreBoolean;
    }


    /**
     * The incipit can start with "from  " or
     * with the less common "select  from  ..."
     */
    public JQLUtil( String incipit, String alias )
    {
        super();
        this.incipit = incipit;
        this.alias = alias;
    }
    
    public static void setJPAMetaData (Map<Class, Set> metadata){
    	ALL_JPA_MAPPED_CLASESS.putAll(metadata);
    }
    

    /**
     * Inner Join
     * @param expression what to join with
     * @param alias Aliased name for this join
     * @return
     */
    public JQLUtil join( String expression, String alias )
    {
        header += " JOIN " + expression + AS + alias;
        return this;
    }

    public JQLUtil leftJoin( String expression )
    {
    	String joinColumn = expression.trim();
    	String alias = joinColumn.substring(joinColumn.lastIndexOf('.') + 1);

    	if (LOG.isDebugEnabled()){
    		header += "\n ";
    	}
    	
    	String target = getAlias() +"."+ joinColumn;
        header += " LEFT OUTER JOIN FETCH " + target + AS + alias;        
        joinAliases.put(target, alias);
        
        return this;
    }

    public JQLUtil leftJoin( String expression, String alias )
    {
    	if (LOG.isDebugEnabled()){
    		header += "\n";
    	}

    	String target = getAlias() +"."+ expression.trim();
        header += " LEFT OUTER JOIN FETCH " + target + AS + alias;        
        joinAliases.put(target, alias);
        
        return this;
    }

    public JQLUtil leftJoin( String expression, String alias, String queryAlias )
    {
    	if (LOG.isDebugEnabled()){
    		header += "\n";
    	}
    	
    	String target = queryAlias +"."+ expression.trim();
        header += " LEFT OUTER JOIN FETCH " + target + AS + alias;        
        joinAliases.put(target, alias);
    	
        return this;
    }
    
    
    public JQLUtil rightJoin( String expression, String alias )
    {
        header += " RIGHT JOIN " + expression + AS + alias;
        joinAliases.put(expression, alias);
        return this;
    }

    public JQLUtil count()
    {
        this.swCount = true;
        return this;
    }

    public boolean hasCount(){ return swCount; }

    /**
     * Useful when we want enable/disable counting
     * @param wantsCount true/false
     * @return this same query object, for chaining calls.
     */
    public JQLUtil count(boolean wantsCount )
    {
        this.swCount = wantsCount;
        return this;
    }

    private String getIncipit()
    {
        String ret = (incipit == null ? "" : incipit + " " );
        if( distinctSentence == null
             || ret.trim().toUpperCase().startsWith( "SELECT" ) )
        {
            //unable to apply distinct!.
            return ret;
        } 

        return getHql();
    }
    
    private String getHql(){
    	return "SELECT " + (swCount ? " COUNT(" : "" )
                + (distinctSentence == null ? getAlias() : " DISTINCT " + distinctSentence )
                + (swCount ? ")" : "" ) + " " + incipit;
    }

    @Override
    public String getQueryString()
    {
        return getIncipit() + header + super.getQueryString() + footer;    
    }

    public String getWhere()
    {
        String hql = super.getQueryString();
        return hql.substring(hql.indexOf("WHERE") + 5);
    }

    @Override
    public String toString()
    {
        return getQueryString();
    }

    public JQLUtil addOrderBy( String orderBy )
    {
        footer += (footer.length() == 0 ? " ORDER BY " : "," ) + orderBy;
        return this;
    }

    public JQLUtil distinct()
    {
        return distinct(null);
    }

    /**
     * NOTE:distinct is applicable ONLY when the query is NOT already
     *           starting with SELECT statement..
     * @param alias aliased name on which to apply the distinct
     *   as in "select distinct(<alias>) from MyTable <alias>"
     * @return
     */
    public JQLUtil distinct( String alias )
    {
        this.distinctSentence = alias;
        return this;
    }

    /**
     * utility method to handle order by clauses.
     * @param sortProperty the property to apply the sorting to.
     * @param sortOrder see enum at class start
     * @param entityAlias  alias to be used for table. defaults to 'obj'
     * @return
     */
    public JQLUtil order( String sortProperty, String sortOrder
                          , String entityAlias )
    {
        //esamino ordinamento
        if( sortProperty == null ){
        	return this; //no ordering.
        }
            

        String orderType = "ASC";
        orderType = "ASC".equalsIgnoreCase( sortOrder ) ?
                "ASC" : "DESC";
        

        //obj prefix, to avoid ambiguity in the query with joins
        String sortPropertyFinal = ( entityAlias == null ? "" : (entityAlias + ".") )
                         + sortProperty;

        addOrderBy( sortPropertyFinal + " " + orderType );
        return this;
    }

    /**
     * utility method to handle order by clauses.
     * @param sortProperty the property to apply the sorting to.
     * @param sortOrder see enum at class start
     * @return
     */
    public JQLUtil order( String sortProperty, String sortOrder )
    {
        return order( sortProperty, sortOrder, getAlias() );
    }

    /**
      * Same as above, but using the Enum
      */
    public JQLUtil order( String sortProperty, SortOrder sortOrder )
    {
        return order( sortProperty, sortOrder.name() );
    }

    public boolean hasOrder() {
        return footer.contains( "ORDER BY" );
    }

    /**
     * Apply conditions to the query
     * @param exampleEntity
     * @return
     * @throws IllegalAccessException 
     * @throws InstantiationException 
     */
    @SuppressWarnings("unchecked")
	public JQLUtil filter(Object exampleEntity) {
    	if (exampleEntity == null){
    		throw new IllegalArgumentException("No se puede hacer consultas de todo el objeto");
    	}	    	
    	
    	attributes = ALL_JPA_MAPPED_CLASESS.get(exampleEntity.getClass());
    	if(attributes == null){
    		throw new BaseExcepcion("Entidades mapeadas incompletas.");
    	}
    	return filter(exampleEntity, dotPath(getAlias()), attributes);			
    }

    /**
	  * Apply conditions to the query,
	  * base on the value of the flag "pattern", that
	  * determines the matching tipo (contains, full match, one or more words..)
	  * @throws IntrospectionException
	  */

	private JQLUtil filter(Object exampleEntity, final String path,  Set<Attribute<?,?>> attributes ) {//final String[] propertyNames, final Type[] propertyTypes) {


			//BeanInfo info = Introspector.getBeanInfo( exampleEntity.getClass() );
			//PropertyDescriptor[] props = info.getPropertyDescriptors();
			
			for (Attribute<?, ?> attribute : attributes) {
				
				String propertyName = attribute.getName();
				Type propertyType = attribute.getDeclaringType();
				PersistentAttributeType pType = attribute.getPersistentAttributeType();
								
				LOG.info("propertyName: {}, propertyType: {}, persistenceType: {} ",propertyName, propertyType.getJavaType(), pType);
				Object value =  null;
				try {
					value = PropertyUtils.getProperty(exampleEntity, propertyName);
				} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
					LOG.info("Error objeniendo la propiedad", e);	
				}
				LOG.info("the value: {}" , value);				
 
				
				
				if (value == null || ListUtil.isInList(ignoreProperties, propertyName)){
					continue; //Ignorar propiedad
				}
				
				switch (pType) {
				case BASIC:					
					addBasicPredicate(attribute, value, propertyName, propertyType, path);					
					break;
				case MANY_TO_ONE:
					addManyToOnePredicade(attribute, value, propertyName, path);					
					break;
				case EMBEDDED:
					LOG.info("Generacion de predicados de objetos embebidos no soportado.");
					break;
				default:
					//TODO agregar funcionalidad para los otros tipos
					break;
				}
			}

		return this;
	 }
	 
	 
	 private void addBasicPredicate(Attribute<?, ?> attribute , Object value, String propertyName, Type propertyType, String path){
			if (attribute.getJavaType().isAssignableFrom(String.class) ) {
				String val = StringUtil.removeAccents((String)value);
				if (StringUtil.isNotEmpty(val)){
					this.and( Predicado.eqPredicate(dotPath(path, propertyName), val) );	
				}
				
			} else if (attribute.getJavaType().isAssignableFrom(Integer.class) ) {
				this.and( Predicado.eqPredicate(dotPath(path, propertyName), value  ) );
				
			} else if (attribute.getJavaType().isAssignableFrom(Long.class) ) {
				this.and( Predicado.eqPredicate(dotPath(path, propertyName), value  ) );

			} else if (attribute.getJavaType().isAssignableFrom(Date.class) ) {
				this.and( Predicado.eqPredicate(dotPath(path, propertyName), (Date) value   ) );
				
			} else {
				LOG.info("2. {} {}={} not supported", propertyType.getJavaType(), propertyName, value);
			}		
	 }
	 
	 
	 private void addManyToOnePredicade(Attribute<?, ?> attribute , Object value, String propertyName, String path){
		 
		 if (value instanceof Identificable){
			 Identificable identificable = (Identificable) value;
			 if (identificable.getId() != null){
				Serializable id = identificable.getId();				
				if (id instanceof String){
					this.and(Predicado.eqPredicate(dotPath(path, propertyName, Identificable.ID), ((String) id) ) );
				}else if (id instanceof Long){
					this.and(Predicado.eqPredicate(dotPath(path, propertyName, Identificable.ID), id) );	
				}
				return;
			 }
		 }			 
		 filter(value, dotPath(path, propertyName), attribute.getJavaType());
		 
	 }	 

	/**
	 * Para navegar atraves de asociaciones
	 *
	 * @param childEntity nombre del propiedad
	 * @param path ruta hata la propiedad
	 * @param manyToOneType tipo de asociacion (hibernate)
	 */
	@SuppressWarnings("unchecked")
	private JQLUtil filter(Object childEntity, String path, Class clazz) {
		
    	Set attributes = ALL_JPA_MAPPED_CLASESS.get(clazz);
		if (attributes == null){
			LOG.warn("Propiedad {} de tipo {} no registrado", new Object[]{path, clazz.toString()});
			throw new BaseExcepcion("Propiedad %s de tipo %s no registrado", new Object[]{path, clazz.toString()});
						
		} else {			
			return filter(childEntity, path, attributes);			
		}			
	}

	public String getAlias() {
		return alias;
	}


	public boolean isIgnoreBoolean() {
		return ignoreBooleanFields;
	}

	public void setIgnoreBoolean(boolean ingnoreBoolean) {
		this.ignoreBooleanFields = ingnoreBoolean;
	}


	/**
	 * Agrega una propiedad para que sea ignorada
	 * @param propertyName Nombre de la propiedad
	 */
	public JQLUtil ignore(String propertyName) {
		ignoreProperties.add(propertyName);
		return this;
	}

	/**
	 * Arma un path utilizando punto como separador
	 *
	 * @param items
	 * @return path
	 */
	protected String dotPath(String... items) {
		final String dot = ".";
		StringBuffer path = new StringBuffer();
		for (int i=0; i<items.length; i++) {
			path.append(items[i]) ;
			if (!path.toString().endsWith(dot) && i < items.length-1){
				path.append(dot) ;
			}
		}		
		return  (joinAliases.containsKey(path.toString())?joinAliases.get(path.toString()):path.toString()) ;
	}
}