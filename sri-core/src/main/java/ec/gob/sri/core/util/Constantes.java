/**
 * Interface Constantes.java 14/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.util;

/**
 * @author mbcc010714
 *
 */
public class Constantes {
	public static final String ERROR_INTERNAMENTE_SERVICIO = "SRI.CORE.ERROR_INTERNAMENTE_SERVICIO";
}
