/**
 * Clase BaseDAOImpl.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;

import ec.gob.sri.core.dao.util.BaseDAOUtil;
import ec.gob.sri.core.dao.util.JPAUtil;
import ec.gob.sri.core.dao.util.blob.LectorBlob;
import ec.gob.sri.core.dao.util.jql.JQLUtil;
import ec.gob.sri.core.dao.util.paging.ManejadorPagina;
import ec.gob.sri.core.dao.util.paging.PaginaDatos;
import ec.gob.sri.core.util.ListUtil;
import ec.gob.sri.core.util.StringUtil;

/**
 *
 * @author Camilo Robayo
 */
@Named
public class BaseDAOImpl implements BaseDAO {

	@Inject EntityManager em;
	@Inject Logger logger;

    @PostConstruct
    protected void init() {
   	    // Si se usa el modulo core en aplicaciones que no utilizan o producen un entityManger
    }
	
	@Override
	public <D> boolean leerArchivoBlob(Class<D> claseDeEntidad, String campoBlob,final Serializable idDeEntidad, final LectorBlob lectorDeBlob) {

		final String hql = "SELECT " + campoBlob + " FROM " +
				claseDeEntidad.getSimpleName() + " WHERE " + BaseDAOUtil.pkName(claseDeEntidad) + " = :param";
		
		Query query = em.createQuery(hql);
		return lectorDeBlob.leerBlob(query.getSingleResult());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <D> D guardarEntidad(D objetoDeEntidad) {
		BaseDAOUtil.prepararParaGuardar(objetoDeEntidad);
		em.merge(objetoDeEntidad);

		// hacer esto debido a la manera como hibernate hace la prelacion en ejecuciones en batch
		// blog.krecan.net/2008/01/05/flushing-hibernate/
		// ver api doc de: AbstractFlushingEventListener metodo performExecutions
		/*Tener en cuenta que el abiente no es spring quedan comentados*/
		//entityManager.flush();
		//entityManager.clear();		
		return objetoDeEntidad;
	}			
	
	@Override
	public <D> D buscarPorIdDeEntidad(Class<D> claseDeEntidad, Serializable idDeEntidad) {
		if (idDeEntidad != null) {			
			return (D) em.find(claseDeEntidad, idDeEntidad);
		}
		return null;	
	}
	
	@Override
	public <D> List<D> buscarPorObjetoDeEntidad(Object objetoEntidad) {
		if (objetoEntidad != null) {
			JQLUtil jqlUtil = new JQLUtil(objetoEntidad.getClass()).filter(objetoEntidad);			
			return buscarPorParametros(jqlUtil.toString(), jqlUtil.getParametersValue());			
		}
		return null;	
	}	

	@Override
	public <T> List<T> buscarPorParametrosSimple(Class<T> claseDeEntidad, String consultaAEjecutar, Object... valoresDeParametros) {
		StringBuffer query = new StringBuffer();

		if (consultaAEjecutar != null && consultaAEjecutar.startsWith("SELECT")) {
			query.append(consultaAEjecutar);
		} else {
			query.append("FROM ").append(claseDeEntidad.getSimpleName());
			if (StringUtil.isNotEmpty(consultaAEjecutar)) {
				query.append(" WHERE ").append(consultaAEjecutar);
			}
		}

		Query q = em.createQuery(query.toString());
		for (int i = 0; i < valoresDeParametros.length; i++) {
			q.setParameter(i+1, valoresDeParametros[i]) ;			
		}
				
		return (List<T>) q.getResultList();
	}

	@Override
	public <D> List<D> buscarTodos(Class<D> claseDeEntidad) {
		StringBuffer query = new StringBuffer();

		query.append("FROM ").append(claseDeEntidad.getSimpleName());
		Query q = em.createQuery(query.toString());
		return (List<D>) q.getResultList();
		
	}
	
	@Override
	public <T> List<T> buscarPorParametros(String consultaAEjecutar, Object... valoresDeParametros) {
		Query q = em.createQuery(consultaAEjecutar);
		for (int i = 0; i < valoresDeParametros.length; i++) {
			q.setParameter(i+1, valoresDeParametros[i]) ;			
		}
		
		return (List<T>) q.getResultList();
	}
	

	public BaseDAO setEntityManager(EntityManager entityManager) {
		this.em = entityManager;
		return this;
	}	
	
	public <T> PaginaDatos<T> buscarConsultasPaginadas(final Class<T> claseDeEntidad,
			final String consultaAEjecutar, final ManejadorPagina manejadorPaginacion, final Object... valoresDeParametros) {

		if (manejadorPaginacion == null) {
			throw new IllegalArgumentException("ManejadorPagina no puede ser NULL");
		}
		
		else if (manejadorPaginacion.ordenarPor() != null){			
			manejadorPaginacion.ordenarPor().valid(claseDeEntidad); //Verifica que sean columna validas
		}
		
		List<T> rows = new ArrayList<T>(0);
		
		// primero se consultan el numero total de filas
		Long totalFilas = contarFilas(claseDeEntidad, consultaAEjecutar, manejadorPaginacion, valoresDeParametros);

		// luego se obtienen los datos
		if (totalFilas != 0) {
			String hql = BaseDAOUtil.query(claseDeEntidad, consultaAEjecutar,
					manejadorPaginacion.ordenarPor(), manejadorPaginacion.distinct());

			Query q = em.createQuery(hql);

			JPAUtil.addParameter(q, valoresDeParametros, consultaAEjecutar);

			if (manejadorPaginacion.maximoFilas() > 0) {
				q.setMaxResults(manejadorPaginacion.maximoFilas());
			}
			if (manejadorPaginacion.indiceInicio() >= 0 && manejadorPaginacion.indiceInicio() < totalFilas) {
				q.setFirstResult(manejadorPaginacion.indiceInicio());
			}
			logger.debug("findByPage.list HQL> \n{} \nPARAMS> {}", hql, valoresDeParametros);

			rows = q.getResultList();
		}
		PaginaDatos<T> resultadoDatosPaginados = new PaginaDatos<T>(rows, totalFilas.intValue(), manejadorPaginacion.tamanioPagina());

		logger.debug("findByPage " + claseDeEntidad.getSimpleName() + " " + consultaAEjecutar + " "
				+ ListUtil.join(valoresDeParametros));
		
		return resultadoDatosPaginados;
	}
	
	private Long contarFilas(final Class claseDeEntidad,final String consultaAEjecutar, final ManejadorPagina manejadorPaginacion, 
			final Object... valoresDeParametros){
		Long contador = -1L;

		if (manejadorPaginacion.ordenarPor() == null || manejadorPaginacion.ordenarPor().isCount()) {
			String hql = BaseDAOUtil.count(claseDeEntidad, consultaAEjecutar, manejadorPaginacion.distinct());

			Query hcnt = em.createQuery(hql);
			JPAUtil.addParameter(hcnt, valoresDeParametros, consultaAEjecutar);

			try {
				contador = (Long) hcnt.getSingleResult();
			}
			finally {
				logger.debug("findByPage.count={}, HQL> \n{} {} {}",
						new Object[]{contador, hql, (valoresDeParametros.length>0)?"\nPARAMS>":"", valoresDeParametros});
			}
		}
		return contador;
	}
}
