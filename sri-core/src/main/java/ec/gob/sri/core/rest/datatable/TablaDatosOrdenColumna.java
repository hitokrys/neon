/**
 * Clase TablaDatosOrdenColumna.java 5 de ene. de 2016
 * Copyright 2016 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.rest.datatable;

/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
public class TablaDatosOrdenColumna {
	
	private String column;
	private String dir;
	
	public TablaDatosOrdenColumna() {
	}
	
	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}
	
}
