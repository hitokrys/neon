/**
 * Clase OrdenPagina.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.dao.util.paging;

import java.lang.reflect.Method;

import javax.inject.Inject;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.slf4j.Logger;

/**
 * Presenta el orden de una fila
 * @author hrobayo@gmail.com May 31, 2010
 */
public class OrdenPagina {
	
	@Inject Logger log;

	public enum OrdenType {ASC, DESC};

	private boolean noCountBoolean;

	private String column;
	private OrdenType modo;

	private OrdenPagina priorSort;
	private boolean validBoolean = true;


	
	private OrdenPagina(String col, OrdenType modo) {
		this.modo = modo;
		this.column = col;
	}

	/**
	 * OrdenPagina de una columna
	 */
	public OrdenPagina(String col, String modo) {
		this(col, (OrdenType.DESC.name().equalsIgnoreCase(modo)) ? OrdenType.DESC : OrdenType.ASC);
	}

	/**
	 * Valida las columnas
	 */
	@SuppressWarnings("rawtypes")
	public OrdenPagina valid(Class entityClass) {
		if (column != null) {
			Method method = getterMethod(entityClass);

			if (method != null){
				validBoolean = (method != null && method.getAnnotation(Transient.class) == null);
			}
				
		}

		//Validar demas columnas
		if (priorSort != null){
			priorSort.valid(entityClass);
		}

		return this;
	}

	/**
	 * Devuelve el metodo GetProperty or IsProperty
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Method getterMethod(Class entityClass, boolean... retryIsBoolean) {
		Method method = null;
		try {
			String getter = (retryIsBoolean.length>0) ? "is" : "get" + WordUtils.capitalize(getColumnName());
			method = entityClass.getMethod(getter);

		} catch (SecurityException e) {
			log.error("error", e);
		} catch (NoSuchMethodException e) {
			//Intentar con Is + Property
			if (retryIsBoolean.length == 0){
				return getterMethod(entityClass, true);
			}
		}
		return method;
	}

	public String getColumnName() {
		int tieneAlias = column.indexOf('.');
		if (tieneAlias > 0){
			return column.substring(tieneAlias + 1);
		} else {
			return column;
		}
			
	}

	/**
	 * Devuelve HQL
	 */
	public String hql() {
		return hql(null);
	}

	/**
	 * Indica si debe ordenar
	 */
	public boolean isEnabled() {
		return this.column != null && isValid();
	}

	public boolean isValid() {
		return (validBoolean || (priorSort != null && priorSort.isValid()));
	}

	/**
	 * Devuelve HQL
	 */
	public String hql(String alias) {
		
		StringBuffer hql = new StringBuffer();

		if (validBoolean) {

			hql.append(column).append(' ').append(modo);
			if (StringUtils.isNotEmpty(alias) && column.indexOf('.') < 0 ) {
				String hqlString = hql.toString(); 
				hql.delete(0, hql.length());
				hql.append(alias).append('.').append(hqlString);
			}
		}

		if (priorSort != null) {
			if (validBoolean){
				hql.append(priorSort.hql(alias)).append(", ").append(hql);
			}
			else{
				hql.append(priorSort.hql(alias));
			}
		}

		return hql.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override public String toString() {
		return hql();
	}

	/**
	 * Next Sort
	 */
	public OrdenPagina asc(String column) {
		OrdenPagina x = ascOrden(column);
		x.priorSort = this;
		return x;
	}

	public OrdenPagina nocount() {
		noCountBoolean = true;
		return this;
	}

	public boolean isCount() {
		return !noCountBoolean;
	}

	/**
	 * Next Sort
	 */
	public OrdenPagina desc(String column) {
		OrdenPagina x = descOrden(column);
		x.priorSort = this;
		return x;
	}



	/**
	 * Instance
	 */
	public static OrdenPagina ascOrden(String col) {
		return new OrdenPagina(col, OrdenType.ASC);
	}

	/**
	 * Disabled OrdenPagina (No ordenar)
	 * setEnabled = false!
	 */
	public static OrdenPagina noOrden() {
		return new OrdenPagina(null, OrdenType.ASC);
	}

	/**
	 * Instance
	 */
	public static OrdenPagina descOrden(String col) {
		return new OrdenPagina(col, OrdenType.DESC);
	}

	public void setPriorSort(OrdenPagina priorSort) {
		this.priorSort = priorSort;
	}

}
