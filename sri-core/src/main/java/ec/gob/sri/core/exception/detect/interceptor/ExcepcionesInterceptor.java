/**
 * Clase ExcepcionesInterceptor.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.exception.detect.interceptor;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;

import ec.gob.sri.core.exception.base.BaseExcepcion;
import ec.gob.sri.core.exception.detect.interceptor.types.InterceptorEnlaceExcepciones;
import ec.gob.sri.core.exception.handle.ContextoFalla;
import ec.gob.sri.core.exception.handle.ManejadorExcepciones;
import ec.gob.sri.core.exception.handle.impl.AnalizadorExcepcionesBaseImpl;
import ec.gob.sri.core.exception.handle.impl.ContextoFallaBaseImpl;
import ec.gob.sri.core.util.Constantes;

/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
@Interceptor
@InterceptorEnlaceExcepciones
public class ExcepcionesInterceptor {
	
	@Inject Logger log;
	
	@Inject AnalizadorExcepcionesBaseImpl analizer;

	/**
	 * 
	 */
	public ExcepcionesInterceptor() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ic
	 * @return
	 * @throws Exception
	 */
	@AroundInvoke
	public Object detectFailure(InvocationContext ic) throws BaseExcepcion {
		
		// Antes de la Excepcion se podria copiar el estado del objeto y 
		// poner un punto de fallo para poder repetir la invocacion
		
		Object returnValue = null;
		
		log.info("DETECT FAILURE START");
		
		try {
			returnValue = ic.proceed();
						
		} catch (IllegalArgumentException|IllegalStateException|EJBException|BaseExcepcion excepcion) {
			return handleException(ic, excepcion);
		} catch (Exception excepcion){
			throw new BaseExcepcion(excepcion, Constantes.ERROR_INTERNAMENTE_SERVICIO);
		}finally{
			log.info("DETECT FAILURE FINISH");
		}				
		
		return returnValue;		
	}
	
	private Object handleException(InvocationContext ic, Exception ex) {
		ContextoFalla fc = new ContextoFallaBaseImpl(ic, ex);
		
		ManejadorExcepciones strategy = analizer.loadStrategies(fc);
		
		if (strategy == null){
			throw (ex instanceof BaseExcepcion)? (BaseExcepcion) ex: new BaseExcepcion(ex, Constantes.ERROR_INTERNAMENTE_SERVICIO);
		}else{
			return strategy.handleFilure(fc);				
		}					
		
	}

	

}
