/**
 * Clase AnalizadorExcepciones.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.exception.handle;



/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
public interface AnalizadorExcepciones {

	
	/**
	 * 
	 * @param fc
	 * @return
	 */
	ManejadorExcepciones loadStrategies(ContextoFalla fc);
	
}
