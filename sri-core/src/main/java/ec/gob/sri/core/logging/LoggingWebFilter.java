/**
 * Clase LoggingWebFilter.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.logging;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.MDC;

/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
@WebFilter(value = "/*")
public class LoggingWebFilter implements Filter {

	@Inject Logger log;

	FilterConfig filterConfig;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		try {
	    	MDC.put("servidorRemoto", "servidor remoto de request: "+request.getRemoteHost());
	    	
			chain.doFilter(request, response);
			
	    	MDC.clear();
		} catch (Exception e) {
			//TODO hacer algo para que esto capture las excepciones nice
			if (response instanceof HttpServletResponse){
				HttpServletResponse hResponse = (HttpServletResponse) response;
				hResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		} 
		
	}

	@Override
	public void destroy() {
		log.debug("destroy");
	}

}
