/**
 * Clase EntidadBaseEliminable.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import ec.gob.sri.core.model.base.EntidadBase;
import ec.gob.sri.core.model.base.EntidadUtil;
import ec.gob.sri.core.model.types.Identificable;

/**
 * Solo para definir Entidades con Id / No descriptibles (No tienen SearchField)
 * @author usuario
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public abstract class EntidadSimple<T extends Serializable> extends EntidadBase implements Identificable {

	private static final long serialVersionUID = 4625468424455166215L;
	public static final String ID = "id";
	
	protected T id;

	public T getId() {
		return id;
	}

	public void setId(T id) {
		this.id = id;
	}

	public T primaryKey() {
		return getId();
	}

	public void removePk() {
		setId(null);
	}

	public String primaryKeyName() {
		return ID;
	}

	@SuppressWarnings("unchecked")
	public Class<?> primaryKeyType() {
		return (Class<T>) EntidadUtil.primaryKeyTypeLookup(getClass());
	}



}
