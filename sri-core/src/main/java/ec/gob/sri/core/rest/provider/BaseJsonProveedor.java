/**
 * Clase BaseJsonProveedor.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.rest.provider;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.plugins.providers.jackson.ResteasyJackson2Provider;
import org.slf4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.PropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

import ec.gob.sri.core.rest.datatable.TablaDatos;
import ec.gob.sri.core.rest.datatable.TablaDatosFiltro;
import ec.gob.sri.core.util.DateUtil;
import ec.gob.sri.core.util.ListUtil;

/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
@Provider
@Consumes({ "application/*+json", "text/json" })
@Produces({ "application/*+json", "text/json" })
public class BaseJsonProveedor extends ResteasyJackson2Provider /*Test*/{

	@Inject
	Logger logger;

	public static final SimpleDateFormat DATE_FMT = new SimpleDateFormat(DateUtil.DEFAULT_DATE_FMT);
	public static final SimpleDateFormat DATE_TIME_FMT = new SimpleDateFormat(
			DateUtil.DEFAULT_DATE_FMT + " " + DateUtil.DEFAULT_TIME_FMT);

	@Override

	public void writeTo(Object value, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, Object> headers, OutputStream body) throws IOException {

		ObjectMapper mapper = locateMapper(type, mediaType);
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.setDateFormat(DATE_TIME_FMT);
		mapper.registerModule(new Hibernate5Module());

		SimpleFilterProvider filters = new SimpleFilterProvider();

		if (annotations != null && annotations.length > 0) {

			for (Annotation annotation : annotations) {

				// Check for TablaDatos annotation, agrega un filtro
				if (annotation.annotationType().equals(TablaDatos.class)) {
					PropertyFilter propertyFilter = addDataTableFilter((TablaDatos) annotation, value);
					filters.addFilter(TablaDatos.FILTER_NAME, propertyFilter);
				}
			}
		}

		// Para que si no encuentra el filtro no se caiga, aunue no filtra nada
		filters.setFailOnUnknownId(false);

		// Setting the filters
		mapper.setFilters(filters);

		/*
		 * SimpleFilterProvider filters1 = new SimpleFilterProvider();
		 * SimpleBeanPropertyFilter serializeAllPropertyFilter1 =
		 * SimpleBeanPropertyFilter.filterOutAllExcept("nro","tipoComprobante",
		 * "rucRazonSocial"); filters1.addFilter("dataTableFilter2",
		 * serializeAllPropertyFilter1); filters1.setFailOnUnknownId(false);
		 * mapper.setFilters(filters1);
		 */

		// ObjectWriter objectWriter = mapper.writerWithDefaultPrettyPrinter();
		// logger.info(objectWriter.writeValueAsString(value));

		// Se quedan caheados los writers para que esto pueda se de manera
		// dinamica se tienen que reconfigurar los
		// writes en cada llamada, no estoy seguro si en la mejor manera, se
		// requere revision.
		// Em produccion seria bueno no limpiar los filtros
		_writers.clear();

		//try {
			super.writeTo(value, type, genericType, annotations, mediaType, headers, body);
		//} catch (Exception e) {
			// Esto no sirve de nada, la respuesta ya fue enviada al cliente en
			// la clase padre(al hacer close se hace un flush)
			// Hay q verificar si se cambia esto debido a que pueden haber
			// problemas en la
			// serializacion de los objetos y la respuesta sigue enviandose con
			// HTTP status code 200
			//e.printStackTrace();
		//}
	}

	@Override
	public Object readFrom(Class<Object> type, Type genericType, Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, String> httpHeaders, InputStream entityStream) throws IOException {

		ObjectMapper mapper = locateMapper(type, mediaType);
		mapper.setDateFormat(DATE_FMT);

		return super.readFrom(type, genericType, annotations, mediaType, httpHeaders, entityStream);
	}

	@SuppressWarnings("unchecked")
	private PropertyFilter addDataTableFilter(TablaDatos dataTable, Object value) {
		Set<String> mDataProps = new HashSet<>();
		SimpleBeanPropertyFilter serializeAllPropertyFilter = SimpleBeanPropertyFilter.serializeAllExcept();
		String parameters = dataTable.propiedades();
		TablaDatosFiltro dataTableFilterType = dataTable.tipoFiltro();

		if (dataTableFilterType == TablaDatosFiltro.FILTER_DEFAULT) {

			// mDataProps via TablaDatos Annotation
			if (parameters.compareTo(TablaDatos.EMPTY_FILTER) != 0) {
				mDataProps.addAll(ListUtil.toList(ListUtil.split(parameters)));
			}

			// mDataProps via TablaDatos Interceptor
			if (value != null && Map.class.isAssignableFrom(value.getClass())) {
				Map<String, Object> valueMap = (Map<String, Object>) value;

				Set<String> mDataPropsCalculated = (Set<String>) valueMap.get(TablaDatos.FILTER_DATA_PROPERTIES);

				if (mDataPropsCalculated != null && !mDataPropsCalculated.isEmpty()) {
					mDataProps.addAll(mDataPropsCalculated);
				}
				valueMap.remove(TablaDatos.FILTER_DATA_PROPERTIES);
			}
			
			if (!mDataProps.isEmpty()){
				serializeAllPropertyFilter = SimpleBeanPropertyFilter.filterOutAllExcept(mDataProps);	
			}
			

		} else {
			// En el caso de NO_FILTER que se serialize todo
		}

		return serializeAllPropertyFilter;

	}

}
