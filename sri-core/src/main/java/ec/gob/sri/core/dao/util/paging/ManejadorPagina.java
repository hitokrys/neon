/**
 * Clase ManejadorPagina.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.dao.util.paging;



/**
 * Dao Option
 * @author usuario
 * @since Jul 10, 2011
 */
public class ManejadorPagina {
	
	public static final int MAX_ROWS = 100;
	public static final int PAGE_SIZE = MAX_ROWS;
	

	private int indiceInicioInt, maximoFilasInt;
	private boolean distinctBoolean = true; //Default
	private OrdenPagina orden;

	private int numeroPaginaInt, tamanioPaginaInt;

	/**
	 * Distinct = TRUE
	 */
	public ManejadorPagina(
			int startIndex,
			int maxRows,
			OrdenPagina orderBy) {
		this(startIndex, maxRows, orderBy, true);
	}

	/**
	 * startIndex / maxRows
	 */
	public ManejadorPagina(
			int startIndex,
			int maxRows,
			OrdenPagina orderBy,
			boolean distinct) {

		this.indiceInicioInt = startIndex;
		this.maximoFilasInt = maxRows;
		this.orden = orderBy;
		this.distinctBoolean = distinct;

		if (maxRows <= 0){
			this.numeroPaginaInt = 0;
		} else {
			this.numeroPaginaInt = 1 + (startIndex / maxRows);
		}
			

		this.tamanioPaginaInt = maxRows; //-1
	}

	/**
	 * By PageNum
	 */
	public static ManejadorPagina numeroPagina(int pageNum, int pageSize, OrdenPagina orderBy) {
		int si = ((pageNum==0)? 0: pageNum-1) * pageSize;
		int mr = pageSize;

		return new ManejadorPagina(si, mr, orderBy, true);
	}

	/**
	 * By Top items (FirstPage)
	 */
	public static ManejadorPagina primerasFilas(int numItems, OrdenPagina orderBy) {
		return numeroPagina(0, numItems, orderBy);
	}

	/**
	 * By PageNum
	 */
	public static ManejadorPagina numeroPagina(int pageNum, int pageSize, OrdenPagina orderBy, boolean distinct) {
		int si = ((pageNum==0)? 0: pageNum-1) * pageSize;
		int mr = pageSize;

		return new ManejadorPagina(si, mr, orderBy, distinct);
	}




	public static ManejadorPagina todasLasFilas(OrdenPagina orderBy) {
		return new ManejadorPagina(0, -1, orderBy);
	}


	public boolean distinct() {
		return distinctBoolean;
	}

	public ManejadorPagina distinct(boolean distinct) {
		this.distinctBoolean = distinct;
		return this;
	}

	/**
	 * Set the orderby
	 */
	public void setOrderBy(OrdenPagina orderBy) {
		this.orden = orderBy;
	}

	/**
	 * NumPage
	 */
	public int numeroPagina() {
		return numeroPaginaInt;
	}

	/**
	 * PageSize
	 */
	public int tamanioPagina() {
		return tamanioPaginaInt;
	}

	/**
	 * ColumnOrder By
	 */
	public OrdenPagina ordenarPor() {
		return orden;
	}

	/**
	 * Max Rows
	 */
	public int maximoFilas() {
		return maximoFilasInt;
	}

	/**
	 * Start index
	 */
	public int indiceInicio() {
		return indiceInicioInt;
	}


	@Override
	public String toString() {
		String distinct1 = (distinctBoolean) ? "distinct" : "";		
		return "ManejadorPagina " + distinct1  + "[" + indiceInicioInt + ", " + maximoFilasInt + "] @ " + orden ;
	}

}
