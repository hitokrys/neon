/**
 * Clase Identificable.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.model.types;

import java.io.Serializable;

/**
 * Entity Bean with Id
 * @author hrobayo@gmail.com Mar 31, 2010
 */
public interface Identificable  {
	
	String ID = "id";

	/**
	 * Id
	 */
	Serializable getId();

	

}
