/**
 * Clase EntidadBase.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.model.base;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ec.gob.sri.core.rest.datatable.TablaDatos;

/**
 * Entidad base con Id Numerico
 *
 * @author mrobayo
 * 
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonFilter(value=TablaDatos.FILTER_NAME)
//@JsonTypeInfo(use=Id.CLASS, include=As.PROPERTY, property="class")
public abstract class EntidadBase implements Serializable {

	/**
	 * Si un objeto es igual a otro en cada uno de sus atributos son iguales
	 */
    public boolean equals(Object o) {
    	if (o instanceof EntidadBase && primaryKey() != null) {
    		Serializable oPk = ((EntidadBase) o).primaryKey();
    		return primaryKey().equals(oPk);
    	}
    	else if (o instanceof Long && primaryKey() != null) {
    		return primaryKey().equals(o);
    	}
        return EqualsBuilder.reflectionEquals(this, o);
    }
    
    @Override
    public int hashCode() {
    	return super.hashCode();
    }


	/**
     * Indica si un objeto es nuevo!
     */
    @JsonIgnore
	public final boolean isNewEntity() {
		return (primaryKey() == null);
	}

	/**
	 * Nombre de la columna que es la PK de esta entidad
	 * @return
	 */
	public abstract String primaryKeyName();

	/**
	 * Devuelve el tipo de dato de la PK
	 * @return
	 */
	public abstract Class<?> primaryKeyType();

	/**
	 * Devuelve la clave primaria de este objeto
	 * @return
	 */
	public abstract Serializable primaryKey();





}
