/**
 * Clase JPAUtil.java 31 de dic. de 2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.dao.util;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Query;

import ec.gob.sri.core.util.StringUtil;

/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
public final class JPAUtil {
	
	public static final String P = "p";
	private static final String PORCENTAJE ="%";
	
	private JPAUtil(){}
	
	
	/**
	 * Add parametros desde Index=0
	 */
	public static void addParameter(Query hql, int idx, Object param, boolean addit) {
		if (!addit){
			return;
		}
		if (param instanceof Boolean) {
			hql.setParameter(idx, (Boolean) param);
		} else if (param instanceof Date) {
			hql.setParameter(idx, (Date) param);
		} else if (param instanceof Integer) {
			hql.setParameter(idx, (Integer) param);			
		} else if (param instanceof Number) {
			Number value = (Number) param;
			hql.setParameter(idx, value.longValue());
		} else if (param instanceof Collection) {
			throw new IllegalArgumentException(
					"Collection is not supported por Index-Based parameter");
		} else if (param instanceof String) {
			hql.setParameter(idx, (String) param);
		} else {
			hql.setParameter(idx, param);
		}
	}



	/**
	 * Parametros x nombre donde los parametros van desde: p0, p1, p2
	 */
	public static void addNamedParameter(Query hql, int idx, Object param, boolean addit) {
		if (!addit){
			return;
		}

		if (param instanceof Boolean) {
			hql.setParameter(P + idx, (Boolean) param);
		} else if (param instanceof Date) {
			hql.setParameter(P + idx, (Date) param);
		} else if (param instanceof Number) {
			Number value = (Number) param;
			hql.setParameter(P + idx, value.longValue());
		} else if (param instanceof Long[]) {
			hql.setParameter(P + idx, (Long[]) param);
		} else if (param instanceof String[]) {
			hql.setParameter(P + idx, (String[]) param);
		} else if (param instanceof Enum<?>[]) {
			hql.setParameter(P + idx, (Enum<?>[]) param);
		} else if (param instanceof Enum<?>) {
			hql.setParameter(P + idx, (Enum<?>) param);
		} else {
			hql.setParameter(P + idx, (String) param);
		}
	}
	
	/**
	 * Add parametros desde Index=0, verifica el where si debe usar
	 * Index-Based / Named parameter
	 */
	public static void addParameter(Query hql, Object[] params, String where) {
		boolean addit = StringUtil.isNotEmpty(where);
		if (!addit){
			return; // No add parametros si no hay where clause
		}
			

		boolean indexBased = where.indexOf('?') >= 0;

		int idx = 1;
		for (Object param : params) {
			if(param != null){
				if (indexBased){
					JPAUtil.addParameter(hql, idx++, param, addit);
				}					
				else {
					JPAUtil.addNamedParameter(hql, idx++, param, addit);
				}
					
			}
		}
	}
	
	/**
	 * Like null% % 1% 22% %333%
	 */
	public static String like(final String value) {
		String trim = StringUtil.trimToEmpty(value);
		
		String likeTemporal = trim;
		StringBuffer like = new StringBuffer(likeTemporal);

		if (trim.indexOf('%') >= 0) {
			return like.toString(); // Si ya contiene % no volver a incluir %
		}
		int both = trim.length();
		if (both > 2) {
			//like = "%" + like;
			like = new StringBuffer("");			
			like.append(PORCENTAJE).append(likeTemporal);
		}
		//return like + "%"; // Add porcentaje al final
		return like.append(PORCENTAJE).toString(); // Add porcentaje al final
	}
	
	



	/**
	 * Agrega una clausula
	 * @param hql
	 * @param like
	 * @return
	 */
	public static String and(String hql, String clausula) {
		StringBuffer query = new StringBuffer("");
		if (StringUtil.isNotEmpty(hql)){
			//query = " AND ";
			query.append(" AND ");
		}
		//query += clausula;
		query.append(clausula);
		return query.toString();
	}	
}
