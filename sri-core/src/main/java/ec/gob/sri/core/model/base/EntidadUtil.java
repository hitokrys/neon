/**
 * Clase EntidadUtil.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.model.base;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import ec.gob.sri.core.exception.base.BaseExcepcion;
import ec.gob.sri.core.model.EntidadSimple;
import ec.gob.sri.core.model.types.Identificable;
import ec.gob.sri.core.util.StringUtil;

/**
 *
 * @author usuario
 */
public final class EntidadUtil {

	private static final EntidadUtil THIS = new EntidadUtil(); 
	
	private EntidadUtil(){		
	}
	
	public static final EntidadUtil getInstance(){
		return THIS;
	}
	
	
	@SuppressWarnings("unchecked")
	public static <T> T getId(EntidadSimple<?> entidad) {
		if (isChild(entidad)) {
			return (T) entidad.getId();
		}
		return null;
	}


	/**
	 * Valida is ID esta lleno
	 */
	public static <T extends Identificable> boolean isChild(final T entidad) {
		if (entidad != null && entidad.getId() != null) {

			if (entidad.getId() instanceof String) {
				return StringUtil.isNotEmpty( (String) entidad.getId());
			}
			else {
				return true;
			}
		}

		return false;
	}

	/**
	 * Valida si ID esta vacio
	 */
	public static <T extends Identificable> boolean isEmpty(final T entidad) {
		return !isChild(entidad);
	}

	/**
	 * Indica si es un child con id
	 */
	public static <T extends Identificable> T getChild(final T entidad) {
		return (isChild(entidad)) ? entidad : null;
	}

	/**
	 * Consulta el nombre la clave primaria de una entidad base
	 *
	 * @param entityClass
	 * @return
	 */
	public static String primaryKeyName(Class<?> entityClass) {
		String pkName = null;
//		EntidadBase entidad = newInstance(entityClass);
//		if (entidad != null) {
//			pkName = entidad.primaryKeyName();
//		}
		//TODO Como obtengo el nombre de la clave primaria???

		return pkName;
	}

	/**
	 * Consulta el tipo de datos de la clave primaria
	 *
	 * @param entityClass
	 * @return
	 */
	public static Class<?> primaryKeyType(Class<?> entityClass) {
		Class<?> pkName = null;
//		EntidadBase entidad = newInstance(entityClass);
//		if (entidad != null) {
//			pkName = entidad.primaryKeyType();
//		}
		//TODO Como obtengo el nombre el tipo clave primaria???
		
		return pkName;
	}


	/**
	 * Primary key lookup
	 *
	 * @param entityClass
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Serializable> Class<T> primaryKeyTypeLookup(Class<T> entityClass) {
		// obtain anonymous, if any, class for 'this' instance
		Type superclass = entityClass.getGenericSuperclass();

		// test if an anonymous class was employed during the call
		if (!(superclass instanceof ParameterizedType)) {
			if (superclass == null){
				throw new BaseExcepcion("This instance should belong to an anonymous class");
			} else {
				return primaryKeyTypeLookup((Class<T>) superclass);
			}
				
		}

		// obtain RTTI of all generic parameters
		final Type[] types = ((ParameterizedType) superclass).getActualTypeArguments();

		int pos = 0;

		// test if enough generic parameters were passed
		if (pos >= types.length) {
			throw new BaseExcepcion(
					String.format(
							"Could not find generic parameter #%d because only %d parameters were passed",
							pos, types.length));
		}

		if (!(types[pos] instanceof Class<?>)) {
			throw new BaseExcepcion(
					"Generic tipo is not a class but declaration definition(all you get is \"[T]\") "
							+ types[pos]);
		}

		// return the tipo descriptor of the requested generic parameter
		return (Class<T>) types[pos];
	}

}
