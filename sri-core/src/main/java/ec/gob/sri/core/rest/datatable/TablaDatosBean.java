/**
 * Clase TablaDatosBean.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.rest.datatable;

import java.util.Collections;
import java.util.List;

import ec.gob.sri.core.dao.util.paging.ManejadorPagina;
import ec.gob.sri.core.dao.util.paging.OrdenPagina;

/**
 * @author Hugo Camilo Robayo Ayala
 * Los nombres estan en ingles debido a que asi lo requier el plugin
 */
public class TablaDatosBean {
	
	private List<TablaDatosOrdenColumna> order = Collections.emptyList();
	private List<String> columns = Collections.emptyList();
	private Integer draw = 0;
	private Integer start = 0;
	private Integer length = 10;

	
	public TablaDatosBean() {
	}
	
	
	public List<String> getColumns() {
		return columns;
	}
	public void setColumns(List<String> columns) {
		this.columns = columns;
	}
	public Integer getDraw() {
		return draw;
	}
	public void setDraw(Integer draw) {
		this.draw = draw;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
 
	public ManejadorPagina paging(){
		OrdenPagina orden = null;
		if (!order.isEmpty()){
			orden  = new OrdenPagina(order.get(0).getColumn(), order.get(0).getDir());	
		}
		return new ManejadorPagina(start, length, orden).distinct(false);
	}


	public List<TablaDatosOrdenColumna> getOrder() {
		return order;
	}


	public void setOrder(List<TablaDatosOrdenColumna> order) {
		this.order = order;
	}


	

}
