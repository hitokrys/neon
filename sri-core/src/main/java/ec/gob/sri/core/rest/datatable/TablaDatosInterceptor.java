/**
 * Clase TablaDatosInterceptor.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.rest.datatable;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;

import ec.gob.sri.core.dao.util.paging.PaginaDatos;
import ec.gob.sri.core.rest.util.MapaDeclarado;
import ec.gob.sri.core.util.StringUtil;

/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
@Interceptor
@TablaDatos
public class TablaDatosInterceptor {
	
	@Inject Logger log;
	
	@AroundInvoke
	public Object interceptDataTable(InvocationContext ic) throws Exception {
		Object returnValue = null;
		
		log.debug("DATA TABLE INTERCEPTOR START");
		
		Object[] parametres =  ic.getParameters();
		
		returnValue = ic.proceed();
		
		if ( parametres != null && parametres.length > 0 &&  MapaDeclarado.class.isAssignableFrom(parametres[0].getClass()) ){
			
			MapaDeclarado<String, Object> typeMap = (MapaDeclarado<String, Object>) parametres[0];
			
			if (returnValue != null && Map.class.isAssignableFrom(returnValue.getClass())){				
				prepareDataTable(typeMap,  (Map) returnValue);
			}
		}
				
		log.debug("DATA TABLE INTERCEPTOR FINISH");
		
		return returnValue;

	}
	
	@SuppressWarnings("unchecked")
	private void  prepareDataTable(MapaDeclarado<String, Object> typeMap,  Map returnValue){
		TablaDatosBean dataTableInfo = typeMap.getObjetoDeclarado(TablaDatosBean.class);
		
		if (dataTableInfo != null){
			
			Set<String> properties = new HashSet<String>();				
			for (String property : dataTableInfo.getColumns()) {
				
				if (property!= null && property.indexOf('.') > 0){
					String[] test = StringUtil.split2(property, "\\.");
					for (String prop : test) {
						properties.add(prop);
					}
				}else{
					properties.add(property);	
				}
				
				
				
				
			}
			Map tempMap = (Map) returnValue;					
			//Agrego automaticamente por autodescubrimiento las propiedades que se serializan
			tempMap.put(TablaDatos.FILTER_DATA_PROPERTIES, properties);		
			tempMap.put(TablaDatos.DATA_ECHO,dataTableInfo.getDraw());
			
			Object byPageObject = tempMap.get(TablaDatos.DATAPAGE_PROPERTY);
			
			if (byPageObject != null && PaginaDatos.class.isAssignableFrom(byPageObject.getClass())){
				PaginaDatos byPage = (PaginaDatos) byPageObject;
				tempMap.put(TablaDatos.DATA_TOTAL_RECORDS,byPage.numeroFilas);
				tempMap.put(TablaDatos.DATA_TOTAL_DISPLAY_RECORDS,byPage.numeroFilas);		
				tempMap.put(TablaDatos.DATA_PROPERTY,byPage.filas);
				//Remuevo la propiedad PaginaDatos
				tempMap.remove(TablaDatos.DATAPAGE_PROPERTY);
			}
			
			
		}
		
	}
	

}
