/**
 * Clase MapaDeclarado.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.rest.util;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * @author Hugo Camilo Robayo Ayala
 *
 */


@JsonDeserialize(using=MapaDeclaradoDesrializadorJson.class,as=MapaDeclarado.class)
		
public class MapaDeclarado<S, T> extends HashMap<S, T> implements Map<S, T>{
	private static final long serialVersionUID = 5238477720380756389L;

	@SuppressWarnings("unchecked")
	public  <Q> Q deserializarObjeto(Class<Q> klass){
		if (containsKey(MapaDeclaradoDesrializadorJson.TYPE_IDENTIFIER+klass.getCanonicalName())){
			Object object = get(MapaDeclaradoDesrializadorJson.TYPE_IDENTIFIER+klass.getCanonicalName());
			if (object != null){
				return (Q) object;	
			}			 
		}
		return null;
	}
	
	public  <Q> Q getObjetoDeclarado(Class<Q> klass, boolean create){
		
		Q q = deserializarObjeto(klass);
		if ( q == null && create){
			try {
				q = klass.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return q;		
	}
	
	public  <Q> Q getObjetoDeclarado(Class<Q> klass){		
		return deserializarObjeto(klass);		
	}	
	
	
}
