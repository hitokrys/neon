/**
 * Clase TipoError.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.exception.base.types;

import ec.gob.sri.core.enumeradores.EnumeracionTitulada;

/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
public enum TipoError implements EnumeracionTitulada{

	INTERNO("Error Interno"),
	SERVICIO("Error de Servicio"),
	CLIENTE("Error de Cliente");
	
	private final String value;
	
	private TipoError(final String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}	
	
	
	@Override
	public String toString() {
		if (value == null){
			getValue();
		}
		return name();
	}

	@Override
	public String getName() {
		return name();
	}
	
}
