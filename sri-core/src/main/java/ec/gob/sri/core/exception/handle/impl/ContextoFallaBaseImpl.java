/**
 * Clase ContextoFallaBaseImpl.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.exception.handle.impl;

import javax.inject.Inject;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;

import ec.gob.sri.core.exception.handle.ContextoFalla;

/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
public class ContextoFallaBaseImpl implements ContextoFalla {

	@Inject Logger log;
	
	//private InvocationContext ic;
	//private Throwable t;
	
	/**
	 * 
	 */
	public ContextoFallaBaseImpl(InvocationContext ic, Throwable t) {
		//this.ic = ic;
		//this.t = t;
		//TODO verificar como hacer el failover
		if (log != null){
			log.debug("the invocation context is: {}",ic);
			log.debug("the exceptions is: {}",t);
			
		}
	}
	
}
