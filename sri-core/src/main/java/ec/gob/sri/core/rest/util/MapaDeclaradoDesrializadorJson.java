/**
 * Clase MapaDeclaradoDesrializadorJson.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.rest.util;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ec.gob.sri.core.rest.provider.BaseJsonProveedor;



/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class MapaDeclaradoDesrializadorJson extends JsonDeserializer<Map> {
	
	public static final String TYPE_IDENTIFIER = "@class:"; 

	/* (non-Javadoc)
	 * @see com.fasterxml.jackson.databind.JsonDeserializer#deserialize(com.fasterxml.jackson.core.JsonParser, com.fasterxml.jackson.databind.DeserializationContext)
	 */

	@Override
	public Map deserialize(JsonParser jp, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		MapaDeclarado<String, Object> result = new MapaDeclarado<String, Object>();
		
		JsonNode rootNode = jp.readValueAsTree();
		
		if (!rootNode.isNull()) {
	        Iterator<Map.Entry<String,JsonNode>> fieldsIterator = rootNode.fields();
	        ObjectMapper mapper = new ObjectMapper();
	        mapper.setDateFormat(BaseJsonProveedor.DATE_FMT);

			Class klass ;
	        while (fieldsIterator.hasNext()) {

	            Map.Entry<String,JsonNode> field = fieldsIterator.next();
	            
	            try {
	                if (field != null && field.getKey().startsWith(TYPE_IDENTIFIER) ){
	                	klass = Class.forName(field.getKey().substring(7));
	                }else{
	                	klass = Object.class; 
	                }
					
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
					klass = Object.class;
				}
	            
	            result.put(field.getKey(), mapper.readValue(field.getValue().toString(),klass));
	        }     
			
		}
		
		return result;
	}

}
