/**
 * Clase Predicado.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.dao.util.jql;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ec.gob.sri.core.util.ListUtil;

/**
 * A predicate is the part of the HQL sentence following the " from ClassName "
 * sentence
 * Example usage:
 * JQLUtil query = new JQLUtil( "from MyClass obj" );
 * query.and( Predicado.eq( "property", value )
 *    .and( Predicado.ilike("property", value+"%" )
 *    .and( Predicado.eq("prop1", value ).or( Predicado.eq( "prop2", value )));
 * @author Davide Alberto Molin (davide.molin@gmail.com)
 */
public class Predicado {

	public static final Logger LOG = LoggerFactory.getLogger(Predicado.class);

    private String predicateSentence;	
    protected List<Object> paramValues;
    


    public Predicado()
    {
        this.predicateSentence = "";
        paramValues = new ArrayList<Object>();
    }
    public Predicado( String predicate )
    {
        this.predicateSentence = (predicate == null ? "" : predicate);
        paramValues = new ArrayList<Object>();
    }

    public Predicado( String predicate, Object... paramValues )
    {
        this.predicateSentence = (predicate == null ? "" : predicate);
        this.paramValues = new ArrayList<Object>();
        this.paramValues.addAll( Arrays.asList( paramValues ) );
    }

    public Predicado and( Predicado pg )
    {
    	String thisQuery = enclose(pg.getQueryString());

        if( predicateSentence.length() == 0 ) {
            this.predicateSentence += " WHERE " + thisQuery;
        }
        else {
            this.predicateSentence += "   AND " + thisQuery;
        }

        this.paramValues.addAll( Arrays.asList( pg.getParametersValue() ) );

        LOG.debug("PREDICATE: " + thisQuery + " [" + ListUtil.join(pg.getParametersValue()) + "]");
        return this;
    }

    public Predicado orPeridcate( Predicado pg )
    {
        if( predicateSentence.length() == 0 ) {
            this.predicateSentence = " WHERE " + enclose(pg.getQueryString());
        }
        else {
            this.predicateSentence += " OR " + enclose(pg.getQueryString());
        }
        this.paramValues.addAll( Arrays.asList( pg.getParametersValue() ) );
        return this;
    }

    private String enclose( String src )
    {
        return "(" + src + ")";
    }

    public static Predicado enclose( Predicado pg )
    {
        return new Predicado( "(" + pg.getQueryString() + ")",
                                                   pg.getParametersValue() );
    }

    /**
	 *
	 */
	public static Predicado between(String expresion, Object desde, Object hasta) {
		return new Predicado(expresion + " BETWEEN ? AND ?", desde, hasta);
	}

    public static Predicado eqPredicate( String expression, Object value )
    {
        return new Predicado( expression + " = ?", value );
    }

    public static Predicado neq( String expression, Object value )
    {
        return new Predicado( expression + " != ?", value );
    }

    public static Predicado like( String expression, String value)
    {    
        return new Predicado( expression + " LIKE ?", initailizeValue(value) );
    }

    public static Predicado ilike( String expression, String value )
    {
        
        return new Predicado( "UPPER(" + expression + ") LIKE ?",
        		initailizeValue(value).toUpperCase() );
    }

    public static Predicado inPredicate( String expression, Object... values )
    {
        StringBuffer cond = new StringBuffer() ;
        cond.append(expression).append(" IN (");
        for( int i=0; i<values.length; i++ )
        {
            cond.append((cond.toString().endsWith("(") ? "?" : ",?" ))  ;
        }
        cond.append(')') ;
        return new Predicado( cond.toString(), values );
    }

    @Override
    public String toString()
    {
        return getQueryString();
    }

    public String getQueryString()
    {
        return predicateSentence;
    }

    public Object[] getParametersValue()
    {
        return paramValues.toArray();
    }
    
    private static String initailizeValue(String value){
    	return (value == null)? "":value;
    }

}