/**
 * Clase PaginaDatos.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.dao.util.paging;

import java.util.Collections;
import java.util.List;

/**
 * Page UI
 * 
 * @author hrobayo@gmail.com 11/02/2010
 */
public class PaginaDatos<T> {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static final PaginaDatos EMPTY = new PaginaDatos(Collections.EMPTY_LIST, 0);
	
	public List<T> filas;
	public int numeroFilas;
	public int tamanioPagina;


	public PaginaDatos() {
	}

	public PaginaDatos(List<T> rows, int rowCount) {
		this.filas = rows;
		this.numeroFilas = rowCount;
	}

	public PaginaDatos(List<T> rows, int rowCount, int pageSize) {
		this.filas = rows;
		this.numeroFilas = rowCount;
		this.tamanioPagina = pageSize;
	}


	public boolean isNotEmpty() {
		if (filas == null) {
			return false;
		} else {
			return !filas.isEmpty();
		}

	}

	public List<T> getFilas() {
		return filas;
	}

	public void setFilas(List<T> rows) {
		this.filas = rows;
	}

	public int getNumeroFilas() {
		return numeroFilas;
	}

	public void setNumeroFilas(int rowCount) {
		this.numeroFilas = rowCount;
	}

	@Override
	public String toString() {
		int size = 0;

		if (filas != null) {
			size = filas.size();
		}
		return "PaginaDatos: [" + size + "] / " + numeroFilas;
	}

	public int getTamanioPagina() {
		return tamanioPagina;
	}

	public void setTamanioPagina(int pageSize) {
		this.tamanioPagina = pageSize;
	}

}
