/**
 * Clase ProductorRecursos.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
public class ProductorRecursos {
	
	
	@Produces
	public Logger getLogger(InjectionPoint ip) { 
	    String category = ip.getMember().getDeclaringClass().getName();
	    return LoggerFactory.getLogger(category); 
	}
	

	
	

	
}
