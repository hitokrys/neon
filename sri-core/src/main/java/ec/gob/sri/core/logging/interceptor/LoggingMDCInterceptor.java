/**
 * Clase LoggingMDC.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.logging.interceptor;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;
import org.slf4j.MDC;

import ec.gob.sri.core.logging.interceptor.types.LoggingMDC;

/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
@LoggingMDC
@Interceptor
public class LoggingMDCInterceptor {
	
    @Inject Logger log;
    
    public LoggingMDCInterceptor() {
    }

    @AroundInvoke
    public Object mcdLogMethod(InvocationContext invocationContext) throws Exception {
    	
    	MDC.put("usuario", "camilex");
    	log.error("ya simon");
    	Object o = null;
    	    	
    	o = invocationContext.proceed();    	

    	//MDC.clear();

        return o; 
    }

}
