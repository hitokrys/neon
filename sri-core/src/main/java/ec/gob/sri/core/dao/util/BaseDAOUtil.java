/**
 * Clase BaseDAOUtil.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.dao.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import org.slf4j.Logger;

import ec.gob.sri.core.dao.util.paging.OrdenPagina;
import ec.gob.sri.core.exception.base.BaseExcepcion;
import ec.gob.sri.core.model.base.EntidadUtil;
import ec.gob.sri.core.model.types.Eliminable;
import ec.gob.sri.core.util.StringUtil;

/**
 *
 * @author mrobayo@gmail.com Mar 22, 2010
 */
public final class BaseDAOUtil {
	
	public static final String P = "p";	
	public static final String
	SELECT   = "SELECT",
	FROM     = "FROM",
	WHERE    = "WHERE",
	AND      = "AND",
	OR       = "OR",
	JOIN     = "JOIN",
	AS       = "AS",
	FETCH    = "FETCH",
	DISTINCT = "DISTINCT",
	_AND	 = " AND ";
	

	// Campos para injectar informacion de Busqueda y Auditoria
	public static final String  FECHAINACTIVO_SET="setFechaInactivo", FECHAINACTIVO_GET="getFechaInactivo";
	private static final String CARACTER_VACIO=" ";


	@Inject static Logger log;
	
	private BaseDAOUtil(){}
 
	/**
	 * Si tiene campo search (lo setea)
	 */
	public static void prepararParaGuardar(Object entity) {

		// Fecha de inactivo si esta inactivo
		if (entity instanceof Eliminable) {
			Eliminable i = (Eliminable) entity;
			try {
				if (!i.isActivo()){
					prepareEntity(entity);
				}

			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				log.error("Error preparing to store - inactivo ",e);
			} 

		}

	}
	private static void prepareEntity(Object entity) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Method mGet = entity.getClass().getMethod(FECHAINACTIVO_GET);
		Object result = mGet.invoke(entity);
		if (result == null){
			Method mSet = entity.getClass().getMethod(FECHAINACTIVO_SET, Date.class);
			mSet.invoke(entity, Calendar.getInstance().getTime());
		}
	}

	/**
	 * Devuele Primary Key's Name
	 */
	public static String pkName(Class<?> entityClass) {
		return EntidadUtil.primaryKeyName(entityClass);
	}

	/**
	 * Get Alias
	 */
	private static String alias(Class<?> entityClass, String w) {
		String alias;
		
		if (w.startsWith(AS)) {
			int aliasIdx = w.indexOf(' ', 3);
			if (aliasIdx < 0){
				aliasIdx = w.length();
			}

			alias = w.substring(3, aliasIdx);
		}
		else if (w.indexOf(" "+AS+" ") > 0){
			int beginIndex = w.indexOf(" "+AS+" ");			
			int endIndex = w.indexOf(" ", beginIndex+4);
			alias = w.substring(beginIndex+4, (endIndex == -1)? w.length():endIndex);
		}
		else {
			alias = entityClass.getSimpleName().toLowerCase();
		}

		return alias;
	}
	
	private static String firstCheckQuery(StringBuffer hql, StringBuffer w1, Class<?> entityClass){
		String w = w1.toString();
		String alias = null;
		
		if (w.startsWith(FROM) && w.indexOf(WHERE) >= 0){
			String[] splitQuery = w.split(WHERE);
			if (splitQuery != null && splitQuery.length > 0 ){
				//hql = splitQuery[0];	
				hql.append(splitQuery[0]);
			}
			if (splitQuery != null && splitQuery.length > 1 ){
				w = WHERE + " "+ splitQuery[1];	
			}
			alias = alias(entityClass, hql.toString());
			
		}else if (w.startsWith(FROM) && w.indexOf(WHERE) < 0){
			//hql = w;
			hql.append(w);
			w = "";
			alias = alias(entityClass, hql.toString());							
		}else{
			//hql += "FROM " + entityClass.getSimpleName();
			hql.append("FROM ").append(entityClass.getSimpleName());
			
			alias = alias(entityClass, w);			
		}
		w1.delete(0, w1.length());
		w1.append(w);
		return alias;
		
	}
	
	private static void secondCheckQuery(StringBuffer hql, String w1, String alias){
		String w = w1;
		if (w.length() > 0) {
			if (!w.startsWith(SELECT) && !w.startsWith(AS) && !w.startsWith(JOIN)
					&& !w.startsWith(WHERE) && (w.indexOf(WHERE) < 0)) {
				//hql += " " + AS + " " + alias + " WHERE";
				hql.append(CARACTER_VACIO).append(AS).append(CARACTER_VACIO).append(alias).append(" WHERE");
			}
			//hql += " " + w;
			hql.append(CARACTER_VACIO).append(w);
		}
		else if (hql.indexOf(AS) < 0) {
			//hql += " " + AS + " " + alias;
			hql.append(CARACTER_VACIO).append(AS).append(CARACTER_VACIO).append(alias);
		}
	}

	/**
	 * Construye sentencia  QUERY  extends IEntidad
	 */
	public static String query(Class<?> entityClass, final String where, OrdenPagina sort, boolean distinct) { 
		String w = StringUtil.trimToEmpty(where);		
		
		StringBuffer hql = new StringBuffer("");
		StringBuffer wBuffer = new StringBuffer(w);
		String alias = firstCheckQuery(hql, wBuffer, entityClass);
		w = wBuffer.toString();
		
		String fields = null;
		if (w.startsWith(SELECT)) {
			int fromIdx = w.indexOf(FROM);
			fields = w.substring(6, w.indexOf(FROM));
			w = w.substring(w.indexOf(AS, fromIdx+4));
		}
		 
		secondCheckQuery(hql, w, alias);
		
		// Ordenar x NOMBRE
		if (sort != null && sort.isEnabled()) {
			//hql += " ORDER BY " + sort.hql(alias);
			hql.append(" ORDER BY ").append(sort.hql(alias));
		}

		// Validar si es un JOIN
		String hqlTemporal = hql.toString();
		hql = new StringBuffer("");
		if (fields == null || alias.equals(StringUtil.trimToEmpty(fields))) {
			//hql = "SELECT " + ((distinct) ? DISTINCT : "") + " " + alias + " " + hql;
			hql.append("SELECT ").append(((distinct) ? DISTINCT : "")).append(CARACTER_VACIO).append(alias).append(CARACTER_VACIO).append(hqlTemporal);
		}else {
			if (!distinct && fields.indexOf(DISTINCT) >= 0) {
				fields = fields.replaceFirst(DISTINCT, "");
			}
			//hql = "SELECT " + fields + " " + hql;
			hql.append("SELECT ").append(fields).append(CARACTER_VACIO).append(hqlTemporal);
		}
		return hql.toString();
	}	

	private static String firstCheckCount(String w1, String where){
		String w = w1;
		if (w.startsWith(SELECT)) {
			int asIndex = w.indexOf(AS);
			if (asIndex == -1) {
				throw new BaseExcepcion("Alias no encontrado, Ej. SELECT... FROM Tab [AS] alias... QUERY: \n" + where);
			}
			w = w.substring(asIndex);
		}
		else if (w.startsWith(FROM)) {
			int asIndex = w.indexOf(AS);
			if (asIndex == -1) {
				throw new BaseExcepcion("Alias no encontrado, Ej. SELECT... FROM Tab [AS] alias... QUERY: \n" + where);
			}
			w = w.substring(asIndex);
		}
		
		return w;
	}
	
	private static StringBuffer secondCheckCount(String w, String alias){
		StringBuffer hql = new StringBuffer("");
		if (w.length() > 0) {
			if (!w.startsWith(AS) && !w.startsWith(JOIN) && !w.startsWith(WHERE)) {
				//hql += " " + AS + " " + alias + " WHERE";				
				hql.append(CARACTER_VACIO).append(AS).append(CARACTER_VACIO).append(alias).append(" WHERE");
			}
			//hql += " " + w;
			hql.append(w);
		}
		else {
			//hql += " " + AS + " " + alias;
			hql.append(CARACTER_VACIO).append(AS).append(CARACTER_VACIO).append(alias);
		}
		return hql;
	}
	
	/**
	 * Construye sentencia COUNT extends IEntidad
	 */
	public static String count(Class<?> entityClass, final String where, boolean distinct) {

		String w = StringUtil.trimToEmpty(where);
		w = firstCheckCount(w, where);

		String alias = alias(entityClass, w);

		String dx = ((distinct) ? DISTINCT : "") + " ";
		String pkName = pkName(entityClass);
		String cnt;
		if (pkName == null){
			cnt = "COUNT("+ dx + alias +")";			
		}else{
			cnt = "COUNT("+ dx + alias + "."+ pkName(entityClass) +")";	
		}
			
		StringBuffer hql = secondCheckCount(w, alias);	



		// Para hacer las cuentas FETCH no debe ir, sino no funciona
		//hql = hql.replace(FETCH, "");
		String hqlTemporal = hql.toString().replace(FETCH, "");
		//hql = hql.replace("fetch", "");
		hqlTemporal = hqlTemporal.replace("fetch", "");
		
		hql = new StringBuffer("");

		//hql = "SELECT " + cnt + " FROM " + entityClass.getSimpleName() + hql;
		hql.append("SELECT ").append(cnt).append(" FROM ").append(entityClass.getSimpleName()).append(CARACTER_VACIO).append(hqlTemporal);
		return hql.toString();
	}


}
