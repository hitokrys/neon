/**
 * Clase BaseServicioAsincronoImpl.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.service;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Named;

/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
@Stateless
@Asynchronous
@Named("asyncService")
public class BaseServicioAsincronoImpl implements BaseServicioAsincrono {
/*
 * 	@Inject
	private JMSContext jmsContext;
	
	@Inject Logger log;

	// @Resource(lookup="java:comp/testMailSession")
	private Session mailSession;

	@Override
	public void sendJMSMessage(Destination destination, Serializable... messages) {
		long delay = 1000;
		for (Serializable message : messages) {
			jmsContext.createProducer().setDeliveryDelay(delay).send(destination, message);
		}

	}

	@Override
	public void sendEmailMessage(String mailFrom, String mailTo, String subject, String content) {
		try {

			Message message = new MimeMessage(mailSession);
			message.setFrom(new InternetAddress(mailFrom));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailTo));
			message.setSubject(subject);
			message.setContent(content, "text/plain");
			Transport.send(message);

		} catch (MessagingException e) {
			throw new BaseExcepcion(e);
		}

	}


 * */
}
