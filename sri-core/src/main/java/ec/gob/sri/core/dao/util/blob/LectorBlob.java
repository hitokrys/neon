/**
 * Clase LectorBlob.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.dao.util.blob;

/**
 * Para leer un campo tipo Blob
 * @author mrobayo
 */
public interface LectorBlob {

	boolean leerBlob(Object objetoBlob);
	
}
