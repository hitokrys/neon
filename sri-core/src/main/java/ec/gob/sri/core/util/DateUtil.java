/**
 * Clase DateUtil.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Funciones para manejo de fechas
 * 
 * @author hrobayo Jun 15, 2010
 */
public final class DateUtil {

	public static final Locale ES_EC = new Locale("ES", "EC");
	public static final String DEFAULT_DATE_FMT = "dd/MM/yyyy";
	public static final String DEFAULT_TIME_FMT = "HH:mm";

	//private static final SimpleDateFormat H24_TIME = new SimpleDateFormat(DEFAULT_TIME_FMT, ES_EC);
	//private static final SimpleDateFormat DEFAULT2 = new SimpleDateFormat(DEFAULT_DATE_FMT, ES_EC);

	private static ThreadLocal<DateFormat> dateFormat24H = new ThreadLocal<DateFormat>() {

		@Override
		public DateFormat get() {
			return super.get();
		}

		@Override
		protected DateFormat initialValue() {
			return new SimpleDateFormat(DEFAULT_TIME_FMT, ES_EC);
		}

		@Override
		public void remove() {
			super.remove();
		}

		@Override
		public void set(DateFormat value) {
			super.set(value);
		}
	};

	
	private static ThreadLocal<DateFormat> dateFormatDefault = new ThreadLocal<DateFormat>() {

		@Override
		public DateFormat get() {
			return super.get();
		}

		@Override
		protected DateFormat initialValue() {
			return new SimpleDateFormat(DEFAULT_DATE_FMT, ES_EC);
		}

		@Override
		public void remove() {
			super.remove();
		}

		@Override
		public void set(DateFormat value) {
			super.set(value);
		}
	};

	private static final String MENSAJE_FECHAS_NULAS = "Las fechas deben ser validas";

	private DateUtil() {
	}

	/**
	 * Timestamp de auditoria
	 */
	public static Timestamp timestamp() {
		long time = Calendar.getInstance().getTimeInMillis();
		return new Timestamp(time);
	}

	/**
	 * Fecha+Hora de hoy dia
	 */
	public static Timestamp ahora() {
		Calendar t = Calendar.getInstance();
		t.set(Calendar.MILLISECOND, 0);

		return new Timestamp(t.getTimeInMillis());
	}

	/**
	 * Fecha de hoy dia
	 */
	public static Date hoy() {
		Calendar t = hoyCalendar();
		return new Timestamp(t.getTimeInMillis());
	}

	/**
	 * Fecha de hoy dia
	 */
	public static Calendar hoyCalendar() {
		Calendar t = Calendar.getInstance();
		t.set(Calendar.HOUR_OF_DAY, 0);
		t.set(Calendar.MINUTE, 0);
		t.set(Calendar.SECOND, 0);
		t.set(Calendar.MILLISECOND, 0);

		return t;
	}

	/**
	 * Parses date
	 */
	public static Date parseDate(String text) {
		try {
			return dateFormatDefault.get().parse(text);

		} catch (ParseException e) {
			throw new IllegalArgumentException("El texto: '" + text + "' no se puede convertir", e);
		}
	}

	/**
	 * Format Date/Time
	 */
	public static String format(Date fecha) {
		if (fecha == null) {
			return "";
		}

		if (isToday(fecha)) {
			return dateFormat24H.get().format(fecha);
		} else {
			return dateFormatDefault.get().format(fecha);
		}
	}

	/**
	 * Formatea un Calendar
	 * 
	 * @param fecha
	 * @return
	 */
	public static String format(Calendar fecha) {
		if (fecha == null) {
			return "";
		}
		return format(fecha.getTime());
	}

	/**
	 * Generar formato completo
	 * 
	 * @param fecha
	 * @return
	 */
	public static String formatoCompleto(Date fecha) {
		if (fecha == null) {
			return null;
		}
		return dateFormatDefault.get().format(fecha) + " " + dateFormat24H.get().format(fecha);
	}

	/**
	 * <p>
	 * Verifica si dos fechas son iguales, ignorando el tiempo.
	 * </p>
	 *
	 * @param date1
	 *            la primera fecha, sin alteraciones, no nula
	 * @param date2
	 *            la segunda fecha, sin alteraciones, no nula
	 * @return true si las dos fechas representan la misma fecha
	 * @throws IllegalArgumentException
	 *             si alguna de las fechas es <codigo>nula</codigo>
	 */
	public static boolean isSameDay(Date date1, Date date2) {
		if (date1 == null || date2 == null) {
			throw new IllegalArgumentException(MENSAJE_FECHAS_NULAS);
		}
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date1);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date2);
		return isSameDay(cal1, cal2);
	}

	/**
	 * <p>
	 * Verifica si dos calendar son iguales, ignorando el tiempo.
	 * </p>
	 *
	 * @param date1
	 *            la primera calendar, sin alteraciones, no nula
	 * @param date2
	 *            la segunda calendar, sin alteraciones, no nula
	 * @return true si las dos fechas representan la misma fecha
	 * @throws IllegalArgumentException
	 *             si alguno de las calendar es <codigo>nula</codigo>
	 */
	public static boolean isSameDay(Calendar cal1, Calendar cal2) {
		if (cal1 == null || cal2 == null) {
			throw new IllegalArgumentException(MENSAJE_FECHAS_NULAS);
		}
		return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
				&& cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
	}

	/**
	 * <p>
	 * Verifica si una fecha es hoy.
	 * </p>
	 *
	 * @param date
	 *            la fecha, no alterada, no nula
	 * @return true si la fecha es hoy.
	 * @throws IllegalArgumentException
	 *             si al fecha es <codigo>null</codigo>
	 */
	public static boolean isToday(Date date) {
		return isSameDay(date, Calendar.getInstance().getTime());
	}

	/**
	 * <p>
	 * Verifica si un calendar es hoy.
	 * </p>
	 *
	 * @param date
	 *            el calendar, no alterado, no nulo
	 * @return true si calendario es hoy.
	 * @throws IllegalArgumentException
	 *             si el calendar es <codigo>null</codigo>
	 */
	public static boolean isToday(Calendar cal) {
		return isSameDay(cal, Calendar.getInstance());
	}

	/**
	 * Verifica si las fechas no estan en el futuro
	 * 
	 * @param values
	 * @return
	 */
	public static boolean isInFuture(Date... values) {
		Date ahora = new Date();
		for (Date date : values) {
			if (date != null && !(DateUtil.isSameDay(date, ahora) || date.compareTo(ahora) <= 0)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Convierte la fecha de Tipo Date a Calendar
	 * 
	 * @param fechaKardex
	 * @return
	 */
	public static Calendar toCalendar(Date fecha) {
		Calendar c = Calendar.getInstance();
		if (fecha == null) {

			c.set(Calendar.YEAR, 1900);
			c.set(Calendar.MONTH, 0);
			c.set(Calendar.DAY_OF_MONTH, 0);
		} else {
			c.setTime(fecha);
		}
		return c;
	}

	/**
	 * Retorna un calendar con la fecha establecida
	 * 
	 * @return Si fecha es NULL => retorna NULL
	 */
	public static Calendar toCal(Date time) {
		Calendar calendar = null;
		if (time != null) {
			calendar = Calendar.getInstance();
			calendar.setTime(time);
		}
		return calendar;
	}

	/**
	 * Ultimo dia del mes
	 */
	public static int ultiomoDiaDelMes(Calendar date) {
		return date.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	/**
	 * Hace dias
	 *
	 * @param dias
	 * @return
	 */
	public static Date getHaceDias(int dias) {
		Calendar haceDias = hoyCalendar();

		haceDias.add(Calendar.DATE, -dias);
		return haceDias.getTime();
	}

	/**
	 * Crea una fecha con los parametros enviados
	 *
	 * @param anio
	 * @param mes
	 * @param dia
	 * @return date
	 */
	public static Date toDate(int anio, int mes, int dia) {
		Calendar c = hoyCalendar();
		c.set(Calendar.YEAR, anio);
		c.set(Calendar.MONTH, mes);
		c.set(Calendar.DATE, dia);
		return c.getTime();
	}

	/**
	 * convierte correctamente un calendar a una fecha
	 */
	public static Date toDate(XMLGregorianCalendar fecha) {
		if (fecha == null) {
			return null;
		}

		Calendar d = fecha.toGregorianCalendar();
		return d.getTime();
	}

}