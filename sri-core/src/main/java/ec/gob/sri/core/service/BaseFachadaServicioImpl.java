/**
 * Clase BaseFachadaServicioImpl.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.core.service;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
@Stateless
@Named("servicioBase")
public class BaseFachadaServicioImpl extends BaseFachadaServicioImplAbstracta implements BaseFachadaServicio{
		
	@Inject EntityManager enm;

}
