package com.test.properties.ejb;


import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

@Startup
@Singleton
public class PruebaConfiguracionesBean {
	
	private static final Logger log= Logger.getLogger(PruebaConfiguracionesBean.class.getName());

	@PostConstruct
	private void init() {
		System.out.println("@PostConstruct");
		log.info("Hola archivo!!");
		Properties properties = new Properties();
		InputStream is = this.getClass().getClassLoader().getResourceAsStream("pruebas.properties"); 

		try {
            //properties.load(new FileInputStream(file));
            properties.load(is);
        } catch (IOException e) {
            System.out.println("Unable to load properties file" + e);
        }

		 Map<String, String> propertiesMap = new HashMap<>();
		 HashMap hashMap = new HashMap<>(properties);
		 propertiesMap.putAll(hashMap);
		 
		 System.out.println("***********************************************************************************");
		 for (Map.Entry<String,String> entry:propertiesMap.entrySet()){
			 System.out.println("key, " + entry.getKey() + " value " + entry.getValue());
			 log.info("key, " + entry.getKey() + " value " + entry.getValue());
			 log.log(Level.SEVERE, "Error");
		 }
		 
		 

	}
}
