/**
 * Clase SaludarSoapTest.java 10-11-2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.base.interfaz.soap;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import ec.gob.sri.base.logica.SaludoBean;

/**
 * @author jada270709
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SaludarSoapTest {
	
	@Mock
	private SaludoBean saludoBean;
	
	 @InjectMocks
	 private SaludarSoap saludarSoap;
	
	public SaludarSoapTest() {
	}
	
	@Test
	public void pruebaSaludar() {
		when(saludoBean.saludar()).thenReturn("Hola desde la logica");    	
        String expResult = "Hola desde la logica";
        String result = saludarSoap.saludar();
        assertEquals(expResult, result);
	}
}
