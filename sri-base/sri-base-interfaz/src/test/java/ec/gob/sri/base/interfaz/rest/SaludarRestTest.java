/**
 * Clase SaludarRestTest.java 10-11-2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.base.interfaz.rest;

import static org.junit.Assert.*;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import ec.gob.sri.base.cliente.SaludarCliente;
import ec.gob.sri.base.logica.SaludoBean;

import static org.mockito.Mockito.when;

/**
 * @author jada270709
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SaludarRestTest {
	 	@Mock
	    private SaludoBean saludoBean;
	    
	    @Mock
	    private SaludarCliente saludarCliente;
	    
	    @Mock
	    Logger log;
	    
	    @InjectMocks
	    private SaludarRest saludarRest;
	    
	    public SaludarRestTest() {
	    }
	    
	    
	    /**
	     * Test of saludar method, of class SaludarRest.
	     */
	    @Test
	    public void testSaludar() {
	       	       
	    	when(saludoBean.saludar()).thenReturn("Hola desde la logica");
	    	
	        String expResult = "Hola desde la logica";
	        String result = saludarRest.saludar();
	        assertEquals(expResult, result);
	        
	    }

	    /**
	     * Test of consumeSaludar method, of class SaludarRest.
	     */
	    @Test
	    public void testConsumeSaludar() {
	       	
	    	when(saludarCliente.consumeSaludar()).thenReturn("Hola desde la logica por el cliente");
	    	
	        String expResult = "Hola desde la logica por el cliente";
	        String result = saludarRest.consumeSaludar();
	        assertEquals(expResult, result);
	       
	    }
	    
	
}
