/**
 * Clase SaludarSoap.java 05-10-2015 * 
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.base.interfaz.soap;

import ec.gob.sri.base.logica.SaludoBean;
import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author jada270709
 */
@WebService(serviceName = "SaludarSoap")
public class SaludarSoap {
    @EJB
    private SaludoBean saludoBean;

    @WebMethod(operationName = "saludar")
    public String saludar() {
        return saludoBean.saludar();
    }
    
}
