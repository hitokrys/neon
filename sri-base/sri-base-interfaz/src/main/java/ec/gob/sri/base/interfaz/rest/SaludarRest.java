/**
 * Clase SaludarRest.java 05-10-2015 * 
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.base.interfaz.rest;

import javax.ejb.EJB;
import javax.inject.Inject;
//import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;


import ec.gob.sri.base.cliente.SaludarCliente;
import ec.gob.sri.base.logica.SaludoBean;

/**
 *
 * @author jada270709
 */
@Path("/Base")
public class SaludarRest {

    @EJB
    private SaludoBean saludoBean;
    
    @EJB
    private SaludarCliente saludarCliente;
    
    @Inject Logger log;
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/Saludar")
    public String saludar() {
    	log.info("Ejemplo de uso de Log");
        return saludoBean.saludar();
    }
    @GET
    @Produces({"application/json"})
    @Path("/consumeSaludar")
    public String consumeSaludar() {
    	
        return saludarCliente.consumeSaludar();
        
    }
    
    @GET
    @Produces({"application/json"})
    @Path("/consumeSaludarAutenticacion")
    public String consumeSaludarAutenticacion() {
    	
        return saludarCliente.consumeSaludarAutenticacion();
        
    }
}
