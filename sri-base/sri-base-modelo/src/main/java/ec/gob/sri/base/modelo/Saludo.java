/**
 * Clase Saludo.java 05-10-2015 * 
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.base.modelo;

/**
 * @author jada270709
 *
 */
public class Saludo {

	private String valor;

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
}
