/**
 * Clase SaludarCliente.java 05-10-2015  
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.base.cliente;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.util.Base64; 
import org.slf4j.Logger;

/**
 *
 * @author jada270709
 */

@Stateless
public class SaludarCliente {

	private static final String URL_BASE = "http://localhost:8080/sri-base-interfaz/rest";
	private static final String URL_SERVICIO_SALUDAR = "/Base/Saludar";
	private static final String MENSAJE_ERROR_AUTENTICACION = "Fallo la autenticacion";
	@Inject Logger log;
	@Resource
	private EJBContext context;
	
	
	public String consumeSaludar() {
		String username = "jadiaz";
	    String password = "jada270709";
	 
	    String usernameAndPassword = username + ":" + password;
		String authorizationHeaderName = "Authorization";
        String authorizationHeaderValue = "Basic " + Base64.getEncoder().encodeToString(usernameAndPassword.getBytes() );
               
		Client client = ClientBuilder.newClient();
		log.info(URL_BASE + "/Base/Saludar");
		try {
		String name = client.target(URL_BASE + URL_SERVICIO_SALUDAR)				
				.request(MediaType.APPLICATION_JSON)
				.header(authorizationHeaderName, authorizationHeaderValue)
				.get(String.class)
				;
		return name + " por el cliente";
		} catch (NotAuthorizedException  e) {
			log.error(MENSAJE_ERROR_AUTENTICACION);
			return MENSAJE_ERROR_AUTENTICACION;	
		}		
	}
	public String consumeSaludarAutenticacion() {
		String username = context.getCallerPrincipal().getName();
	    String password = "jada270709";
	    
	    
	    String usernameAndPassword = username + ":" + password;
		String authorizationHeaderName = "Authorization";
        String authorizationHeaderValue = "Basic " + Base64.getEncoder().encodeToString(usernameAndPassword.getBytes() );
               
		Client client = ClientBuilder.newClient();
		log.info(URL_BASE + URL_SERVICIO_SALUDAR);
		try {
		String name = client.target(URL_BASE + URL_SERVICIO_SALUDAR)				
				.request(MediaType.APPLICATION_JSON)
				.header(authorizationHeaderName, authorizationHeaderValue)
				.get(String.class)
				;
		return name + " por el cliente";
		} catch (NotAuthorizedException  e) {
			log.error(MENSAJE_ERROR_AUTENTICACION);
			return MENSAJE_ERROR_AUTENTICACION;	
		}		
	}
}
