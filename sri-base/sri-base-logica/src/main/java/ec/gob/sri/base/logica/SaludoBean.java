/**
 * Clase SaludoBean.java 05-10-2015  
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.base.logica;

import javax.ejb.Stateless;

import ec.gob.sri.base.modelo.Saludo;

/**
 *
 * @author jada270709
 */
@Stateless
public class SaludoBean  {

    public String saludar() {
    	
    	Saludo saludo = new Saludo();
    	saludo.setValor("Hola desde la logica");
        return saludo.getValor();
    }
    
    public String saludo2(String a) {
    	if ("1".equals(a)) {
    		return "1";
    	}
    	return "hola";
    	
    }

}
