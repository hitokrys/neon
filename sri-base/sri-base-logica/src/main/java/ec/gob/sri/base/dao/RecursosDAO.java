/**
 * Clase SaludoDAO.java 05-10-2015 * 
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.base.dao;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;





/**
 * @author jada270709
 *
 */
public class RecursosDAO {

	/*
	 * 
	 * */
	
	@PersistenceContext(unitName = "DisenoPU")
	public EntityManager entityManagerDefault;
	
	@Produces
	public EntityManager getEm(InjectionPoint ip) {             	    
	    return entityManagerDefault; 
	}	

}
