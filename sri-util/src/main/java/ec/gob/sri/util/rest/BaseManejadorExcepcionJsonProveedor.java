/**
 * Clase BaseManejadorExcepcionJsonProveedor.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util.rest;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;

import ec.gob.sri.util.excepcion.BaseExcepcion;

/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
@Provider
public class BaseManejadorExcepcionJsonProveedor implements ExceptionMapper<BaseExcepcion>
{
	
	@Inject Logger log;
	
    @Override
    public Response toResponse(BaseExcepcion exception)
    {
    	exception.log();
    	return Response.status(Status.INTERNAL_SERVER_ERROR).entity(exception).build();
    }
}