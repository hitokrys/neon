/**
 * Clase TablaDatosFiltro.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util.rest.tabladatos;



/**
 * @author Hugo Camilo Robayo Ayala
 *
 */
public enum TablaDatosFiltro {
	FILTER_DEFAULT, NO_FILTER;
}
