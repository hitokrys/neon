/**
 * Clase ExcepcionesInterceptor.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util.rest.interceptor;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;

import ec.gob.sri.util.Constantes;
import ec.gob.sri.util.excepcion.BaseExcepcion;

/**
 * @author
 *
 */
@Interceptor
@TramaRest
public class TramaRestInterceptor {

	@Inject
	Logger log;
	private TramaRestBase response;

	/**
	 * 
	 */
	public TramaRestInterceptor() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ic
	 * @return
	 * @throws Exception
	 */
	@AroundInvoke
	public Object detectFailure(InvocationContext ic) throws BaseExcepcion {
		Object retornoTramaDatos = null;
		response = new TramaRestBase();
		Cuerpo cuerpo = new Cuerpo();
		Mensaje mensaje = new Mensaje();
		Cabecera cabecera = new Cabecera();
		response.setCabecera(cabecera);

		log.info("DETECT FAILURE START");

		try {
			verificaParametros(ic);
			retornoTramaDatos = ic.proceed();

			mensaje.setCodigo(Constantes.CODIGO_MENSAJE_RESPUESTA_REST_OK);
			mensaje.setDescripcion(Constantes.MENSAJE_RESPUESTA_REST_OK);

			cuerpo.setMensaje(mensaje);
			response.setCuerpo(cuerpo);
			cuerpo.setDatos(retornoTramaDatos);

		} catch (BaseExcepcion be) {

			mensaje.setCodigo(be.getCodigo());
			mensaje.setDescripcion(be.getError());
			cuerpo.setMensaje(mensaje);
			response.setCuerpo(cuerpo);

			// log.info("Token: " + response.getCabecera().getToken() + " -
			// Captura de excepcion Base en interceptor "
			// + response.getCuerpo().getMensaje().getDescripcion());

		} catch (IllegalArgumentException | IllegalStateException | EJBException excepcion) {

			mensaje.setCodigo(String.valueOf(Status.CONFLICT.getStatusCode()));
			mensaje.setDescripcion(excepcion.getMessage());
			cuerpo.setMensaje(mensaje);
			response.setCuerpo(cuerpo);

			// log.info("Token: " + response.getCabecera().getToken() + " -
			// Captura de excepcion en interceptor "
			// + response.getCuerpo().getMensaje().getDescripcion());

		} catch (Exception excepcion) {

			mensaje.setCodigo(String.valueOf(Status.INTERNAL_SERVER_ERROR.getStatusCode()));
			mensaje.setDescripcion(excepcion.getMessage());
			response.setCuerpo(cuerpo);

			// log.info("Token: " + response.getCabecera().getToken() + " -
			// Captura de excepcion interna en interceptor "
			// + response.getCuerpo().getMensaje().getDescripcion());

		} finally {
			log.info("DETECT FAILURE FINISH");
		}

		return response;
	}

	private void verificaParametros(InvocationContext context) {
		BaseExcepcion baseExcepcion;

		for (Object objeto : context.getParameters()) {

			if (objeto == null || objeto.equals("")) {
				baseExcepcion = retornaNuevaexcepcion();
				baseExcepcion.setCodigo(String.valueOf(Status.BAD_REQUEST.getStatusCode()));
				baseExcepcion.setError(Status.BAD_REQUEST.name());
				throw baseExcepcion;
			} else {
				TramaRestBase trama = (TramaRestBase) objeto;
				response.setCabecera(trama.getCabecera());
			}

		}
	}

	private BaseExcepcion retornaNuevaexcepcion() {
		return new BaseExcepcion();
	}

}
