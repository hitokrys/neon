/**
 * Clase Constantes.java 20-07-2016
 * Copyright 2016 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util;

/**
 * @author masc010815
 *
 */
public class Constantes {
	public static final String ERROR_INTERNAMENTE_SERVICIO = "SRI.CORE.ERROR_INTERNAMENTE_SERVICIO";
	public static final String CODIGO_MENSAJE_RESPUESTA_REST_OK = "200";
	public static final String MENSAJE_RESPUESTA_REST_OK = "OK";
}
