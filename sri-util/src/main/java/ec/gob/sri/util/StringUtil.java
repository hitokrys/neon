/**
 * Clase StringUtil.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util;

import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

/**
* Funciones para manejar cadenas de texto
* @author hrobayo Jun 15, 2010
*/
public final class StringUtil {

	
	
	public static final String CHAR_SEPARATOR = ",";
	public static final String CODE_SEPARATOR = "-";

	public static final Pattern CSV_REGEX_PATTERN = Pattern.compile("(\\d+)(,\\s*\\d+)*");

	private StringUtil(){}
	
	/**
	 * Cuts a text
	 */
	public static String abrev(final String text, final int i) {
		String value = trimToEmpty(text);
		if (value.length() > i) {value = value.substring(0, i - 3) + "...";}
		return value;
	}

	/**
	 * Parecido a this.cut()
	 */
	public static String cut(final Object text, final int limit) {
		if (text == null) {
			return null;
		}
		else {
			String head = text.toString();
			if (head.length() > limit) {
				head = head.substring(0, limit);
			}
			return head;
		}
	}

	/**
	 * Cuts a text
	 */
	public static String cut(final String text, final String tail) {
		String value = trimToEmpty(text);
		if (value.lastIndexOf(tail) >= 0) {
			value = value.substring(0, value.lastIndexOf(tail));
		}
		return value;
	}

	public static boolean isNotEmpty(String... values) {
		for (String value : values) {
			if (isEmpty(value)){ return false;}
		}
		return true;
	}

//	public static boolean isNotEmpty(String value) {
//		return (value != null && value.length()>0);
//	}

	public static boolean isEmpty(String value) {
		return (value == null || value.length()==0);
	}

	/**
	 * Trim to Empty
	 */
	public static String trimToEmpty(Object value) {
		String text = "";
		if (value != null){ text = value.toString();}
		return text.trim();
	}

	/**
	 * Quita los espacios duplicados y todo caracter al inicio que no sea A-Z 0-9
	 * http://java.sun.com/j2se/1.4.2/docs/api/java/util/regex/Pattern.html
	 *
	 * ^[\\W]* quita cualquier caracter no alpha numerico del inicio (\\s)+ quita espacios
	 * duplicados ^[\\s\\p{Punct}]* quita signos de puntuacion y espacios al inicio
	 *
	 * @return
	 */
	public static String trimToName(String text) {
		if (text == null){
			return null;
		}
		// Quita espacios de sobrantes (dobles espacios)
		return text.replaceAll("^[\\s\\p{Punct}]*", "").replaceAll("(\\s)+", " ").trim();
	}

	/**
	 * Lo mismo pero UPPER CASE
	 */
	public static String trimToNAME(String text) {
		if (text == null){return null;}

		//MAYUSCULAS
		return trimToName(text).toUpperCase();
	}

	/**
	 * Length
	 */
	public static int length(String value) {
		String text = trimToEmpty(value);
		return text.length();
	}

	/**
	 * Trunc to Length
	 */
	public static String trunc(final String value, final int length) {
		String text = trimToEmpty(value);
		if (length(text) > length) {
			if (length > 100) {
				text = text.substring(0, length-3) + "...";
			}
			else {
				text = text.substring(0, length);
			}

		}
		return text.trim();
	}

	/**
	 * Date To String
	 */
//	public static String toStringDate(Date value) {
//		if (value != null){
//			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//			return dateFormat.format(value);
//		}
//		return "";
//	}

	/**
	 * Parse long
	 */
	public static Long parseLong(String s) {
		try {
			return Long.parseLong(s);
		} catch(NumberFormatException e) { return null; }
	}

	/**
	 * Parse double
	 */
	public static Double parseDouble(String number, double... defaultValue) {
		try {
			return Double.parseDouble(number);
		} catch(NumberFormatException e) {
			if (defaultValue.length > 0){return defaultValue[0];
			} else{return null;}
		}
	}

	public static Integer parseInt(String number) {
		try {
			return Integer.parseInt(number);
		} catch(NumberFormatException e) { return null; }
	}

	/**
	 * DateQuery ?
	 *
	 * @param col
	 * @param query
	 */
	public static String dateQuery(String col, String query) {

		Date di = null, df = null;
		String si = null, sf = null;
		int between = query.indexOf('-');

		if (between > 0) {
			si = query.substring(0, between).trim();
			sf = query.substring(between+1).trim();
		}
		else {
			si = query.trim();
		}

		di = FechaUtil.parseDate(si);
		df = FechaUtil.parseDate(sf);

		// Retorna en caso de no existir fecha
		if (di == null && df == null) {return null;}

		if (di == null) {
			Calendar c = Calendar.getInstance();
			c.set(Calendar.DATE, 1);
			c.set(Calendar.MONTH, 1);
			c.set(Calendar.YEAR, 1999);
			di = c.getTime();
		}
		else if (df == null) {
			Calendar c = Calendar.getInstance();
			c.setTime(di);
			c.add(Calendar.DATE, 1);

			df = c.getTime();
		}

		if (df == null) {
			Calendar c = Calendar.getInstance();
			c.set(Calendar.MONTH, 12);
			c.set(Calendar.DATE, 31);
			c.set(Calendar.YEAR, 2051);

			df = c.getTime();
		}

		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

		return col + " BETWEEN '" + fmt.format(di) + "' AND '" + fmt.format(df) + "' ";
	}

	public static boolean likeDates(String query) {
		return dateQuery("", query) != null;
	}

	

	/**
	 * Quita caracter subrayado
	 */
	public static String quitaSubrayado(Object text) {
		String result = null;

		if (text != null) {
			result = text.toString().replace("_", " ");
		}

		return result;
	}

	/**
	 * Capitalizar primera palabra
	 */
	public static String capitalize(String string) {
		return StringUtils.capitalize(string);
	}


	/**
	 * Trnasforma a una lista separada por comas
	 * @param values
	 * @return
	 */
	public static String getComaSeparated(List<Long> list){

		if (list != null && !list.isEmpty()) {
			StringBuffer ids = new StringBuffer();
			boolean first = true;
			for (Long id : list) {
				ids.append((first) ? id : ",").append(id);
				first = false;
			}
			return ids.toString();
		}
		throw new IllegalArgumentException("No ha enviado el codigo");
	}

	/**
	 * Separa un texto usando el caracter: |
	 * @param text
	 * @return
	 */
	public static Long[] splitToLong(String text) {
		int len = 0;
		String[] textList = null;

		if (isNotEmpty(text)) {
			textList = split(text);
			len = textList.length;
		}
		Long[] numberList = new Long[len];
		try {
			for(int i=0; i<len; i++) {
				numberList[i] = Long.parseLong(textList[i]);
			}
		} catch (NumberFormatException e) {
			numberList = new Long[0];
		}

		return numberList;
	}

	/**
	 * Split text utilizando un separador
	 */
	public static String[] split(String text, String... separator) {
		if (text == null){
			return new String[0];
		} else {
			String sep = CHAR_SEPARATOR;
			if (separator.length > 0){ sep = separator[0];}

			return text.split(sep);
		}
	}

	/**
	 * Split quitando las cadenas vacias
	 */
	public static String[] split2(String text, String... separator) {
		if (text == null){
			return new String[0];
		}else {
			String sep = CHAR_SEPARATOR;
			if (separator.length > 0){ sep = separator[0];}

			int count = 0;
			String[] items = text.split(sep);
			for (String item : items) {
				count += (StringUtil.isEmpty(item)) ? 0: 1;
			}

			String[] result = new String[count];
			int i = 0;
			for (String item : items) {
				if (StringUtil.isNotEmpty(item)){ result[i++] = item;}
			}

			return result;
		}

	}

	/**
	 * Devuelve el valor no nulo
	 * @param notempty
	 * @param other
	 * @return
	 */
	public static String nvl(String notempty, String other) {
		if (isNotEmpty(notempty)) {return notempty;}else{ return other;}
	}

	/**
	 * Verifiva el patron (\\d+)(,\\s*\\d+)* 
	 * @param ids
	 */
	public static String checkCSV(String ids) throws IllegalArgumentException{

		Matcher matcher = CSV_REGEX_PATTERN.matcher(ids);
		boolean found = false;

		while (matcher.find()) {
			found = true;
		}
		if (!found){
			throw new IllegalArgumentException("Los argumentos no son validos.");
		}
		return ids;
	}
	
	
	/**
	 * Use Normalizer to Remove Accents Method to convert a given string with no
	 * accent. This can be useful in a database field to simplify name searching
	 * with accent or not.
	 *
	 * @param textWithDiacritics
	 * @return
	 */
	public static String removeAccents(String textWithDiacritics) {
		if (textWithDiacritics == null){
			return "";
		}

		//Quita espacios de sobrantes (dobles espacios)
		String text = textWithDiacritics.replaceAll("(\\s)+", " ");

		text = Normalizer.normalize(
			text.trim(), Normalizer.Form.NFD);

		return text.replaceAll("[^\\p{ASCII}]", "");
	}	

}
