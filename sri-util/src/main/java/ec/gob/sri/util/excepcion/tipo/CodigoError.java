/**
 * Clase CodigoError.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util.excepcion.tipo;

import ec.gob.sri.util.enumeradores.EnumeracionTitulada;

/**
 * @author Hugo Camilo Robayo Ayala
 * TODO
 * Definir si los codigos de error deben ser programados, o un tipo de dato o una mezcla
 * Si es un tipo de dato como se lo hace portable????
 */
public enum CodigoError implements EnumeracionTitulada{

	
	SRI_APP_CODE_BASE ("SRI.CORE.ERROR_BASE");
	
	
	private final String value;

	
	private CodigoError(final String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}	
	
	
	@Override
	public String toString() {
		if (value == null){
			getValue();
		}
		return name();
	}

	@Override
	public String getName() {
		return name();
	}
	
}
