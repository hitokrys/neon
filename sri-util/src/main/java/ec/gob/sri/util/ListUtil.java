/**
 * Clase ListUtil.java 20-07-2016
 * Copyright 2016 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author hrobayo Jun 15, 2010
 */
public class ListUtil {

	private ListUtil() {

	}

	/**
	 * Valida si un item esta en un array
	 */
	public static <T> boolean isInArray(T[] array, T value) {
		if (array == null) {
			return false;
		}
		for (T item : array) {
			if (item != null && item.equals(value)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Valida si un item esta una Lista
	 * 
	 * @param list
	 * @param item
	 * @return
	 */
	public static <T> boolean isInList(List<T> list, T value) {
		if (list == null) {
			return false;
		}
		for (T item : list) {
			if (item != null && item.equals(value)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Valida si un item esta en un array
	 */
	public static <T> boolean isInList(String list, T value) {
		if (list == null) {
			return false;
		}
		return isInArray(splitToLong(list), value);
	}

	/**
	 * Une la lista con un separador
	 * 
	 * @param items
	 * @return
	 */
	public static String join(final List<Long> items) {
		return join(items, StringUtil.CHAR_SEPARATOR);
	}

	/**
	 * Unir listado
	 */
	public static String join(final List<Long> items, String separator) {

		StringBuffer joined = new StringBuffer();

		for (Serializable s : items) {
			if (joined.length() > 0) {
				joined.append(separator);
			}
			if (s != null) {
				joined.append(s);
			}
		}
		return joined.toString();
	}

	/**
	 * Unir listado
	 */
	public static String join(final String... param) {
		return join(param, StringUtil.CHAR_SEPARATOR);
	}

	/**
	 * Une utilizando el caracter: separatorChar
	 */
	@SuppressWarnings("unchecked")
	public static <T> String join(T... param) {
		return join(param, StringUtil.CHAR_SEPARATOR);
	}

	/**
	 * Funciones utiles
	 */
	public static <T> String join(T[] array, String separator) {
		StringBuffer joined = new StringBuffer();
		if (array != null) {
			for (T s : array) {
				if (joined.length() > 0) {
					joined.append(separator);
				}

				if (s instanceof Object[]) {
					String arr = join((Object[]) s);
					joined.append('[').append(arr).append(']');
				} else if (s != null) {
					joined.append(s);
				}
			}
		}
		return joined.toString();
	}

	public static <T> String joinAll(T[] param, String separator) {
		StringBuffer text = new StringBuffer();
		boolean b = false;
		for (Object p : param) {
			String v = "";
			if (p != null) {
				v = p.toString().replaceAll(separator, "");
			}
			text.append((b) ? separator : "");
			text.append(v);
			b = true;
		}
		return text.toString();
	}

	/**
	 * Splits
	 * 
	 * @param text
	 */
	public static String[] split(String text) {
		if (text == null) {
			return new String[0];
		} else {
			return text.split(StringUtil.CHAR_SEPARATOR);
		}
	}

	public static class KeyValue {
		public final String key, value;

		public KeyValue(String keyValue) {
			String[] items = StringUtil.split(keyValue, "=");

			if (items.length == 0) {
				throw new IllegalArgumentException("key es requerido");
			}

			key = StringUtil.trimToEmpty(items[0]);
			if (StringUtil.isEmpty(key)) {
				throw new IllegalArgumentException("key es requerido");
			}
			if (items.length > 1) {
				value = StringUtil.trimToEmpty(items[1]);
			} else {
				value = "";
			}
		}
	}

	/**
	 * devuelve un KeyValue
	 */
	public static KeyValue keyValue(String item) {
		return new KeyValue(item);
	}

	/**
	 * Separa un texto usando el caracter: |
	 * 
	 * @param text
	 * @return
	 */
	public static Long[] splitToLong(String text) {
		int len = 0;
		String[] textList = null;

		if (StringUtil.isNotEmpty(text)) {
			textList = split(text);
			len = textList.length;
		}
		Long[] numberList = new Long[len];
		try {
			for (int i = 0; i < len; i++) {
				numberList[i] = Long.parseLong(textList[i]);
			}
		} catch (NumberFormatException e) {
			numberList = new Long[0];
		}

		return numberList;
	}

	/**
	 * @param params
	 * @param items
	 */
	public static void addAll(List<Object> list, Object... items) {
		for (Object item : items) {
			list.add(item);
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> List<T> addToList(List<T> list, T... items) {
		for (T item : items) {
			list.add(item);
		}
		return list;
	}

	/**
	 * @param item
	 * @param length
	 * @return
	 */
	public static String repeat(String item, int size, String... separator) {
		StringBuffer text = new StringBuffer();
		for (int i = 0; i < size; i++) {
			if (separator.length > 0) {
				text.append((text.length() > 0) ? separator[0] : "");
			}
			text.append(item);
		}
		return text.toString();
	}

	/**
	 * Devuelve el primero de la lista o nullo si esta vaica
	 * 
	 * @param <D>
	 * @param list
	 * @return
	 */
	public static <D> D getFirst(final List<D> list) {
		if (list != null && !list.isEmpty()) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * Crea una cadena separada con comas de los objetos de la lista
	 * 
	 * @param <D>
	 * @param list
	 * @return
	 */
	public static <D> String createCSVString(final Collection<D> list) {
		if (list != null && !list.isEmpty()) {
			int count = 1, total = list.size();
			StringBuffer sb = new StringBuffer();
			for (D d : list) {
				if (d == null) {
					sb.append("");
				} else {
					sb.append(d.toString());
				}

				if (count < total) {
					sb.append(',');
				}
				count++;
			}
			return sb.toString();
		}
		return "";
	}

	/**
	 * Crea una cadena separada con comas de los objetos de la lista y reemplaza
	 * el valor del objeto con otro caracter
	 * 
	 * @param <D>
	 * @param list
	 * @param replace
	 * @return
	 */
	public static <D> String createCSVString(final Collection<D> list, String replace) {
		if (list != null && !list.isEmpty()) {
			int count = 1, total = list.size();
			StringBuffer sb = new StringBuffer();
			for (D d : list) {
				if (d == null) {
					sb.append("");
				} else {
					sb.append(replace);
				}

				if (count < total) {
					sb.append(',');
				}
				count++;
			}
			return sb.toString();
		}
		return "";
	}

	/**
	 * Crea una cadena separada con comas de los objetos encerrados entre
	 * comillas simples de la lista
	 * 
	 * @param <D>
	 * @param list
	 * @return
	 */
	public static <D> String createQuotedCSVString(final Collection<D> list) {
		if (list != null && !list.isEmpty()) {
			int count = 1, total = list.size();
			StringBuffer sb = new StringBuffer("'");
			for (D d : list) {
				sb.append(d.toString());
				if (count < total) {
					sb.append(',');
				}
				count++;
			}
			sb.append('\'');
			return sb.toString();
		}
		return "";
	}

	public static <D> String[] createStringArray(Collection<D> list) {
		if (list != null && !list.isEmpty()) {
			int count = 0;
			String[] sb = new String[list.size()];
			for (D d : list) {
				sb[count++] = d.toString();
			}
			return sb;
		}
		return new String[] {};
	}

	/**
	 * @param findAll
	 * @return
	 */
	public static <D> Set<D> toSet(Collection<D> list) {
		Set<D> set = new HashSet<D>();
		for (D item : list) {
			set.add(item);
		}
		return set;
	}

	/**
	 * Hace un string
	 * 
	 * @param values
	 * @return
	 */
	public static String toString(Object... values) {
		if (values == null) {
			return null;
		}

		StringBuffer t = new StringBuffer();

		for (Object val : values) {
			String str = "";
			if (val instanceof Date) {
				str = FechaUtil.formatoCompleto((Date) val);
			}
			if (val instanceof String) {
				str = (String) val;
			} else if (val instanceof Number) {
				str = val.toString();
			} else {
				str = (String) val;
			}
			if (t.length() > 0) {
				t.append(", ");
			} else {
				t.append("");
			}
			t.append(str);
		}

		return "[" + t + "]";
	}

	/**
	 * Convierte un arreglo en una Lista
	 * 
	 * @param items
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<T> toList(T... items) {
		List<T> list = new ArrayList<T>();
		for (T item : items) {
			list.add(item);
		}
		return list;
	}

	/**
	 * Verifica que una lista no este vacia
	 * 
	 * @param items
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static boolean isNotEmpty(List items) {
		return (items != null && !items.isEmpty());
	}

}
