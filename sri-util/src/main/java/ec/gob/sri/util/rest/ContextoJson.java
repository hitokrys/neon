/**
 * Clase MapperContextResolver.java 31-05-2016
 * Copyright 2016 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util.rest;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.plugins.providers.jackson.ResteasyJackson2Provider;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.PropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

import ec.gob.sri.util.FechaUtil;
import ec.gob.sri.util.ListUtil;
import ec.gob.sri.util.rest.modulos.FechaModulo;
import ec.gob.sri.util.rest.tabladatos.TablaDatos;

/**
 * @author masc010815
 */
@Provider
@Produces("application/json")
@Consumes("application/json")
public class ContextoJson extends ResteasyJackson2Provider implements ContextResolver<ObjectMapper> {

	private final ObjectMapper mapper;
	private SimpleFilterProvider filters;

	public ContextoJson() {
		mapper = new ObjectMapper();
		mapper.registerModule(new Hibernate5Module());
		mapper.setDateFormat(new SimpleDateFormat(FechaUtil.PATRON_FECHA));
		mapper.registerModule(new FechaModulo());
	}

	@Override
	public ObjectMapper getContext(Class<?> cls) {
		return mapper;
	}

	@Override
	public void writeTo(Object value, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, Object> headers, OutputStream body) throws IOException {

		filters = new SimpleFilterProvider();
		for (Annotation annotation : annotations) {
			if (annotation.annotationType().equals(TablaDatos.class)) {
				PropertyFilter propertyFilter = addDataTableFilter((TablaDatos) annotation);
				filters.addFilter(TablaDatos.FILTER_NAME, propertyFilter);
			}
		}

		// Ignorar el filtrado de los campos miembros de referencia
		filters.setFailOnUnknownId(false);
		mapper.setFilters(filters);

		super.writeTo(value, type, genericType, annotations, mediaType, headers, body);

	}

	/**
	 * Método que retorna el filtro de propiedades
	 * 
	 * @param tablaDatos
	 * @return
	 */
	private PropertyFilter addDataTableFilter(TablaDatos tablaDatos) {
		String propiedades = tablaDatos.propiedades();
		Set<String> setPropiedades = new HashSet<>();
		SimpleBeanPropertyFilter serializeAllPropertyFilter = SimpleBeanPropertyFilter.serializeAllExcept();

		setPropiedades.addAll(ListUtil.toList(ListUtil.split(propiedades)));
		for (String string : setPropiedades) {
			if (!string.isEmpty()) {
				serializeAllPropertyFilter = SimpleBeanPropertyFilter.filterOutAllExcept(setPropiedades);
			}
		}
		return serializeAllPropertyFilter;
	}

}