/**
 * Clase DateSerialize.java 04-08-2016
 * Copyright 2016 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util.rest.modulos;

import java.io.IOException;
import java.sql.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import ec.gob.sri.util.FechaUtil;

/**
 * @author sdrc250216
 *
 */
public class FechaSerializador extends JsonSerializer<Date> {
	public FechaSerializador() {
	}

	@Override
	public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {
		String fecha = FechaUtil.FORMATO_FECHA.format(date);
		jsonGenerator.writeString(fecha);
	}
}
