/**
 * Clase InformacionError.java 04/03/2016
 * Copyright 2016 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util.excepcion.base;

import ec.gob.sri.util.excepcion.BaseExcepcion;

/**
 * @author mbcc010714
 *
 */
public class MensajeInformacion extends BaseExcepcion {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MensajeInformacion() {
	}
	
	public MensajeInformacion(String codigoMensajeInformacion) {
		super(codigoMensajeInformacion);
	}
	
	public String agregarMensajeInformacion() {
		return error;
	}
}
