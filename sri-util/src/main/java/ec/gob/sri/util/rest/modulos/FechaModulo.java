/**
 * Clase DateModulo.java 04-08-2016
 * Copyright 2016 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util.rest.modulos;

import java.sql.Date;

import com.fasterxml.jackson.databind.module.SimpleModule;

/**
 * @author sdrc250216
 *
 */
public class FechaModulo extends SimpleModule {
	private static final long serialVersionUID = 1L;

	public FechaModulo() {
		super();
		addSerializer(Date.class, new FechaSerializador());
	}
}
