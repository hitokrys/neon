/**
 * Clase FechaUtil.java 20-07-2016
 * Copyright 2016 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Utilitario con métodos comunes para tratar fechas
 * 
 * @author djlu030215
 *
 */
public final class FechaUtil {

	public static final Locale LOCALIDAD_ECUADOR = new Locale("ES", "EC");
	public static final String PATRON_FECHA_SIMPLE = "yyyy-MM-dd";
	public static final String PATRON_FECHA = "dd/MM/yyyy";
	public static final String PATRON_TIEMPO_SIMPLE = "hh:mm:ss.SSS";
	public static final String PATRON_FECHA_COMPLETO = PATRON_FECHA_SIMPLE + " " + PATRON_TIEMPO_SIMPLE;

	public static final SimpleDateFormat FORMATO_FECHA = new SimpleDateFormat(PATRON_FECHA);

	public static final SimpleDateFormat FORMATO_FECHA_SIMPLE = new SimpleDateFormat(PATRON_FECHA_SIMPLE,
			LOCALIDAD_ECUADOR);
	public static final SimpleDateFormat FORMATO_FECHA_COMPLETO = new SimpleDateFormat(PATRON_FECHA_COMPLETO,
			LOCALIDAD_ECUADOR);
	public static final SimpleDateFormat FORMTATO_TIEMPO = new SimpleDateFormat(PATRON_TIEMPO_SIMPLE,
			LOCALIDAD_ECUADOR);

	/**
	 * constructor
	 */
	private FechaUtil() {
	}

	/**
	 * Transforma tipo de dato fecha a cadena en un formato fecha tiempo
	 * (dd/MM/yyyy HH:mm)
	 * 
	 * @param fecha ingreso
	 * @return cadena en un formato fecha tiempo
	 */
	public static String formatoCompleto(Date fecha) {
		if (fecha == null) {
			return null;
		} else {
			return FORMATO_FECHA_COMPLETO.format(fecha);
		}
	}

	/**
	 * Transforma tipo de dato fecha a cadena en un formato fecha (dd/MM/yyyy)
	 * 
	 * @param fecha ingreso
	 * @return cadena en un formato fecha
	 */
	public static String formatoSimple(Date fecha) {
		if (fecha == null) {
			return null;
		} else {
			return FORMATO_FECHA_SIMPLE.format(fecha);
		}
	}

	/**
	 * Parse Date
	 * @param text ingreso
	 * @return
	 */
	public static Date parseDate(String text) {
		try {
			return dateFormatDefault.get().parse(text);

		} catch (ParseException e) {
			throw new IllegalArgumentException("El texto: '" + text + "' no se puede convertir", e);
		}
	}

	private static ThreadLocal<DateFormat> dateFormatDefault = new ThreadLocal<DateFormat>() {

		@Override
		public DateFormat get() {
			return super.get();
		}

		@Override
		protected DateFormat initialValue() {
			return new SimpleDateFormat(PATRON_FECHA, LOCALIDAD_ECUADOR);
		}

		@Override
		public void remove() {
			super.remove();
		}

		@Override
		public void set(DateFormat value) {
			super.set(value);
		}
	};

}
