/**
 * Clase TablaDatos.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util.rest.tabladatos;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.enterprise.util.Nonbinding;

@Inherited
@Target({ TYPE, METHOD })
@Retention(RUNTIME)
public @interface TablaDatos {
	
	// Propiedad utilizadas por el Interceptor y el Filtro de propiedades de Jackson
	public static final String FILTER_NAME = "dataTableFilter";
	public static final String FILTER_DATA_PROPERTIES = "mDataProp_";
	
	// Empty String
	public static final String EMPTY_FILTER = "";
	
	//Propiedad para registrar la clave en el mapa de salida que contiene el objeto DATAPAGE
	public static final String DATAPAGE_PROPERTY = "_DATAPAGE_PROPERTY_";	
	
	
	@Nonbinding public TablaDatosFiltro tipoFiltro() default TablaDatosFiltro.FILTER_DEFAULT;
	@Nonbinding public String propiedades() default EMPTY_FILTER;

}
