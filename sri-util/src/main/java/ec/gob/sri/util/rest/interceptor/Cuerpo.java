/**
 * Clase Cuerpo.java 11 de jul. de 2016
 * Copyright 2016 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util.rest.interceptor;

/**
 * @author masc010815
 *
 */
public class Cuerpo {
	
	
	private Object datos;
	private Mensaje mensaje;
	public Cuerpo() {
	}

	/**
	 * @return the datos
	 */
	public Object getDatos() {
		return datos;
	}


	/**
	 * @param datos the datos to set
	 */
	public void setDatos(Object datos) {
		this.datos = datos;
	}


	/**
	 * @return the mensaje
	 */
	public Mensaje getMensaje() {
		return mensaje;
	}

	/**
	 * @param mensaje the mensaje to set
	 */
	public void setMensaje(Mensaje mensaje) {
		this.mensaje = mensaje;
	}
	
}
