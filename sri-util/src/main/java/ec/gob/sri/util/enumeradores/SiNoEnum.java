/**
 * Clase SiNoEnum.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util.enumeradores;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author jada270709
 */
public enum SiNoEnum implements Serializable{
    /**
	 * <p>
	 * Campo con valor SI : SI
	 * </p>
	 */
	SI(Boolean.TRUE),

	/**
	 * <p>
	 * Campo con valor NO : NO
	 * </p>
	 */
	NO(Boolean.FALSE);

	/**
     *  
     */
	public Boolean valor;

	/**
	 * SiNo constructor
	 */
	private SiNoEnum(Boolean valor) {
		setValor(valor);
	}

	/**
	 * Get the VALOR property
	 * 
	 * @return Boolean
	 */
	public Boolean getValor() {
		return valor;
	}

	/**
	 * Set the VALOR property.
	 * 
	 * @param value the new value
	 */
	public void setValor(Boolean value) {
		this.valor = value;
	}

	/**
	 * Return the SiNo from a string value
	 * 
	 * @return SiNo enum object
	 */
	public static SiNoEnum fromString(String value) {
		String newValue = value.toLowerCase();
		if("si".equals(newValue)) {
			return SiNoEnum.SI;
		} else if("no".equals(newValue)) {
			return SiNoEnum.NO;
		}
		
		//return valueOf(value);
		return null;
	}

	/**
	 * Return a Collection of all literal values for this enumeration
	 * 
	 * @return Collection literal values
	 */
	@SuppressWarnings("rawtypes")
	public static Collection literals() {
		final Collection<String> literals = new ArrayList<String>(values().length);
		for (int i = 0; i < values().length; i++) {
			literals.add(values()[i].name());
		}
		return literals;
	}
}
