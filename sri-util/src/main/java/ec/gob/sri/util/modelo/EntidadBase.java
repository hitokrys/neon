/**
 * Clase EntidadBase.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util.modelo;

import java.io.Serializable;
import javax.persistence.MappedSuperclass;
import com.fasterxml.jackson.annotation.JsonFilter;
import ec.gob.sri.util.rest.tabladatos.TablaDatos;

/**
 * Entidad base con Id Numerico
 *
 * @author mrobayo
 * 
 */
@JsonFilter(value=TablaDatos.FILTER_NAME)
@MappedSuperclass
public abstract class EntidadBase implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 @Override
	    public int hashCode() {
	    	return super.hashCode();
	    }


}
