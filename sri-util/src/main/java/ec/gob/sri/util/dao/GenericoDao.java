/**
 * Clase GenericoDao.java 15 de feb. de 2016
 * Copyright 2016 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 * Implementación del patrón de acceso a datos con métodos genéricos CRUD
 * 
 * @author djlu030215
 */
public class GenericoDao<T, I extends Serializable> {

	@Inject
	protected EntityManager em;

	private final Class<T> tablaEntidad;

	/**
	 * Constructor
	 * 
	 * @param tablaEntidad
	 */
	public GenericoDao(final Class<T> tablaEntidad) {
		this.tablaEntidad = tablaEntidad;
	}

	/**
	 * Método genérico para insertar un registro
	 * 
	 * @param tablaEntidad
	 *            tabla de tipo entidad
	 */
	public void crear(final T tablaEntidad) {
		em.persist(tablaEntidad);
	}

	/**
	 * Método genérico para actualizar un registro
	 * 
	 * @param tablaEntidad
	 *            tabla de tipo entidad
	 * @return tabla de tipo entidad con registro modificado
	 */
	public T modificar(final T tablaEntidad) {
		return em.merge(tablaEntidad);
	}

	/**
	 * Método genérico para buscar un registro correspondiente a su
	 * identificador principal
	 * 
	 * @param identificador
	 *            código o identificador principal
	 * @return tabla de tipo entidad
	 */
	public T obtenerPorCodigo(final I identificador) {
		return em.find(tablaEntidad, identificador);
	}

	/**
	 * Método genérico para obtener todos los registros
	 * 
	 * @return lista de registros correspondiente a la tabla de tipo entidad
	 */
	public List<T> obtenerTodos() {
		TypedQuery<T> consulta = em.createQuery("Select t from " + tablaEntidad.getSimpleName() + " t", tablaEntidad);
		return consulta.getResultList();
	}

	/**
	 * Método genérico para obtener todos los registros paginados
	 * 
	 * @param numeroDePagina
	 *            número de la página
	 * @param elementosPorPagina
	 *            tamaño o elementos de la pagína
	 * @return lista de registros correspondiente a la tabla de tipo entidiad
	 *         agrupados en páginas
	 */
	public List<T> obtenerTodos(int numeroDePagina, int elementosPorPagina) {
		TypedQuery<T> consulta = em.createQuery("Select t from " + tablaEntidad.getSimpleName() + " t", tablaEntidad);
		consulta.setFirstResult((numeroDePagina - 1) * elementosPorPagina);
		consulta.setMaxResults(elementosPorPagina);
		return consulta.getResultList();
	}
}
