/**
 * Clase RestResponseBase.java 13 de jun. de 2016
 * Copyright 2016 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util.rest.interceptor;

/**
 * @author sdrc250216
 * @param <Object>
 *
 */
public class TramaRestBase {

	private Cabecera cabecera;
	private Cuerpo cuerpo;
	public TramaRestBase() {
	}
	/**
	 * @return the cabecera
	 */
	public Cabecera getCabecera() {
		return cabecera;
	}
	/**
	 * @param cabecera the cabecera to set
	 */
	public void setCabecera(Cabecera cabecera) {
		this.cabecera = cabecera;
	}
	/**
	 * @return the cuerpo
	 */
	public Cuerpo getCuerpo() {
		return cuerpo;
	}
	/**
	 * @param cuerpo the cuerpo to set
	 */
	public void setCuerpo(Cuerpo cuerpo) {
		this.cuerpo = cuerpo;
	}
	
	
}
