/**
 * Clase SeveridadError.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.util.excepcion.tipo;

import ec.gob.sri.util.enumeradores.EnumeracionTitulada;

/**
 * @author Hugo Camilo Robayo Ayala
 * TODO hay que describir en que casos poner la severidad
 */
public enum SeveridadError implements EnumeracionTitulada{

	
	ADVERTENCIA ("Advertencia"),
	GRAVE ("Grave"),
	FATAL ("Fatal");
	
	
	private final String value;
	
	private SeveridadError(final String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}	
	
	
	@Override
	public String toString() {
		if (value == null){
			getValue();
		}
		return name();
	}

	@Override
	public String getName() {
		return name();
	}
	
}
 

	

