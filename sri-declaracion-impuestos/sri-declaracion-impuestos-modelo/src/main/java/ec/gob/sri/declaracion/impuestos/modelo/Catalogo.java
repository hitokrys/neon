/**
 * Clase ComprobantesRecibidos.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.declaracion.impuestos.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import ec.gob.sri.core.model.EntidadSimple;


/**
 * @author mbcc010714
 *
 */
@Entity
@Table(name = "CATALOGO")
public class Catalogo extends EntidadSimple<Long> implements Serializable {
	private static final long serialVersionUID = 1L;

	//private long codigoCatalogo;

	private String descripcion;

	private String nemonico;

	private String valor;
	
	/*
	@OneToMany(mappedBy="catalogoMes")
	private List<DeclaracionIvaMensual> listaCatalogoMes;

	@OneToMany(mappedBy="catalogoFormaPago")
	private List<DeclaracionIvaMensual> listaCatalogoFormaPago;

	@OneToMany(mappedBy="catalogoAnio")
	private List<DeclaracionIvaMensual> listaCatalogoAnio;
 
	 * */


	public Catalogo() {
	}
	
	public Catalogo(Long id) {
		this.id = id;
	}
	

	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "catalogo_secuencia")
	@SequenceGenerator(name = "catalogo_secuencia", sequenceName = "SEQ_CATALOGO", allocationSize = 1)
	@Column(name="CODIGO_CATALOGO")	
	public Long getId() {
		return this.id;
	}	


	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNemonico() {
		return this.nemonico;
	}

	public void setNemonico(String nemonico) {
		this.nemonico = nemonico;
	}

	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	/*
	public List<DeclaracionIvaMensual> getListaCatalogoMes() {
		return listaCatalogoMes;
	}

	public void setListaCatalogoMes(List<DeclaracionIvaMensual> listaCatalogoMes) {
		this.listaCatalogoMes = listaCatalogoMes;
	}

	public List<DeclaracionIvaMensual> getListaCatalogoFormaPago() {
		return listaCatalogoFormaPago;
	}

	public void setListaCatalogoFormaPago(List<DeclaracionIvaMensual> listaCatalogoFormaPago) {
		this.listaCatalogoFormaPago = listaCatalogoFormaPago;
	}

	public List<DeclaracionIvaMensual> getListaCatalogoAnio() {
		return listaCatalogoAnio;
	}

	public void setListaCatalogoAnio(List<DeclaracionIvaMensual> listaCatalogoAnio) {
		this.listaCatalogoAnio = listaCatalogoAnio;
	}	
 
	 * */


}