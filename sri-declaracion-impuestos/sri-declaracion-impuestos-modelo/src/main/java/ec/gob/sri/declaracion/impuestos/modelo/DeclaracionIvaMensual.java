/**
 * Clase ComprobantesRecibidos.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.declaracion.impuestos.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ec.gob.sri.core.model.EntidadSimple;

import javax.persistence.Basic;


/**
 * @author mbcc010714
 *
 */
@Entity
@Table(name="DECLARACION_IVA_MENSUAL")
public class DeclaracionIvaMensual extends EntidadSimple<Long> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private byte[] archivoDeclaracion;
	private Date fechaDeclaracion;
	private String observacion;
	private Catalogo catalogoMes;
	private Catalogo catalogoFormaPago;
	private Catalogo catalogoAnio;

	public DeclaracionIvaMensual() {}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "declaracion_iva_mensual_sequencia")
	@SequenceGenerator(name = "declaracion_iva_mensual_sequencia", sequenceName = "SEQ_DECLARACION_IVA_MENSUAL", allocationSize = 1)
	@Column(name="CODIGO_DECLARACION_IVA_MENSUAL")	
	public Long getId() {
		return this.id;		
	}
	
	
	@Lob
	@Column(name="ARCHIVO_DECLARACION")
	@Basic(fetch = FetchType.LAZY)
	public byte[] getArchivoDeclaracion() {
		return this.archivoDeclaracion;
	}

	public void setArchivoDeclaracion(byte[] archivoDeclaracion) {
		this.archivoDeclaracion = archivoDeclaracion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_DECLARACION")	
	public Date getFechaDeclaracion() {
		return this.fechaDeclaracion;
	}

	public void setFechaDeclaracion(Date fechaDeclaracion) {
		this.fechaDeclaracion = fechaDeclaracion;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MES")	
	public Catalogo getCatalogoMes() {
		return catalogoMes;
	}

	public void setCatalogoMes(Catalogo catalogoMes) {
		this.catalogoMes = catalogoMes;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FORMA_PAGO")
	public Catalogo getCatalogoFormaPago() {
		return catalogoFormaPago;
	}

	public void setCatalogoFormaPago(Catalogo catalogoFormaPago) {
		this.catalogoFormaPago = catalogoFormaPago;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ANIO")
	public Catalogo getCatalogoAnio() {
		return catalogoAnio;
	}

	public void setCatalogoAnio(Catalogo catalogoAnio) {
		this.catalogoAnio = catalogoAnio;
	}
}