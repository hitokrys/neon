/**
 * Clase ArchivoDeclaracionIva.java 4/1/2016
 * Copyright 2016 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.declaracion.impuestos.interfaz.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MultivaluedMap;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import ec.gob.sri.core.util.archivo.Archivo;
import ec.gob.sri.declaracion.impuestos.modelo.Catalogo;
import ec.gob.sri.declaracion.impuestos.modelo.DeclaracionIvaMensual;

/**
 * @author mbcc010714
 *
 */
public class UtilidadesDeclaracionIva {
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String CONTENT_DISPOSITION = "Content-Disposition";
	
	public UtilidadesDeclaracionIva() {
	}
	private Archivo procesarCargaArchivo(MultipartFormDataInput multipartFormDataInput) throws IOException{
		String nombreArchivo = "";
		String contentType ="";
		byte [] bytesArchivo = null;		
		Map<String, List<InputPart>> mapaDatosEntrada = multipartFormDataInput.getFormDataMap();
		List<InputPart> listaPartesDatosEntrada = mapaDatosEntrada.get("fileUploadId");

		for (InputPart parteDatosEntrada : listaPartesDatosEntrada) {
			MultivaluedMap<String, String> cabecera = parteDatosEntrada.getHeaders();
			nombreArchivo = obtenerCabecera(cabecera,CONTENT_DISPOSITION);
			contentType   = obtenerCabecera(cabecera,CONTENT_TYPE);

			InputStream inputStream = parteDatosEntrada.getBody(InputStream.class,null);
			bytesArchivo = IOUtils.toByteArray(inputStream);				
		}	
		return new Archivo(nombreArchivo, bytesArchivo, contentType);
	}	

	private String obtenerCabecera(MultivaluedMap<String, String> mapaCabecera, String cabecera) {
		if (cabecera.compareTo(CONTENT_DISPOSITION) == 0){
			String[] arregloCabeceras = mapaCabecera.getFirst(cabecera).split(";");
			for (String nombreArchivo : arregloCabeceras) {
				if ((nombreArchivo.trim().startsWith("filename"))) {

					String[] nombreExplicito = nombreArchivo.split("=");					
					return nombreExplicito[1].trim().replaceAll("\"", "");
				}
			}			
		}else if (cabecera.compareTo(CONTENT_TYPE) == 0){
			return mapaCabecera.getFirst(CONTENT_TYPE);
		}				
		return "unknown";
	}

	public void crearEscribirArchivo(byte[] contenido, String nombreArchivo) throws IOException {
		File file = new File(nombreArchivo);
		if (!file.exists()) {
			file.createNewFile();
		}
		FileOutputStream fileOutputStream = new FileOutputStream(file);

		fileOutputStream.write(contenido);
		fileOutputStream.flush();
		fileOutputStream.close();
	}
		
	private String getExtraParameter(MultipartFormDataInput input, String paramName) throws IOException {

		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts = uploadForm.get(paramName);

		return inputParts.get(0).getBody(String.class, null);
	}	
	
	
	public void construirObjetoDeclaracionIvaMensual(MultipartFormDataInput multipartFormDataInput, DeclaracionIvaMensual declaracionIvaMensual) throws IOException{
		Archivo item = procesarCargaArchivo(multipartFormDataInput);		

		String mes = getExtraParameter(multipartFormDataInput, "mes");
		String anio = getExtraParameter(multipartFormDataInput, "anio");
		String formaPago = getExtraParameter(multipartFormDataInput, "formaPago");
		
		declaracionIvaMensual.setArchivoDeclaracion(item.getContenido());
		declaracionIvaMensual.setCatalogoAnio(new Catalogo(Long.valueOf(anio)));
		declaracionIvaMensual.setCatalogoMes(new Catalogo(Long.valueOf(mes)));
		declaracionIvaMensual.setCatalogoFormaPago(new Catalogo(Long.valueOf(formaPago)));
	}	
}
