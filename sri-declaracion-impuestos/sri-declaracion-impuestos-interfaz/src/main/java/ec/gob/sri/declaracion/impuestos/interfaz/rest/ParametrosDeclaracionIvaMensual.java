/**
 * 
 */
package ec.gob.sri.declaracion.impuestos.interfaz.rest;

import java.io.Serializable;

import javax.ws.rs.FormParam;

import org.jboss.resteasy.annotations.providers.multipart.PartType;

public class ParametrosDeclaracionIvaMensual implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String nombreArchivoConExtension;
	private String nombreArchivo;
	private byte[] datosArchivo;
	private String extension;
	private long tamanioArchivo;

	public String getNombreArchivoConExtension() {
		return nombreArchivoConExtension;
	}

	@FormParam("nombreArchivoConExtension")
	public void setNombreArchivoConExtension(String nombreArchivoConExtension) {
		this.nombreArchivoConExtension = nombreArchivoConExtension;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	@FormParam("nombreArchivoConExtension")
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public byte[] getDatosArchivo() {
		return datosArchivo;
	}

	@FormParam("file")
	@PartType("application/octet-stream")
	public void setDatosArchivo(byte[] datosArchivo) {
		this.datosArchivo = datosArchivo;
	}

	public String getExtension() {
		return extension;
	}

	@FormParam("extension")
	public void setExtension(String extension) {
		this.extension = extension;
	}

	public long getTamanioArchivo() {
		return tamanioArchivo;
	}

	@FormParam("tamanioArchivo")
	public void setTamanioArchivo(long tamanioArchivo) {
		this.tamanioArchivo = tamanioArchivo;
	}

	

}