/**
 * Clase Constantes.java 14/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.declaracion.impuestos.interfaz.util;

/**
 * @author mbcc010714
 *
 */
public class Constantes {
	public static final String  DECLARACION_IMPUESTOS_SUBIDA_EXITOSA = "DECLARACION_IMPUESTOS.DECLARACION_SUBIDA_EXITOSA";
	public static final String  DECLARACION_IMPUESTOS_ERROR_CONSULTA = "DECLARACION_IMPUESTOS.ERROR_CONSULTA_DECLARACIONES";
	public static final String  DECLARACION_IMPUESTOS_ERROR_CARGA_ARCHIVO = "DECLARACION_IMPUESTOS.ERROR_CARGA_ARCHIVO";
	public Constantes() {
	}
}
