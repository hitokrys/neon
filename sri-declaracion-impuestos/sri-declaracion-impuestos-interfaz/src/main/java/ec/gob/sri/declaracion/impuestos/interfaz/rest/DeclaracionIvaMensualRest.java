/**
 * Clase ComprobantesRecibidosRest.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.declaracion.impuestos.interfaz.rest;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import ec.gob.sri.declaracion.impuestos.interfaz.util.Constantes;
import ec.gob.sri.core.dao.util.jql.JQLUtil;
import ec.gob.sri.core.dao.util.paging.PaginaDatos;
import ec.gob.sri.core.exception.base.BaseExcepcion;
import ec.gob.sri.core.exception.base.MensajeInformacion;
import ec.gob.sri.core.exception.detect.interceptor.types.InterceptorEnlaceExcepciones;
import ec.gob.sri.core.rest.datatable.TablaDatos;
import ec.gob.sri.core.rest.datatable.TablaDatosBean;
import ec.gob.sri.core.rest.util.MapaDeclarado;
import ec.gob.sri.core.service.BaseFachadaServicio;
import ec.gob.sri.declaracion.impuestos.interfaz.util.UtilidadesDeclaracionIva;
import ec.gob.sri.declaracion.impuestos.logica.local.DeclaracionIvaLocal;
import ec.gob.sri.declaracion.impuestos.modelo.DeclaracionIvaMensual;

/**
 * En este servicio web tipo Rest implementa la frontera entre el cliente web y la fachada de servicios,
 * en este ejemplo implementa las llamadas a los servicios de fachada de la declaracion del IVA Mensual.
 * 
 * Se debe tener presente que desde el servicio web tipo rest para acceder a los servicios de negocio se debe utilizar
 * el Patron de Disenio JEE "Service Facade" [DeclaracionIvaLocal.java].
 * 
 * Los metodos que se implementen en un servicio web tipo Rest deben devolver(return en caso de requerirlo) un 
 * objeto generico de tipo Map [ Map<String,Object> ], este objeto luego es deserializado en la capa del cliente (.js, .html) con
 * los objetos respectivos que forman parte de dicho objeto generico.
 * 
 * @author mbcc010714
 *
 */
@Path("/declaracionIvaMensual")
public class DeclaracionIvaMensualRest {
	
	//Patron Service Facade
	@EJB DeclaracionIvaLocal declaracionIvaLocal;	
	// Patron "Service Facade" del "SRI Core"
	@EJB BaseFachadaServicio baseFachadaServicio;
	
	
	@POST
	@Path("/buscarDeclaracionesIva")	
	@InterceptorEnlaceExcepciones
	@TablaDatos
	public Map<String,Object> buscarDeclaracionesIva(MapaDeclarado<String,Object> typedMap ) {
		Map<String, Object> listaComprobantesRecibidos = new HashMap<String, Object>();
		try {
			DeclaracionIvaMensual dec = typedMap.getObjetoDeclarado(DeclaracionIvaMensual.class);

			JQLUtil hqlQuery = new JQLUtil(DeclaracionIvaMensual.class).filter(dec);
			
			hqlQuery.leftJoin("catalogoMes").leftJoin("catalogoAnio").leftJoin("catalogoFormaPago");			
			
			PaginaDatos<DeclaracionIvaMensual> dataPage = baseFachadaServicio.buscarConsultasPaginadas(DeclaracionIvaMensual.class,
					hqlQuery.toString(), typedMap.getObjetoDeclarado(TablaDatosBean.class, true).paging(),
					hqlQuery.getParametersValue());

			listaComprobantesRecibidos.put(TablaDatos.DATAPAGE_PROPERTY, dataPage);
		} catch (BaseExcepcion baseExcepcion) {
			throw new BaseExcepcion(baseExcepcion, Constantes.DECLARACION_IMPUESTOS_ERROR_CONSULTA);
		}
		return listaComprobantesRecibidos;
	}		
	
	@POST
	@Path("/obtenerCatalogo")
	@InterceptorEnlaceExcepciones
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Map<String, List<Object>> obtenerCatalogo(List<String> valoresNemonicos) {
		return declaracionIvaLocal.consultarCatalogos(valoresNemonicos);
	}
	
	@POST
	@Path("/cargarDeclaracionIvaMensualSimple")
	@InterceptorEnlaceExcepciones
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Map<String, Object> cargarDeclaracionIvaMensualSimple(MultipartFormDataInput multipartFormDataInput) {
		Map<String, Object> resultado = new HashMap<String, Object>();
		DeclaracionIvaMensual declaracionIvaMensual = new DeclaracionIvaMensual();
		try {				
			new UtilidadesDeclaracionIva().construirObjetoDeclaracionIvaMensual(multipartFormDataInput, declaracionIvaMensual);
			
			resultado = declaracionIvaLocal.guardarDeclaracionIvaMensual(declaracionIvaMensual);
			
			resultado.put("mensajeExito", new MensajeInformacion(Constantes.DECLARACION_IMPUESTOS_SUBIDA_EXITOSA).agregarMensajeInformacion());
			//throw new BaseExcepcion("Error");

		} catch (BaseExcepcion | IOException excepcion) {
			throw new BaseExcepcion(excepcion, Constantes.DECLARACION_IMPUESTOS_ERROR_CARGA_ARCHIVO);
		}	
		finally{
			declaracionIvaMensual = null;
		}
		return resultado;
	}
	
	@POST
	@Path("/cargarDeclaracionIvaMensualUno")
	@InterceptorEnlaceExcepciones
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Map<String, Object> cargarDeclaracionIvaMensual(@MultipartForm ParametrosDeclaracionIvaMensual parametrosDeclaracionIvaMensual) {
		Map<String, Object> resultado = new HashMap<String, Object>();		
		try {
			String nombreArchivo = "D:/"	+ parametrosDeclaracionIvaMensual.getNombreArchivoConExtension();
			new UtilidadesDeclaracionIva().crearEscribirArchivo(parametrosDeclaracionIvaMensual.getDatosArchivo(), nombreArchivo);
			resultado.put("mensajeExito",new Object());
		} catch (BaseExcepcion | IOException excepcion) {
			throw new BaseExcepcion(excepcion, Constantes.DECLARACION_IMPUESTOS_ERROR_CARGA_ARCHIVO);
		}
		return resultado;
	}

	@POST
	@Path("/removerArchivo")
	@InterceptorEnlaceExcepciones
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Map<String, Object> removerArchivo(@FormParam("fileName") String nombreArchivo) {
		Map<String, Object> resultado = new HashMap<String, Object>();
		String nombreArchivoTemp = "D:/" + nombreArchivo;
		File file = new File(nombreArchivoTemp);
		file.delete();
		return resultado;
	}
		
}
