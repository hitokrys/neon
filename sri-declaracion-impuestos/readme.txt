                                 base

 Creado usando sri-jee6-archetype.
  
 http://www.sri.gob.ec

 Arquetipo SRI para desarrollo JEE7 sobre WILDFLY 
 ===========

 Este es un proyecto base inicial para Java EE 7 y puede ser deployado utilizando
 maven 3.3.3 o mayor con Java 1.8 o mayor. Esta aplicacion puede ser deployada en cualquier
 contenedor JEE 7, actualmente apunta al WILDFLY 8.0.2-FINAL 

 Modulos del proyecto
 ====================

sri-declaracion-impuestos
====================

Módulo que contiene a los módulos que pertenecen al servicio a programarse y contiene dependencias, profiles y plugins generales. 
Carpeta con el pom.xml con la configuración del servicio.

sri-declaracion-impuestos-web-servicio
====================
Módulo que se empaqueta en un WAR para el despliegue de los servicios web (SOAP o Rest), contiene como librerias sri-declaracion-impuestos-logica, sri-declaracion-impuestos-modelo y externamente sri-core.
WAR

sri-declaracion-impuestos-logica
====================
Módulo que se empaqueta en un EJB JAR para escribir la lógica de los servicios, utiliza el modelo para la capa de DAO, y se encarga de llamadas a capacidades de otros servicios.
JAR


sri-modelo
====================
Modulo que se empaqueta en JAR para representar las entidades de negocio.
JAR
 