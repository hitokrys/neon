/**
 * Clase SaludoDAO.java 05-10-2015 * 
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.declaracion.impuestos.dao;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;





/**
 * @author jada270709
 *
 */
public class RecursosDAO {

	@Produces
	@PersistenceContext(unitName = "DeclaracionImpuestosPU")
	public EntityManager entityManagerDefault;
}
