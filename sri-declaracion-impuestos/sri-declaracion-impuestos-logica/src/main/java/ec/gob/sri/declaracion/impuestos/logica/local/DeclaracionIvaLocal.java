/**
 * Clase ComprobantesConsultaLocal.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.declaracion.impuestos.logica.local;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import ec.gob.sri.declaracion.impuestos.modelo.DeclaracionIvaMensual;

/**
 * @author mbcc010714
 *
 */
@Local
public interface DeclaracionIvaLocal {
	Map<String,Object> guardarDeclaracionIvaMensual(DeclaracionIvaMensual declaracionIvaMensual);
	
	Map<String, List<Object>> consultarCatalogos(List<String> valoresNemonicos);

}
