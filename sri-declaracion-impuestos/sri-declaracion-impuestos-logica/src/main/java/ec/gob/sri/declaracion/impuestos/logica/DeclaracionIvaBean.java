/**
 * Clase ComprobantesConsultaBean.java 29/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.declaracion.impuestos.logica;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import ec.gob.sri.core.util.ListUtil;
import ec.gob.sri.core.service.BaseFachadaServicio;
import ec.gob.sri.declaracion.impuestos.logica.local.DeclaracionIvaLocal;
import ec.gob.sri.declaracion.impuestos.logica.local.DeclaracionIvaMensualLocal;
import ec.gob.sri.declaracion.impuestos.modelo.Catalogo;
import ec.gob.sri.declaracion.impuestos.modelo.DeclaracionIvaMensual;

/**
 * Este bean sesion sin estado implementa el Patron de Disenio JEE "Service Facade", el mismo es el nexo entre la frontera y los patrones de disenio "Service" y/o "DAO". 
 * 
 * Esta implementacion (DeclaracionIvaLocal.java) del patron "Service Facade" esta siendo invocada desde el Servicio Web tipo Rest 
 * "DeclaracionIvaMensualRest.java". Es necesario tener presente que en la implementacion del patron "Service Facade" 
 * se puede invocar al patron "DAO" y/o el patron "Service".
 * 
 * Es importante que la implementacion del patron "Service Facade" sea expuesta mediante una Interface Local.
 *  
 * Para mayor detalle del mismo referirse al documento "SRI Patrones Disenio JEE.odt".
 * 
 * @author mbcc010714
 *
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class DeclaracionIvaBean implements DeclaracionIvaLocal{

	// Patron "Service Facade" del "SRI Core"
	@EJB 
	BaseFachadaServicio baseFachadaServicio;
	
	//Patron "Service"
	@EJB
	DeclaracionIvaMensualLocal declaracionIvaMensualLocal;
	
	@Override
	public Map<String,Object> guardarDeclaracionIvaMensual(DeclaracionIvaMensual declaracionIvaMensual) {
		return declaracionIvaMensualLocal.guardarDeclaracionIvaMensual(declaracionIvaMensual);
	}
	
	public Map<String, List<Object>> consultarCatalogos(List<String> valoresNemonicos){
		Map<String, List<Object>> mapaCatalogos = new HashMap<>();
		
		if (!ListUtil.isNotEmpty(valoresNemonicos)){
			return mapaCatalogos;
		}

		StringBuffer jql = new StringBuffer();
		jql.append(" nemonico in (").append(ListUtil.createCSVString(valoresNemonicos, "?")).append(')');			
		List<Catalogo> catalogos = baseFachadaServicio.buscarPorParametrosSimple(Catalogo.class, jql.toString(), ListUtil.createStringArray(valoresNemonicos));
		
		String nemonico;
		for (Iterator<Catalogo> iterator = catalogos.iterator(); iterator.hasNext();) {
			Catalogo catalogo = iterator.next();
			nemonico = catalogo.getNemonico();
			if ( !mapaCatalogos.containsKey(nemonico) ){
				mapaCatalogos.put(nemonico, ListUtil.toList());
			}
			mapaCatalogos.get(nemonico).add(catalogo);
		}		
		return mapaCatalogos;		
	}

}
