/**
 * Clase ComprobantesRecibidosBean.java 21/12/2015
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.declaracion.impuestos.logica;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import ec.gob.sri.core.service.BaseFachadaServicio;
import ec.gob.sri.core.util.DateUtil;
import ec.gob.sri.declaracion.impuestos.logica.local.DeclaracionIvaMensualLocal;
import ec.gob.sri.declaracion.impuestos.modelo.DeclaracionIvaMensual;

/**
 * Este bean sesion sin estado implementa el Patron de Disenio JEE "Service" su
 * objetivo principal es hacer que la construccion de la logica de negocio sea
 * reutilizable y mas facil de mantener.
 * 
 * Esta implementacion (DeclaracionIvaMensualLocal.java) del patron
 * "Service" esta siendo invocada desde la implementacion del Patron de disenio
 * JEE "Service Facade" [DeclaracionIvaBean.java].
 * 
 * En el caso de requerir ejecutar consultas no complejas, en la implementacion
 * se debe utilizar el componente "BaseFachadaServicio.java que forma parte del
 * API sri_core" la misma que contiene implementacion de metodos que ejecutan
 * sentencias DML genericas y que solo se las debe reutilizar, en el documento
 * "Guia de Uso del Modulo SRI-CORE.odt" se describe como usar el API sri_core.
 * 
 * Es importante que la implementacion del patron sea encapsulada mediante una
 * Interface Local.
 * 
 * Para mayor detalle del mismo referirse al documento
 * "SRI Patrones Disenio JEE.odt".
 *
 * @author mbcc010714
 *
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class DeclaracionIvaMensualBean implements DeclaracionIvaMensualLocal {

	// Patron "Service Facade" del "SRI Core"
	@EJB
	BaseFachadaServicio baseFachadaServicio;

	public DeclaracionIvaMensualBean() {
	}

	public Map<String, Object> guardarDeclaracionIvaMensual(DeclaracionIvaMensual declaracionIvaMensual) {

		Map<String, Object> declaracionIvaMensualGuardada = new HashMap<String, Object>();
		declaracionIvaMensual.setFechaDeclaracion(DateUtil.ahora());
		baseFachadaServicio.guardarEntidad(declaracionIvaMensual);
		
		declaracionIvaMensualGuardada.put("id",declaracionIvaMensual.getId());

		return declaracionIvaMensualGuardada;
	}
}
