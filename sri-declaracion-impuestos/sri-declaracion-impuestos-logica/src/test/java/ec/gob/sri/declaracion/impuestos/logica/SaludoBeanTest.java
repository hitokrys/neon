/**
 * Clase SaludarBeanTest.java 05-10-2015 * 
 * Copyright 2015 Servicio de Rentas Internas.
 * Todos los derechos reservados.
 */
package ec.gob.sri.declaracion.impuestos.logica;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SaludoBeanTest {

	@InjectMocks 
	private SaludoBean saludarBean;
	
	@Test
	public void pruebaSaludar() {
		assertEquals("Hola desde la logica", saludarBean.saludar());
	}
	@Test
	public void pruebaMetodo2() {
		assertEquals("1", saludarBean.saludo2("1"));
	}
	@Test
	public void pruebaMetodo2False() {
		assertEquals("hola", saludarBean.saludo2("2"));
	}
	
}
