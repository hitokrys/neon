package org.chris.pruebas.rest.client;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.ws.rs.core.MediaType;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;

public class DemoFileUploader {
	public static void main(String args[]) throws Exception
    {
       
        
        
        //enviaArchivo();
        enviaArchivo2();
        //envioConAutorizacion();
        envioConAutorizacion2();
        
    }  
     
    private static void envioConAutorizacion2() throws ClientProtocolException, IOException {
    	File file = new File("C:/SampleDocs/UploadDoc.pdf");
    	StringEntity stringEntity= new StringEntity("{\"documentClassCd\": \"Document\",\"metadataItem\":[{\"keyName\": \"DocumentTitle\",\"value\":[\"Sample Name\"],\"typeCd\": \"\"}]}");
    	CloseableHttpClient httpClients = HttpClientBuilder.create().build();
    	HttpPost request = new HttpPost("http://server-name.com:8080/v1/somename0/somname1/somename2");
    	MultipartEntityBuilder builder = MultipartEntityBuilder.create();
    	builder.addBinaryBody("fileContent", file);
    	HttpEntity reqEntity = builder.build();
    	request.setEntity(reqEntity);
    	request.setEntity(stringEntity);
    	request.setHeader("Authorization", "TOKEN");
    	request.setHeader("traceabilityid","12345");
    	request.setHeader("messageid","123");
    	HttpResponse response = httpClients.execute(request);
		
	}

	private static void envioConAutorizacion() throws Exception {
    	File file = new File("C:/SampleDocs/UploadDoc.pdf");
    	ClientRequest request = new ClientRequest("https://serverName:<portNumber>/v1/repositories/documents");
    	 String input = "{fileName=New Microsoft Word Document&fileMetadata={\"documentClassCd\": \"Document\",\"metadataItem\":[{\"keyName\": \"DocumentTitle\",\"value\":[\"ELWAY JOHN\"],\"typeCd\": \"\"}]}}}";
    	 request.body(MediaType.APPLICATION_JSON_TYPE, input);
    	 request.body(MediaType.APPLICATION_OCTET_STREAM_TYPE, file);
    	request.header("authorization", "Bearer tset");
    	request.header("traceabilityid","12345");
    	request.header("messageid","123");
    	ClientResponse<String> response = request.post();

		
	}

	private static void enviaArchivo2() throws ClientProtocolException, IOException {
    	CloseableHttpClient client = HttpClients.createDefault();
    	 HttpPost httpPost = new HttpPost("http://localhost:8080/RestEasy-Example/employee/upload");
    	 File file = new File("C:\\Users\\cgad070313\\Pictures\\duda.png") ;
    	 
    	 /*
    	 RequestConfig requestConfig = RequestConfig.copy(RequestConfig.DEFAULT)
    			    .setProxy(new HttpHost("XXX.XXX.XXX.XXX", 8080))
    			    .build();
    			    httppost.setConfig(requestConfig);

    			    httppost.addHeader("content-type", "application/x-www-form-urlencoded;charset=utf-8");
    			    */
    	 
    	 MultipartEntityBuilder builder = MultipartEntityBuilder.create();
    	 builder.addBinaryBody("file", file,
    		      ContentType.APPLICATION_OCTET_STREAM, "duda.png");
    		    HttpEntity multipart = builder.build();
    		 
    		    httpPost.setEntity(multipart);
    		    HttpResponse response = client.execute(httpPost);
    		    HttpEntity resEntity = response.getEntity();

    		    System.out.println(response.getStatusLine());
    		    if (resEntity != null) {;
    		      System.out.println(EntityUtils.toString(resEntity));
    		    }

    		    client.close();
		
	}

	private static void enviaArchivo() {
    	 DemoFileUploader fileUpload = new DemoFileUploader () ;
         File file = new File("C:\\Users\\cgad070313\\Pictures\\duda.png") ;
         //Upload the file
         try {
			fileUpload.executeMultiPartRequest("http://localhost:8080/RestEasy-Example/employee/upload", 
			         file, file.getName(), "File Uploaded :: Tulips.jpg") ;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void executeMultiPartRequest(String urlString, File file, String fileName, String fileDescription) throws Exception 
    {
        HttpClient client = new DefaultHttpClient() ;
        HttpPost postRequest = new HttpPost (urlString) ;
        try
        {
            //Set various attributes 
            MultipartEntity multiPartEntity = new MultipartEntity () ;
            multiPartEntity.addPart("fileDescription", new StringBody(fileDescription != null ? fileDescription : "")) ;
            multiPartEntity.addPart("filename", new StringBody(fileName != null ? fileName : file.getName())) ;
  
            FileBody fileBody = new FileBody(file, "application/octect-stream") ;
            //Prepare payload
            //multiPartEntity.addPart("attachment", fileBody) ;
            multiPartEntity.addPart("file", fileBody) ;
            
  
            //Set to request body
            postRequest.setEntity(multiPartEntity) ;
             
            //Send request
            HttpResponse response = client.execute(postRequest) ;
             
            //Verify response if any
            if (response != null)
            {
                System.out.println(response.getStatusLine().getStatusCode());
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace() ;
        }
    }
}
