/**
 * @ngdoc configuracion
 * @name GrundFile.js
 * @desc Archivo de configuracion para la ejecucion de tareas de desarrollo del modulo de declaracion de impuestos. 25
 *       de feb. de 2016
 */
module.exports = function(grunt) {
	'use strict';
  grunt.initConfig({
    jshint: {
      all: [
        'webapp/recursos/js/**/*.js'
        //'GruntFile.js', 'tasks/**/*.js',  '<%= nodeunit.tests %>'
      ],
	  options: {
		jshintrc: './.jshintrc',
		reporter: require('jshint-html-reporter'),
	    reporterOutput: 'webapp/test_reportes/jshint/jshint-reporte.html'
	  }

    }, 
    karma: {
    	  unit: {
    	    configFile: 'karma.conf.js',
    	    autoWatch: true
    	  }
    },   
    clean: ["webapp/test_reportes/*"]

  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-contrib-clean');

  //Se registran las tareas a ser ejecutadas
  grunt.registerTask('ejecuta-clean', ['clean']);
  grunt.registerTask('ejecuta-jshint', ['jshint']);
  grunt.registerTask('ejecuta-karma', ['karma']);
  grunt.registerTask('ejecuta-todo', ['clean','karma','jshint']);

};