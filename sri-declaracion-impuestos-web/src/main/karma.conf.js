// Karma configuration
// Generated on Mon Jan 25 2016 12:18:00 GMT-0500 (Hora est. Pacífico, Sudamérica)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: 'webapp',

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
	'test/bower_components/jquery/dist/jquery.js',
	'test/bower_components/datatables/media/js/jquery.dataTables.js',
	'test/bower_components/jquery-validation/dist/jquery.validate.js',
	'test/bower_components/bootstrap-fileinput/js/fileinput.js',
	'test/bower_components/bootstrap-fileinput/js/fileinput_locale_es.js',
	'test/bower_components/angular/angular.js',
	'test/bower_components/angular-mocks/angular-mocks.js',
	'test/bower_components/angular-translate/angular-translate.js',
	'test/bower_components/angular-translate-loader-partial/angular-translate-loader-partial.js',
	'test/bower_components/angular-translate-loader-partial/angular-translate-loader-partial.js',
	'test/bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js',
	'test/bower_components/angular-dynamic-locale/src/tmhDynamicLocale.js',
	'test/bower_components/kendo-ui-core/js/kendo.ui.core.min.js',
	
	'test/lib/sri/util.js',
	
	'recursos/js/*.js',
	'recursos/js/**/*.js',
	'test/config/*.js',
    'test/js/**/*.js'
    ],

    // list of files to exclude
    exclude: [
    ],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {//archivos de analisis de cobertura y jshint
    	'recursos/js/**/*.js': ['jshint','coverage']
    },
    
    jshintPreprocessor: {
    	jshintrc: './.jshintrc',
    	stopOnError: false
    },

    coverageReporter: {//caracteristicas de archivo de cobertura
    	dir: 'test_reportes/cobertura/', 
    	reporters: [
		{
		    type: 'text-summary'
		},
    	{
    		type: 'lcov', subdir: '.'
    	},
    	{
    		type: 'cobertura', subdir: '.', 
    		file: 'cobertura.xml'
    	}
    	]
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress', 'jshint', 'junit', 'coverage'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],
    //browsers: ['Chrome', 'Firefox'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,
    plugins: [
		'karma-chrome-launcher',
		'karma-firefox-launcher',
		'karma-jasmine',
		'karma-junit-reporter',
		'karma-coverage',
		'karma-jshint-preprocessor'
		],
	junitReporter: { //archivo de salida junit
		outputDir: 'test_reportes/junit',
		outputFile: 'junit-reporte.xml',
		useBrowserName: false,
		suite: 'webapp.test.js'
    },

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  });
};
