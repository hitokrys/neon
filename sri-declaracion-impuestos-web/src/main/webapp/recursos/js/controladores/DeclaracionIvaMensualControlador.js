(function (angular) {
	'use strict';
	/**
	 * @ngdoc Controlador
	 * @name DeclaracionIvaMensualControlador
	 * @description Es el controlador que forma parte del modulo (declaraciones.declaracionIvaMensual) de declaraciones de IVA mensual que permite 
	 * la interaccion dinamica con las paginas .html de declaracion de IVA mensual del mismo modulo.
	 * @param $scope: Es el ámbito de las variables. Todas las variables y/o metodos definidas en el objeto $scope son
	 * accesibles desde las paginas .html de este modulo.
	 * @param declaracionIvaMensualServicio: Es la referencia de los servicios que están implementados en DeclaracionIvaMensualServicio.js
	 * @returns.
	 */
	angular.module("declaraciones.declaracionIvaMensual").controller('DeclaracionIvaMensualControlador',DeclaracionIvaMensualControlador);
	DeclaracionIvaMensualControlador.$inject = ['$scope','AlertaFactory', 'ConstanteValue','DeclaracionIvaMensualServicio','InternacionalizacionDeclaracionIvaServicio'];
	
	function DeclaracionIvaMensualControlador($scope, alertaFactory, Constante, declaracionIvaMensualServicio,internacionalizacionDeclaracionIvaServicio) {
		internacionalizacionDeclaracionIvaServicio.usarIdiomaActual($scope.lenguajeActual);
		$scope.valorVariableMensaje="";
    	$scope.catalogo = {};    	
		$scope.anexo = {
				mes:  null,
				anio: null,
				formaPago:null              
		};	     
			     	
		$scope.subirDeclaracionIvaMensual =function(){							
			
			if ($("#frmDeclaracionIvaMensual").valid()){
				
				declaracionIvaMensualServicio.subirDeclaracionIvaMensual('#fileUploadId');
				
			}else{
				//fnShowAlertMsg("#frmDeclaracionIvaMensual", {status: 'danger', titulo: "", mensaje: "Por favor llene los siguientes datos. Los campos con (*) son obligatorios."});
				alertaFactory.agregarAlerta(Constante.ERROR, "SRI.GENERAL.CAMPOS_OBLIGATORIOS");
			}						
		};
		
	    $scope.selectOptions = {
	            dataTextField: "descripcion",
	            dataValueField: "id",
	            valuePrimitive: true,
	            autoBind: false
        };	
	    
	    $scope.opcionesDeclaracionIvaMensual = {
			    uploadUrl: "", 
			    showUpload: false,
			    showPreview: false,
			    allowedFileExtensions : ['txt','xml'],		    
			    uploadExtraData: function(){
					return $scope.anexo;
				},
			    uploadAsync: true,
			    maxFileCount: 1,
			    maxFileSize: 10000, //10MB,
			    elErrorContainer: "#errorDeclaracionIvaMensual",
			    language:$scope.lenguajeActual
        };
	
		$scope.inicializarSubidaDeclaracionIvaMensual =function(){
			declaracionIvaMensualServicio.inicializarSubidaDeclaracionIvaMensual('#fileUploadId',$scope.opcionesDeclaracionIvaMensual);		    

			var sFormId = "frmDeclaracionIvaMensual";	
		    $('#fileUploadId').on('fileuploaded', function(event, data, previewId, index) {
		        var response = (data && typeof(data.response) !== 'undefined')? data.response: "DECLARACION_IMPUESTOS.DECLARACION_SUBIDA_EXITOSA";	
		        
		        $("#uploadFileButtonId").attr("disabled", "disabled");
		        
		        alertaFactory.agregarAlerta(Constante.EXITO, response.mensajeExito);
		        $scope.$apply();		        
		        //fnShowAlertMsg("#"+sFormId, {status: 'success', titulo: "", mensaje: response.mensajeExito});
		    });
		    
		    $('#fileUploadId').on('fileuploaderror', function(event, data, previewId, index) {		    	
		        //$("#errorDeclaracionIvaMensual").removeClass("hidden");
		    	$scope.valorVariableMensaje = $scope.opcionesDeclaracionIvaMensual.allowedFileExtensions;
		    	
		    	var mensajeError = (data.filenames.length == 0)? "DECLARACION_IMPUESTOS.EXTENSION_ARCHIVO_NO_SOPORTADO":data.jqXHR.responseJSON.error;
		    	
		        alertaFactory.agregarAlerta(Constante.ERROR, mensajeError);
		        $scope.$apply();
		    	//fnShowAlertMsg("#"+sFormId, {status: 'danger', titulo: "Error:", mensaje: "pedrito"});		        
	        });
		    
		    $('#fileUploadId').on('filebatchuploaderror', function(event, data, previewId, index) {

		    	alertaFactory.agregarAlerta(Constante.ERROR, data.jqXHR.responseJSON.error);
		        $scope.$apply();

		    });
			
		    fnGetCatalog(["MES","ANIO","FORMA_PAGO"], function(data){
		    	 $scope.catalogo = data;	
		    });

		};
		
		$scope.inicializarSubidaDeclaracionIvaMensual();		
    };

}(window.angular));
