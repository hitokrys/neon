(function (angular) {
	'use strict';
	/**
	 * @ngdoc Controlador
	 * @name ConsultaDeclaracionIvaMensualControlador
	 * @description Es el controlador que forma parte del modulo (declaraciones.declaracionIvaMensual) de declaraciones de IVA mensual que permite 
	 * la interaccion dinamica con las paginas .html de consultas de declaraciones de IVA mensual del mismo modulo.
	 * @param $scope: Es el ámbito de las variables. Todas las variables y/o metodos definidas en el objeto $scope son
	 * accesibles desde las paginas .html de este modulo.
	 * @param declaracionIvaMensualServicio: Es la referencia de los servicios que están implementados en DeclaracionIvaMensualServicio.js
	 * @returns.
	 */
	angular.module("declaraciones.declaracionIvaMensual").controller('ConsultaDeclaracionIvaMensualControlador',ConsultaDeclaracionIvaMensualControlador);
	ConsultaDeclaracionIvaMensualControlador.$inject = ['$scope','AlertaFactory', 'ConstanteValue','DeclaracionIvaMensualServicio','InternacionalizacionDeclaracionIvaServicio'];
    	    
    function ConsultaDeclaracionIvaMensualControlador($scope, alertaFactory, Constante, declaracionIvaMensualServicio, internacionalizacionDeclaracionIvaServicio) {	

    	internacionalizacionDeclaracionIvaServicio.usarIdiomaActual($scope.lenguajeActual);
		  
		$scope.catalogo = {}; 
		$scope.declaracion = {
				id:  null,
				fechaDeclaracion: null,
				catalogoMes:{id:null},
				catalogoAnio:{id:null},
				catalogoFormaPago:{id:null}
		};		
		
		$scope.columns = 
			[{data:"id",title:"ID",visible: false},
			 {data:"fechaDeclaracion",title_es:"Fecha",title_en:"Date"},/*, render: fnRenderDate*/
			 {data:"catalogoMes.descripcion",title_es:"Mes",title_en:"Month"},
			 {data:"catalogoAnio.descripcion",title_es:"Anio",title_en:"Year"},
			 {data:"catalogoFormaPago.descripcion",title_es:"Forma Pago",title_en:"Type Payment"},
			 {data:null,title:"",width: "5%",orderable:false,render:function( data, type, row, meta ){
				 var pencil = "<span class='glyphicon glyphicon-pencil sri-color-celeste sri-link' aria-hidden='true'></span>";
				 var trash = "<span class='glyphicon glyphicon-trash sri-color-rojo sri-link' aria-hidden='true'></span>";
				 return pencil + "&nbsp;&nbsp;&nbsp;" +trash;
			 }}
			];	
		
	    $scope.selectOptions = {
	            dataTextField: "descripcion",
	            dataValueField: "id",
	            valuePrimitive: true,
	            autoBind: false
        };	
	    
		$scope.buscarDeclaracionIvaMensual = function(sTableId){
			declaracionIvaMensualServicio.buscarDeclaracionIvaMensual("#"+sTableId);
		};	

	    $scope.inicializarConsultaDeclaracionIvaMensual = function () {
	    	
			var jsonData = {};						
			jsonData["@class:ec.gob.sri.declaracion.impuestos.modelo.DeclaracionIvaMensual"] = $scope.declaracion;	
			
			declaracionIvaMensualServicio.inicializarConsultaDeclaracionIvaMensual('#tblDeclaracionesIvaMensual', jsonData, $scope.columns);
			
	    	$('#tblDeclaracionesIvaMensual').on('error.dt', function(event, settings, techNote, message){
			    alertaFactory.agregarAlerta(Constante.ERROR, settings.jqXHR.responseJSON.error);
			    //$scope.$apply();
	    	});
	    	
			$('#tblDeclaracionesIvaMensual').on('draw.dt', function (event, data) {
			    if(data._iRecordsTotal == 0){
			    	alertaFactory.agregarAlerta(Constante.INFO, "DECLARACION_IMPUESTOS.NO_DATOS_DECLARACIONES");
			    	//$scope.$apply();
			    }		    
			    
			    //fnShowAlertMsg("#frmConsultaCelRecibidos", {status: 'info', titulo: "", mensaje: "CEL.NO_DATOS_COMPROBANTES"});
			});		
	    	
		    fnGetCatalog(["MES","ANIO","FORMA_PAGO"], function(data){
		    	 $scope.catalogo = data;
		    });	 
		    		    		    
	    };	    

	    $scope.inicializarConsultaDeclaracionIvaMensual();
	};

}(window.angular));

