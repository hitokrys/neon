(function (angular) {
	'use strict';
	/**
	 * @ngdoc Modulo
	 * @name declaraciones.declaracionIvaMensual
	 * @description Este modulo angular agrupa todas las funcionalidades de declaraciones de IVA mensual.
	 * @param kendo.directives: Modulo que es dependencia de "cel.consultasCel" que contiene las directivas de la libreria de Kendo.
	 * @returns.
	 */
    
    angular.module('declaraciones.declaracionIvaMensual', ['kendo.directives']);
    	
}(window.angular));