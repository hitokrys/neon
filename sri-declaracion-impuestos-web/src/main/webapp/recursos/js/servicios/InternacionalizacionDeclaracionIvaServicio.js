(function (angular) {
	'use strict';
	/**
	 * @ngdoc Servicio
	 * @name InternacionalizacionDeclaracionIvaServicio
	 * @description Este servicio actualiza el idioma actual donde el usuario final esta trabajando por las aplicaciones.
	 * @returns.
	 */		
		angular.module("declaraciones.declaracionIvaMensual").service('InternacionalizacionDeclaracionIvaServicio', InternacionalizacionDeclaracionIvaServicio);
		InternacionalizacionDeclaracionIvaServicio.$inject = ['$translatePartialLoader', '$translate'];

		function InternacionalizacionDeclaracionIvaServicio($translatePartialLoader, $translate) {
	
			this.usarIdiomaActual = function (lenguajeActual) {
				$translatePartialLoader.addPart('/sri-declaracion-impuestos-web/recursos/lenguajes');
				$translate.use(lenguajeActual);
				$translate.refresh();
			};		
		};

}(window.angular));