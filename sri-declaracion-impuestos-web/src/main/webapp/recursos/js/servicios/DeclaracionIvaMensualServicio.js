(function (angular) {
	'use strict';
	/**
	 * @ngdoc Servicio
	 * @name DeclaracionIvaMensualServicio
	 * @description Este servicio contiene los servicios utilizados en declaraciones de IVA mensual y que los mismos son invocados desde
	 * desde el o los controladores del modulo "declaraciones.declaracionIvaMensual".
	 * @returns.
	 */	
		angular.module("declaraciones.declaracionIvaMensual").service('DeclaracionIvaMensualServicio', DeclaracionIvaMensualServicio);
		DeclaracionIvaMensualServicio.$inject = [];

		function DeclaracionIvaMensualServicio() {
			this.inicializarSubidaDeclaracionIvaMensual = function (idComponenteFileUpload, opcionesDeclaracionIvaMensual) {				
				
				opcionesDeclaracionIvaMensual.uploadUrl = "/sri-declaracion-impuestos-interfaz/rest/declaracionIvaMensual/cargarDeclaracionIvaMensualSimple";
				$(idComponenteFileUpload).fileinput(opcionesDeclaracionIvaMensual);				 
			};
		
			this.subirDeclaracionIvaMensual = function (idComponenteFileUpload) {	
				$(idComponenteFileUpload).fileinput('upload');
			};
			
			
			this.inicializarConsultaDeclaracionIvaMensual = function (idTablaAMostrar, jsonData, columnasAVisualizar) {
				
				$(idTablaAMostrar).initTable('/sri-declaracion-impuestos-interfaz/rest/declaracionIvaMensual/buscarDeclaracionesIva', jsonData, columnasAVisualizar, {"buttons": []});				
			};
		
			this.buscarDeclaracionIvaMensual = function (idTablaAMostrar) {	  
				$(idTablaAMostrar).DataTable().draw();
			};    
	  };
}(window.angular));