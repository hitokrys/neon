describe("controladores.ConsultaDeclaracionIvaMensualControladorTest", function() {
	// cargar el modulo app, si se encesitan mas modulos se debe hacer con callback
	beforeEach(module("pascalprecht.translate"));
	beforeEach(module("declaraciones.declaracionIvaMensual"));	
	
	describe("ConsultaDeclaracionIvaMensualControlador", function() {
		var scope, ctrl;
		// angular-mock permite el uso de la función inject para tener acceso a
		// los servicios y dependencias de angularjs
		// se obtiene el $rootscope y el servicio $controller para simular la
		// carga completa del ConsultasCelControlador
		beforeEach(inject(function($rootScope, $controller) {
			scope = $rootScope.$new();
			ctrl = $controller("ConsultaDeclaracionIvaMensualControlador", {				
				$scope : scope
			});
		}));
		// sección para cada crear las pruebas. Por cada it por cada cosa a testear
		
		it ("debe estar definida un objeto llamado declaracion", function() {
			expect(scope.declaracion).toBeDefined();
		});

		it ("ejecucion de funcion buscarDeclaracionIvaMensual()", function() {
			var idTabla = 'tblDeclaracionesIvaMensual';
			expect(function() {scope.buscarDeclaracionIvaMensual(idTabla);}).not.toThrow();
		});		
	})
});
