describe("controladores.DeclaracionIvaMensualControladorTest", function() {
	// cargar el modulo app, si se encesitan mas modulos se debe hacer con callback
	beforeEach(module("pascalprecht.translate"));
	beforeEach(module("declaraciones.declaracionIvaMensual"));	
	
	describe("DeclaracionIvaMensualControlador", function() {
		var scope, ctrl, varSpy;
		// angular-mock permite el uso de la función inject para tener acceso a
		// los servicios y dependencias de angularjs
		// se obtiene el $rootscope y el servicio $controller para simular la
		// carga completa del ConsultasCelControlador
		beforeEach(inject(function($rootScope, $controller) {
			scope = $rootScope.$new();
				    
			ctrl = $controller("DeclaracionIvaMensualControlador", {				
				$scope : scope
			});			
			
		}));
		// sección para cada crear las pruebas. Por cada it por cada cosa a testear
		
		it ("Debe estar definida un objeto llamado anexo", function() {
			expect(scope.anexo).toBeDefined();
		});


		it ("Ejecucion de funcion subirDeclaracionIvaMensual() simula la validacion EXITOSA del formulario [frmDeclaracionIvaMensual]", function() {
			//spyOn($.fn,'valid');
			spyOn($.fn,'valid').andReturn(true);
			
			expect(scope.subirDeclaracionIvaMensual).not.toThrow();
		});
		
		it ("Ejecucion de funcion subirDeclaracionIvaMensual() simula la validacion ERRONEA del formulario [frmDeclaracionIvaMensual]", function() {
			spyOn($.fn,'valid');
			//spyOn($.fn,'valid').andReturn(true);
			
			expect(scope.subirDeclaracionIvaMensual).not.toThrow();
		});		
	})
});
