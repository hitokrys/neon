(function (angular) {
	'use strict';

    angular.module('declaraciones.declaracionIvaMensual').config(['$translateProvider', '$translatePartialLoaderProvider', function ($translateProvider, $translatePartialLoaderProvider) {

    	//<!--BEGIN:CONFIGURACION DE INTERNACIONALIZACION-->
    	$translateProvider.useLoader('$translatePartialLoader', {
    	        urlTemplate: '{part}/{lang}.json'
    	});
    	$translateProvider.useSanitizeValueStrategy('escaped');    	                    
    	}]);
    	//<!--BEGIN:CONFIGURACION DE INTERNACIONALIZACION-->
    	
}(window.angular));