package org.chris.pruebas.rest;


import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
public class HelloServiceBean {

	String createHelloMessage(String name) {
        return "Hello " + name + "!";
    }
}
