package org.chris.pruebas.rest;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

@Path("/exa1")
public class ServicioEjemplo {

	
	//@Inject
    //HelloServiceBean helloService;
	
	@EJB
    MyProducer myProducer;

    @GET
    @Path("/json")
    @Produces({ "application/json" })
    public String getHelloWorldJSON() {
       // return "{\"result\":\"" + helloService.createHelloMessage("Christian") + "\"}";
        
        return "No retendreis aquello que me pertenece";
    }

    @GET
    @Path("/xml")
    @Produces({ "application/xml" })
    public String getHelloWorldXML() {
    	myProducer.enqueue("Un mensaje brutal...");
        return "<xml><result>" + "Hola"+ "</result></xml>";
    }
    
    private static final String _UBICACION_ARCHIVOS="D:\\TEST";
    
    
    @POST
	@Path("/upload")
	@Consumes("multipart/form-data")
	public Response uploadFile(MultipartFormDataInput input) {

		String fileName = "";

		Map<String, List<InputPart>> formParts = input.getFormDataMap();

		List<InputPart> inPart = formParts.get("file");

		for (InputPart inputPart : inPart) {

			 try {

				// Retrieve headers, read the Content-Disposition header to obtain the original name of the file
				MultivaluedMap<String, String> headers = inputPart.getHeaders();
				fileName = parseFileName(headers);

				// Handle the body of that part with an InputStream
				InputStream istream = inputPart.getBody(InputStream.class,null);

				fileName = _UBICACION_ARCHIVOS + fileName;

				saveFile(istream,fileName);

			  } catch (IOException e) {
				e.printStackTrace();
			  }

			}

                String output = "File saved to server location : " + fileName;

		return Response.status(200).entity(output).build();
	}
 // Parse Content-Disposition header to get the original file name
 	private String parseFileName(MultivaluedMap<String, String> headers) {

 		String[] contentDispositionHeader = headers.getFirst("Content-Disposition").split(";");

 		for (String name : contentDispositionHeader) {

 			if ((name.trim().startsWith("filename"))) {

 				String[] tmp = name.split("=");

 				String fileName = tmp[1].trim().replaceAll("\"","");

 				return fileName;
 			}
 		}
 		return "randomName";
 	}

 // save uploaded file to a defined location on the server
 	private void saveFile(InputStream uploadedInputStream,
 		String serverLocation) {

 		try {
 			OutputStream outpuStream = new FileOutputStream(new File(serverLocation));
 			int read = 0;
 			byte[] bytes = new byte[1024];

 			outpuStream = new FileOutputStream(new File(serverLocation));
 			while ((read = uploadedInputStream.read(bytes)) != -1) {
 				outpuStream.write(bytes, 0, read);
 			}
 			outpuStream.flush();
 			outpuStream.close();
 		} catch (IOException e) {

 			e.printStackTrace();
 		}
 	}


}
